<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_eventos'] = array (
  'order' => 100,
  'module' => 'SCO_Eventos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_EVENTOS_FROM_SCO_EVENTOS_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_eventos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_despachos'] = array (
  'order' => 100,
  'module' => 'SCO_Despachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_DESPACHOS_FROM_SCO_DESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_despachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    0 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
      'initial_filter_fields' => array(
          'emb_orig' => 'des_origen_advanced',
          'emb_transp' => 'des_modtrans_advanced',
          'emb_estado' => 'des_est_advanced[]',
          'iddivision_c' => 'iddivision_c_advanced[]'
      )
    ),
  ),
);


 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_facturasproveedor'] = array (
  'order' => 100,
  'module' => 'SCO_FacturasProveedor',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_FACTURASPROVEEDOR_FROM_SCO_FACTURASPROVEEDOR_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_facturasproveedor',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_documentodespacho'] = array (
  'order' => 100,
  'module' => 'SCO_DocumentoDespacho',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_DOCUMENTODESPACHO_FROM_SCO_DOCUMENTODESPACHO_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_documentodespacho',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_productosdespachos'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosDespachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_PRODUCTOSDESPACHOS_FROM_SCO_PRODUCTOSDESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_productosdespachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['SCO_Embarque']['subpanel_setup']['sco_embarque_sco_despachos']['override_subpanel_name'] = 'SCO_Embarque_subpanel_sco_embarque_sco_despachos';

 
            if (!defined("sugarEntry") || !sugarEntry)
    die("Not A Valid Entry Point");     
    
    $layout_defs["SCO_Embarque"]["subpanel_setup"]["sco_embarque_sco_eventos"]["top_buttons"]
 = array(0 => array("widget_class" => "SubPanelTopExelExportBtn",),1 => array("widget_class" => "SubPanelTopPdfExportBtn",),); 

//auto-generated file DO NOT EDIT
$layout_defs['SCO_Embarque']['subpanel_setup']['sco_embarque_sco_documentodespacho']['override_subpanel_name'] = 'SCO_Embarque_subpanel_sco_embarque_sco_documentodespacho';

?>