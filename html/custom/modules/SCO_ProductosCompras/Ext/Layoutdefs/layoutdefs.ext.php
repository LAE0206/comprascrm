<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2023-04-16 01:32:09
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_ordencompra_sco_productoscompras'] = array (
  'order' => 100,
  'module' => 'SCO_OrdenCompra',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_ORDENCOMPRA_SCO_PRODUCTOSCOMPRAS_FROM_SCO_ORDENCOMPRA_TITLE',
  'get_subpanel_data' => 'sco_ordencompra_sco_productoscompras',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productos_sco_productoscompras'] = array (
  'order' => 100,
  'module' => 'SCO_Productos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOS_SCO_PRODUCTOSCOMPRAS_FROM_SCO_PRODUCTOS_TITLE',
  'get_subpanel_data' => 'sco_productos_sco_productoscompras',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productoscompras_sco_proyectosco'] = array (
  'order' => 100,
  'module' => 'SCO_ProyectosCO',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOMPRAS_SCO_PROYECTOSCO_FROM_SCO_PROYECTOSCO_TITLE',
  'get_subpanel_data' => 'sco_productoscompras_sco_proyectosco',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productoscompras_sco_despachos'] = array (
  'order' => 100,
  'module' => 'SCO_Despachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOMPRAS_SCO_DESPACHOS_FROM_SCO_DESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_productoscompras_sco_despachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productoscompras_sco_productoscotizados'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosCotizados',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOMPRAS_SCO_PRODUCTOSCOTIZADOS_FROM_SCO_PRODUCTOSCOTIZADOS_TITLE',
  'get_subpanel_data' => 'sco_productoscompras_sco_productoscotizados',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productoscompras_sco_productosdespachos'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosDespachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOMPRAS_SCO_PRODUCTOSDESPACHOS_FROM_SCO_PRODUCTOSDESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_productoscompras_sco_productosdespachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);

?>