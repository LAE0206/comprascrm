<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProyectosCO"]["subpanel_setup"]['sco_productoscompras_sco_proyectosco'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosCompras',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOMPRAS_SCO_PROYECTOSCO_FROM_SCO_PRODUCTOSCOMPRAS_TITLE',
  'get_subpanel_data' => 'sco_productoscompras_sco_proyectosco',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProyectosCO"]["subpanel_setup"]['sco_productoscotizados_sco_proyectosco'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosCotizados',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOTIZADOS_SCO_PROYECTOSCO_FROM_SCO_PRODUCTOSCOTIZADOS_TITLE',
  'get_subpanel_data' => 'sco_productoscotizados_sco_proyectosco',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);

?>