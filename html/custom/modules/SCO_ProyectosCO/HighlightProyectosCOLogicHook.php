 <?php

class HighlightProyectosCOLogicHook{

    public function highlightProyectosCO(SugarBean $bean, $event, $arguments){
          
          #MODIFICANDO EL NAME DEL LISTADO, AGREGANDO ICONO
          $icono = '
          <img style="width: 20px;height: 20px;font-weight: normal;font-style: normal;margin-right: 5px;margin-bottom: 3px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGZpbGw9Im5vbmUiIHZpZXdCb3g9IjAgMCAyMCAyMCI+PHBhdGggZmlsbD0iI0ZGQjkwMCIgZD0iTTEwIDUgOC41ODYgMy41ODZBMiAyIDAgMCAwIDcuMTcyIDNIMmExIDEgMCAwIDAtMSAxdjEyYTEgMSAwIDAgMCAxIDFoMTZhMSAxIDAgMCAwIDEtMVY2YTEgMSAwIDAgMC0xLTFoLTh6Ii8+PHBhdGggZmlsbD0iI0ZGRDc1RSIgZD0iTTEwIDUgOC41ODYgNi40MTRBMiAyIDAgMCAxIDcuMTcyIDdIMXY5YTEgMSAwIDAgMCAxIDFoMTZhMSAxIDAgMCAwIDEtMVY2YTEgMSAwIDAgMC0xLTFoLTh6Ii8+PHBhdGggZmlsbD0idXJsKCNhKSIgZD0iTTEwIDUgOC41ODYgNi40MTRBMiAyIDAgMCAxIDcuMTcyIDdIMXY5YTEgMSAwIDAgMCAxIDFoMTZhMSAxIDAgMCAwIDEtMVY2YTEgMSAwIDAgMC0xLTFoLTh6Ii8+PHBhdGggZmlsbD0iI0U2NzYyOCIgZD0iTTIgMTYuNWMtLjM3MyAwLS43MS0uMTQyLS45NzMtLjM2N0EuOTg5Ljk4OSAwIDAgMCAyIDE3aDE2Yy41MDUgMCAuOTA1LS4zOC45NzMtLjg2Ny0uMjYyLjIyNi0uNi4zNjctLjk3My4zNjdIMnoiLz48cGF0aCBmaWxsPSIjZmZmIiBkPSJNOC44MzYgNi45MTQgMTAuNzUgNUgxMEw4LjU4NiA2LjQxNEEyIDIgMCAwIDEgNy4xNzIgN0gxdi41aDYuNDIyYTIgMiAwIDAgMCAxLjQxNC0uNTg2eiIgb3BhY2l0eT0iLjQiLz48ZGVmcz48bGluZWFyR3JhZGllbnQgaWQ9ImEiIHgxPSIxIiB4Mj0iMSIgeTE9IjUiIHkyPSIxNyIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiPjxzdG9wIHN0b3AtY29sb3I9IiNmZmYiIHN0b3Atb3BhY2l0eT0iLjAxIi8+PHN0b3Agb2Zmc2V0PSIuOTk5IiBzdG9wLWNvbG9yPSIjRkZENzVFIiBzdG9wLW9wYWNpdHk9Ii4zIi8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PC9zdmc+" />
';
      if($_REQUEST['action'] == 'index' || $_REQUEST['action'] == 'DetailView' ){
          $bean->name = '<div class="col-sm-4">'.$icono.'</div><div class="col-sm-8">'.$bean->name.'</div>';
        }else{
          $bean->name = $bean->name;
        }
          
           
    }
}