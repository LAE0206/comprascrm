<?php
/**
*Esta clase realiza el poblado de datos de SCO_CONTANTOS (hansa), se autopobla los datos del Subpanel
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license /var/www/html/custom/modules/SCO_OrdenCompra
*/
class Cldatospm
{
  static $already_ran = false;

  function Fndatospm($bean, $event, $arguments)
  {
    //Modulo Users
    $user = BeanFactory::getBean('Users', $bean->user_id_c);
    $bean->proyc_codigopm = $user->idempleado_c;
    $bean->proyc_aux1 = $user->user_name;
    $bean->save();
  }
}