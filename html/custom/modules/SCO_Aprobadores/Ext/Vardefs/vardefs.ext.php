<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2023-04-16 01:32:09
$dictionary["SCO_Aprobadores"]["fields"]["sco_ordencompra_sco_aprobadores"] = array (
  'name' => 'sco_ordencompra_sco_aprobadores',
  'type' => 'link',
  'relationship' => 'sco_ordencompra_sco_aprobadores',
  'source' => 'non-db',
  'module' => 'SCO_OrdenCompra',
  'bean_name' => 'SCO_OrdenCompra',
  'vname' => 'LBL_SCO_ORDENCOMPRA_SCO_APROBADORES_FROM_SCO_ORDENCOMPRA_TITLE',
  'id_name' => 'sco_ordencompra_sco_aprobadoressco_ordencompra_ida',
);
$dictionary["SCO_Aprobadores"]["fields"]["sco_ordencompra_sco_aprobadores_name"] = array (
  'name' => 'sco_ordencompra_sco_aprobadores_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SCO_ORDENCOMPRA_SCO_APROBADORES_FROM_SCO_ORDENCOMPRA_TITLE',
  'save' => true,
  'id_name' => 'sco_ordencompra_sco_aprobadoressco_ordencompra_ida',
  'link' => 'sco_ordencompra_sco_aprobadores',
  'table' => 'sco_ordencompra',
  'module' => 'SCO_OrdenCompra',
  'rname' => 'name',
);
$dictionary["SCO_Aprobadores"]["fields"]["sco_ordencompra_sco_aprobadoressco_ordencompra_ida"] = array (
  'name' => 'sco_ordencompra_sco_aprobadoressco_ordencompra_ida',
  'type' => 'link',
  'relationship' => 'sco_ordencompra_sco_aprobadores',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SCO_ORDENCOMPRA_SCO_APROBADORES_FROM_SCO_APROBADORES_TITLE',
);


// created: 2023-04-16 01:32:13
$dictionary["SCO_Aprobadores"]["fields"]["sco_autorizaciones_sco_aprobadores"] = array (
  'name' => 'sco_autorizaciones_sco_aprobadores',
  'type' => 'link',
  'relationship' => 'sco_autorizaciones_sco_aprobadores',
  'source' => 'non-db',
  'module' => 'SCO_Autorizaciones',
  'bean_name' => 'SCO_Autorizaciones',
  'vname' => 'LBL_SCO_AUTORIZACIONES_SCO_APROBADORES_FROM_SCO_AUTORIZACIONES_TITLE',
  'id_name' => 'sco_autorizaciones_sco_aprobadoressco_autorizaciones_ida',
);
$dictionary["SCO_Aprobadores"]["fields"]["sco_autorizaciones_sco_aprobadores_name"] = array (
  'name' => 'sco_autorizaciones_sco_aprobadores_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SCO_AUTORIZACIONES_SCO_APROBADORES_FROM_SCO_AUTORIZACIONES_TITLE',
  'save' => true,
  'id_name' => 'sco_autorizaciones_sco_aprobadoressco_autorizaciones_ida',
  'link' => 'sco_autorizaciones_sco_aprobadores',
  'table' => 'sco_autorizaciones',
  'module' => 'SCO_Autorizaciones',
  'rname' => 'document_name',
);
$dictionary["SCO_Aprobadores"]["fields"]["sco_autorizaciones_sco_aprobadoressco_autorizaciones_ida"] = array (
  'name' => 'sco_autorizaciones_sco_aprobadoressco_autorizaciones_ida',
  'type' => 'link',
  'relationship' => 'sco_autorizaciones_sco_aprobadores',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_SCO_AUTORIZACIONES_SCO_APROBADORES_FROM_SCO_AUTORIZACIONES_TITLE',
);

?>