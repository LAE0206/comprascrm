<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2023-04-16 01:32:09
$layout_defs["SCO_Productos"]["subpanel_setup"]['sco_ordencompra_sco_productos'] = array (
  'order' => 100,
  'module' => 'SCO_OrdenCompra',
  'subpanel_name' => '',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_ORDENCOMPRA_SCO_PRODUCTOS_FROM_SCO_ORDENCOMPRA_TITLE',
  'get_subpanel_data' => 'sco_ordencompra_sco_productos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);

?>