<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2023-04-16 01:32:07
$layout_defs["SCO_ProductosDespachos"]["subpanel_setup"]['sco_despachos_sco_productosdespachos'] = array (
  'order' => 100,
  'module' => 'SCO_Despachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_DESPACHOS_SCO_PRODUCTOSDESPACHOS_FROM_SCO_DESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_despachos_sco_productosdespachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);


 // created: 2023-04-16 01:32:08
$layout_defs["SCO_ProductosDespachos"]["subpanel_setup"]['sco_embarque_sco_productosdespachos'] = array (
  'order' => 100,
  'module' => 'SCO_Embarque',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_PRODUCTOSDESPACHOS_FROM_SCO_EMBARQUE_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_productosdespachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);

?>