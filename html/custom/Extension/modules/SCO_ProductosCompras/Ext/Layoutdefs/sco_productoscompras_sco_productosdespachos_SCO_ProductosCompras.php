<?php
 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productoscompras_sco_productosdespachos'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosDespachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOMPRAS_SCO_PRODUCTOSDESPACHOS_FROM_SCO_PRODUCTOSDESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_productoscompras_sco_productosdespachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
