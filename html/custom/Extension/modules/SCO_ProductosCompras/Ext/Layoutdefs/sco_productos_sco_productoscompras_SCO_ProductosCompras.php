<?php
 // created: 2023-04-16 01:32:10
$layout_defs["SCO_ProductosCompras"]["subpanel_setup"]['sco_productos_sco_productoscompras'] = array (
  'order' => 100,
  'module' => 'SCO_Productos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOS_SCO_PRODUCTOSCOMPRAS_FROM_SCO_PRODUCTOS_TITLE',
  'get_subpanel_data' => 'sco_productos_sco_productoscompras',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
