<?php
 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_despachos'] = array (
  'order' => 100,
  'module' => 'SCO_Despachos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_DESPACHOS_FROM_SCO_DESPACHOS_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_despachos',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    0 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
      'initial_filter_fields' => array(
          'emb_orig' => 'des_origen_advanced',
          'emb_transp' => 'des_modtrans_advanced',
          'emb_estado' => 'des_est_advanced[]',
          'iddivision_c' => 'iddivision_c_advanced[]'
      )
    ),
  ),
);
