<?php
 // created: 2023-04-16 01:32:08
$layout_defs["SCO_Embarque"]["subpanel_setup"]['sco_embarque_sco_facturasproveedor'] = array (
  'order' => 100,
  'module' => 'SCO_FacturasProveedor',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_EMBARQUE_SCO_FACTURASPROVEEDOR_FROM_SCO_FACTURASPROVEEDOR_TITLE',
  'get_subpanel_data' => 'sco_embarque_sco_facturasproveedor',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
