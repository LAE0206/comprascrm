<?php
 // created: 2023-04-16 01:32:11
$layout_defs["SCO_Proveedor"]["subpanel_setup"]['sco_consolidacion_sco_proveedor'] = array (
  'order' => 100,
  'module' => 'SCO_Consolidacion',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_CONSOLIDACION_SCO_PROVEEDOR_FROM_SCO_CONSOLIDACION_TITLE',
  'get_subpanel_data' => 'sco_consolidacion_sco_proveedor',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
