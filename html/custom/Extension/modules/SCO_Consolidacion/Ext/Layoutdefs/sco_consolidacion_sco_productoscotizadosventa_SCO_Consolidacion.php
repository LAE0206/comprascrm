<?php
 // created: 2023-04-16 01:32:11
$layout_defs["SCO_Consolidacion"]["subpanel_setup"]['sco_consolidacion_sco_productoscotizadosventa'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosCotizadosVenta',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_CONSOLIDACION_SCO_PRODUCTOSCOTIZADOSVENTA_FROM_SCO_PRODUCTOSCOTIZADOSVENTA_TITLE',
  'get_subpanel_data' => 'sco_consolidacion_sco_productoscotizadosventa',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
