<?php
 // created: 2023-04-16 01:32:10
$layout_defs["SCO_OrdenCompra"]["subpanel_setup"]['sco_productoscotizados_sco_ordencompra'] = array (
  'order' => 100,
  'module' => 'SCO_ProductosCotizados',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_PRODUCTOSCOTIZADOS_SCO_ORDENCOMPRA_FROM_SCO_PRODUCTOSCOTIZADOS_TITLE',
  'get_subpanel_data' => 'sco_productoscotizados_sco_ordencompra',
  'top_buttons' => 
  array (
    #0 => 
    #array (
    #  'widget_class' => 'SubPanelTopButtonQuickCreate',
    #),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
