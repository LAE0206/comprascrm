<?php
 // created: 2023-04-16 01:32:09
$layout_defs["SCO_OrdenCompra"]["subpanel_setup"]['sco_ordencompra_sco_documentos'] = array (
  'order' => 100,
  'module' => 'SCO_documentos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_ORDENCOMPRA_SCO_DOCUMENTOS_FROM_SCO_DOCUMENTOS_TITLE',
  'get_subpanel_data' => 'sco_ordencompra_sco_documentos',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
