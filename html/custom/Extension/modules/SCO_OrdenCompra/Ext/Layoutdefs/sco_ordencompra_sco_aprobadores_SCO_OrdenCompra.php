<?php
 // created: 2023-04-16 01:32:09
$layout_defs["SCO_OrdenCompra"]["subpanel_setup"]['sco_ordencompra_sco_aprobadores'] = array (
  'order' => 100,
  'module' => 'SCO_Aprobadores',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SCO_ORDENCOMPRA_SCO_APROBADORES_FROM_SCO_APROBADORES_TITLE',
  'get_subpanel_data' => 'sco_ordencompra_sco_aprobadores',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    #1 => 
    #array (
    #  'widget_class' => 'SubPanelTopSelectButton',
    #  'mode' => 'MultiSelect',
    #),
  ),
);
