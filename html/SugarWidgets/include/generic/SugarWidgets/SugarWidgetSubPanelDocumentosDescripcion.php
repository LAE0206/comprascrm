<?php
/**
*Esta clase realiza operaciones matemáticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelDocumentosDescripcion extends SugarWidgetField
{
	function displayHeaderCell($layout_def){
       return "<a class='listViewThLinkS1'>Descripcion</a>";
    }

    function displayList($layout_def){
        //Obteniendo el id del Subpanel de Despachos
    	$id_des = $layout_def['fields']['ID'];
    	$nombre = $layout_def['fields']['NAME'];
        //Obteniedno Id de la vista detallada ProdcutosCompras
        $id_pro = $_REQUEST['record'];
        //Query para obtener cantidades de productos en despachos de acuerdo al Id de Despacho ($id_des) y el Id de ProductosCompras ($id_des
        return '
                <textarea id="orc_observaciones" name="orc_observaciones" rows="1" cols="20" title="" tabindex="0"></textarea>
            ';
    }
}
