<?php
/**

*
*@author Erik Maquera <emaquera@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelEvaluadorBtnReenviar extends SugarWidgetField
{
    function displayHeaderCell($layout_def){
      return '<a style="font-weight: normal;color:#fff;">Invitación</a>';
    }
    function displayList($layout_def)
    {
      $html = '';
      $html .= '<style>
          #btnReenviar{
          background-color: #378cbe !important;
          color:#fff !important;
          border: 1px solid #cccccc;
          padding: 0;
          margin: 0 !important;
          display: inline-block;
          font-size: 1.1em !important;
          padding: 0px 10px;
          float:center;
          height:25px;
          min-width:80px;
          border-radius:7px;
          font-weight:bold;
          display:none;
        }
         #btnEnviar{
          background-color: #28a745!important;
          color:#fff !important;
          border: 1px solid #cccccc;
          padding: 0;
          margin: 0 !important;
          display: inline-block;
          font-size: 1.1em !important;
          padding: 0px 10px;
          float:center;
          height:25px;
          min-width:80px;
          border-radius:7px;
          font-weight:bold;
          display:none;
        }
        #btnEnviar:hover, #btnReenviar:hover{
          transform:scale(1.15);
          cursor:pointer;
        }
      </style>';
      $id = $layout_def['fields']['ID'];

      $beane = BeanFactory::getBean('SCC_EvaluadorCotizacion', $id);

      //aqui capturamos si se ha enviado o no para reenviar o no
      $estadoev = $beane->eco_estadoevaluacion;

      if($estadoev=='02'){
        $html .= '<button title="Reenviar Invitación" class="btn btn-sm btnEnvio" id="btnReenviar" onclick=\'invitar("'.$id.'")\'>Reenviar</button>';
      }
      else if($estadoev=='01'){
        $html .= '<button title="Enviar Invitación" class="btn btn-sm btnEnvio" id="btnEnviar" onclick=\'invitar("'.$id.'")\'>Enviar</button>';
      }
      $html.='<script>
      if(estado>=4){
        $(".btnEnvio").show();
      }
        function invitar(idEval){
          creaEvaluacionIndividual(idEval);
          $.ajax({
            url:"index.php?to_pdf=true&module=SCC_EvaluadorCotizacion&action=evaluadores",
            type:"post",
            data:{
              id:idEval,
              estado : "02",
              valor:4
            },
            success:function(e){
              alert("Se envió la invitación para su evaluación");
              location.reload();
            }, 
            error:function(data){
              alert("Error en la petición widget invitar");
            }
          })
        }

        function creaEvaluacionIndividual(idEval){
          $.ajax({
            url:"index.php?to_pdf=true&module=SCC_Evaluaciones&action=evaluacionController",
            type:"post",
            data:{
              id,
              idEval,
              valor:2
            },
            success:function(e){
              if(e == 1){
                console.log("Se creó la evaluación de manera exitosa, se actualizará la página");
              }
              else{
                console.log("Ocurrió un error - crea Evaluacion");
              }
              location.reload();
            }, 
            error:function(data){
              alert("Ocurrió un error en la petición");
            }
          })
        }
      </script>';
      return $html;
    }
}
 ?>
