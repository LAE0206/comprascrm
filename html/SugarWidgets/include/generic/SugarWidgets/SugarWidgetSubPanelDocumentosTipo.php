<?php
/**
*Esta clase realiza operaciones matemáticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelDocumentosTipo extends SugarWidgetField
{
	function displayHeaderCell($layout_def){
       return "<a class='listViewThLinkS1'>Tipo</a>";
    }

    function displayList($layout_def){
        //Obteniendo el id del Subpanel de Despachos
    	$id_des = $layout_def['fields']['ID'];
    	$nombre = $layout_def['fields']['NAME'];
        //Obteniedno Id de la vista detallada ProdcutosCompras
        $id_pro = $_REQUEST['record'];
        //Query para obtener cantidades de productos en despachos de acuerdo al Id de Despacho ($id_des) y el Id de ProductosCompras ($id_des
        return '
                <select name="doc_tipo" id="doc_tipo" title="">
                <option label="Cotización" value="1" selected="selected">Cotización</option>
                <option label="Informe de adjudicación excepcional" value="3">Informe de adjudicación excepcional</option>
                <option label="Informe de abastecimiento, Venta y Distribución" value="4">Informe de abastecimiento, Venta y Distribución</option>
                <option label="Otro" value="0">Otro</option>
                <option label="OC de compra firmada" value="2">OC de compra firmada</option>
                </select>
            ';
    }
}
