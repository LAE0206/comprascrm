<?php 
/**
*Esta clase realiza operaciones matemáticas.
*
*@author Erik Maquera <emaquera@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelProveedorCorreoContacto extends SugarWidgetField
{
    function displayHeaderCell($layout_def){
        return '<a style="font-weight: normal;color:#fff;">Correo Electrónico</a>';
    }
    function displayList($layout_def)
    {
      //Obteniendo el id del Subpanel de Proveedores
        $id = $layout_def['fields']['ID'];
        $beanprov = BeanFactory::getBean('SCC_ProveedorCotizacion', $id);
        $correo=$beanprov->prc_correo;
        $html='';
        if(isset($correo)){
            $html.='<span id="correoWidget_'.$id.'">'.$correo.'</span>';
        }
        return $html; 
    }  
}
?>