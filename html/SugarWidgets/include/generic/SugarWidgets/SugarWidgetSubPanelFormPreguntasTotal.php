<?php 
/**
*Este widget muestra la cantidad de preguntas de cada formulario en el subpanel Formulario de Preguntas
*
*@author Erik Maquera <emaquera@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelFormPreguntasTotal extends SugarWidgetField
{
    function displayHeaderCell($layout_def){
        return '<a style="font-weight: normal;color:#fff;">Total Preguntas</a>';
    }
    function displayList($layout_def)
    {
      //Obteniendo el id del Subpanel de Form Preguntas
        $id = $layout_def['fields']['ID'];
        $html='
        <style>
          .cantPregs{
            width:30px;
            height:30px;
            background-color:#31708F;
            border-radius:50%;
            margin-left:23px;
            padding-top:6px;
            padding-left:10px;
            color:#fff;
            font-size:14px;
            font-weight:bold;
          }
        </style>
        ';
        $html.='
        <div>
          <div class="cantPregs" id="cantPregs_'.$id.'">
            
          </div>
        </div>
        ';
         $html.='<script>
         idForm="'.$id.'"
          $(document).ready(function(){
            $.ajax({
              type:"post",
              url:"index.php?to_pdf=true&module=SCC_Formulario&action=formularioCRUD",
              data:{
                valor:8,
                id:idForm
              },
              beforeSend:function(){
                console.log("Cargando datos")
              },
              success:function(e){
                const res = JSON.parse(e)
                preguntas=res.preguntas
                let cantidad = preguntas.length
                $("#cantPregs_"+idForm).html(cantidad)
              }, 
              error:function(){
                alert("Ocurrió un error en la petición")
              }
            })
          })
         </script>';
        
       return $html; 
    }  
}
?>