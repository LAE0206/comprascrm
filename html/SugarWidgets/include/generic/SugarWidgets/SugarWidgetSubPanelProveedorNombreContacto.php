<?php 
/**
*Esta clase realiza operaciones matemáticas.
*
*@author Erik Maquera <emaquera@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelProveedorNombreContacto extends SugarWidgetField
{

    function displayList($layout_def)
    {
      //Obteniendo el id del Subpanel de Proveedores
        $id = $layout_def['fields']['ID'];
        $beanp = BeanFactory::getBean('SCC_ProveedorCotizacion', $id);
        $idContacto = $beanp->contact_id_c;

        $beanC=BeanFactory::getBean('Contacts', $idContacto);
        $nombreC=$beanC->first_name;
        $apellidoC=$beanC->last_name;

        $html='';
        $html='<style>
          .btn-primary{
            background-color:#378cbe !important;
          }
          .contact{
            height:25px;
            font-size:11px;
          }
          .fila{
            width:170px;
          }
        </style>';
        $html.='<script>var idProv="'.$id.'"
        </script>';
        if($idContacto==null){
          $html.= '
          <form id="subpanelProv_'.$id.'">
          <div class="row fila">
          <div class="col-sm-8">
                <input type="text" class="form-control contact" id="contactoProv_'.$id.'" name="contactoProv_'.$id.'" onkeypress=\'agregar(event, this.value, "'.$id.'")\'>
                <input type="hidden" class="form-control contact" id="idContact_'.$id.'" name="idContact_'.$id.'">
          </div>
         <div class="col-sm-2"> 
                <button type="button" onclick=\'open_popup(
                "Contacts", 
                600,    
                400, 
                "", 
                true, 
                false, 
                {"call_back_function":"set_return","form_name":"subpanelProv_'.$id.'","field_to_name_array":{"id":"idContact_'.$id.'","name":"contactoProv_'.$id.'"}}, 
                "single", 
                true
                );\' class="text-white btn btn-primary contact form-control"><span class=" justify-content-center"><i class="fas fa-search"></i></span></button>
         </div>
         <div class="col-sm-2">       
                    <button type="button" class="btn btn-primary contact form-control" onclick=\'limpiarContacto("'.$id.'")\'><span class="justify-content-center"><i class="fas fa-user-times"></i></span></button>
        </div>
                  </div>
                  </form>';
                  $html.='<script>
                    function agregar(e, val, idProv){

                     if(e.keyCode === 13 && !e.shiftKey){
                      e.preventDefault()
                        
                        var ic=$("#idContact_"+idProv).val()
                        if(ic.length>0){
                            $.ajax({
                            type:"post",
                            url:"index.php?to_pdf=true&module=SCC_ProveedorCotizacion&action=proveedores",
                            data:{
                              datos:ic,
                              filtro:6,
                              id:idProv
                            },
                            beforeSend:function(){
                              console.log("Enviando...")
                            },
                            success:function(e){
                              $("#subpanelProv_"+idProv).html(val)
                              $("#correoWidget_"+idProv).html(e)
                            },
                            error:function(data){
                              alert("ocurrio un error")
                            }
                          })
                        }
                        else{
                          console.log("No existe contacto para agregar")
                        }
                     }
                    }
                    function limpiarContacto(idP){
                      $("#contactoProv_"+idP).val("")
                      $("#idContact_"+idP).val("")
                  }
                  </script>';
        }
        else{
          $html.= '<span>'.$nombreC.' '.$apellidoC.'</span>';
        }
       return $html; 
    }  
}
?>