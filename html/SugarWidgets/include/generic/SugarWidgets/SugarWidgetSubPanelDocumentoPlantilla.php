<?php
/**

*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelDocumentoPlantilla extends SugarWidgetField
{
    function displayHeaderCell($layout_def){
      return '<center><a style="font-weight: normal;color:#fff;">Plantilla</a></center>';
    }
    function displayList($layout_def)
    {
      $html .= '';
      $html .= '<style>
        .border{
          border:1px solid #000;
        }
      </style>';
      $id = $layout_def['fields']['ID'];
      $beand = BeanFactory::getBean('SCC_DocumentoSolicitado', $id);

      //aqui capturamos si se ha enviado o no para reenviar o no
      $nomPlantilla=$beand->dos_nombrepla;
      $enlace=$beand->dos_enlacepla;
      $tamanio=$beand->dos_tamaniopla;
      $b64=$beand->dos_plantilla;
      $html.='<script>
          var sw=0
        </script>';
      if(isset($nomPlantilla) && $nomPlantilla!=null){
        $html.='
        <center><div style="width:180px !important" id="plantilla_'.$id.'">

        </div></center>

        <script>
          sw=1
        </script>
        ';
      }
      else{
        $html.='<center><h6>Sin plantilla</h6></center>';
      }

      $html.='
      <script src="modules/SCC_DocumentoSolicitado/libs/js/iconos.js"></script>
      <script>
        $(document).ready(function(){
          if(sw==1){
            var idDoc="'.$id.'"
            var nombre="'.$nomPlantilla.'"
            var tipo=nombre.split(".")[1]
            var tamanio="'.$tamanio.'"
            var b64="'.$b64.'"
            var file = dataURLtoFile(b64, nombre)
            const enlaceDoc = URL.createObjectURL(file)

            var icono=""
            if(tipo=="pdf"){
              icono=iconoPDF()
            }
            else if(tipo=="docx"){
              icono=iconoWord()
            }
            else if(tipo=="xlsx"){
              icono=iconoExcel()
            }
            else if(tipo=="jpg" || tipo=="jpeg" || tipo=="JPG" || tipo=="JPEG" || tipo=="PNG" || tipo=="png"){
              icono=iconoJPG()
            }
            else if(tipo=="zip" || tipo=="rar" || tipo=="tar"){
              icono=iconoRAR()
            }
            else if(tipo=="ppt"){
              icono=iconoPPT()
            }
            else{
              icono=iconoUnknown()
            }
            var doc=`
              <div>
              <div class="col-sm-3">
                <img width="30" height="30" src="${icono}" />
              </div>
              <div class="col-sm-9 text-left mt-5">
                <div>
                  <a href="${enlaceDoc}" target="_blank" id="nombreDoc">${nombre}</a>
                </div>
                <div>
                  <a href="${enlaceDoc}" target="_blank" id="tamanioDoc">${tamanio} Kb</a>
                </div>
              </div>
            </div>
            `
          $(`#plantilla_${idDoc}`).html(doc)
          }
      })

      function dataURLtoFile(dataurl, filename) {
 
        var arr = dataurl.split(","),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
            
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        
        return new File([u8arr], filename, {type:mime});
    }
      </script>

      ';
      return $html;
    }
}
 ?>
