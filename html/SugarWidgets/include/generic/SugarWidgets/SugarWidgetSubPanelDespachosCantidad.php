<?php
/**
*Esta clase realiza operaciones matemáticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelDespachosCantidad extends SugarWidgetField
{
	function displayHeaderCell($layout_def){
        if($id_pro = $_REQUEST['module'] == 'SCO_ProductosCompras'){
            return "
                    <div class='row'>
                        <div class='col-sm-4'>
                            <a style='font-weight: normal;color:#fff;'>Cantidad</a>
                        </div>
                        <div class='col-sm-8'>
                            <a style='font-weight: normal;color:#fff;'></a>
                        </div>
                    </div>";
        }else{
            return "
                    <div class='row'>
                        <div class='col-sm-6'>
                            <a style='font-weight: normal;color:#fff;'>Detalle Producto</a>
                        </div>
                    </div>";
        }
    }

    function displayList($layout_def){
        //Obteniendo el id del Subpanel de Despachos
    	$id_des = $layout_def['fields']['ID'];
    	$nombre = $layout_def['fields']['NAME'];
        //Obteniedno Id de la vista detallada ProdcutosCompras
        $id_pro = $_REQUEST['record'];
        //Query para obtener cantidades de productos en despachos de acuerdo al Id de Despacho ($id_des) y el Id de ProductosCompras ($id_des)
        $pc_des = "SELECT ifnull(SUM(pd.prdes_cantidad), 0) as cantidad
        FROM sco_despachos_sco_productosdespachos_c as d_pd
        INNER JOIN sco_productosdespachos as pd
        ON d_pd.sco_despachos_sco_productosdespachossco_productosdespachos_idb = pd.id
        INNER JOIN sco_productos_co as p
        ON pd.prdes_idproductos_co = p.id
        WHERE d_pd.deleted = 0
        AND pd.deleted = 0
        AND d_pd.sco_despachos_sco_productosdespachossco_despachos_ida = '$id_des'
        AND p.pro_idpro = '$id_pro'; ";
        $obj_pc_des = $GLOBALS['db']->query($pc_des, true);
        $row_pc_des = $GLOBALS['db']->fetchByAssoc($obj_pc_des);

        $pc_items = "SELECT COUNT(*) as cantidadItems
                    FROM suitecrm.sco_despachos_sco_productosdespachos_c dp
                    INNER JOIN suitecrm.sco_productosdespachos pd
                    ON dp.sco_despachos_sco_productosdespachossco_productosdespachos_idb = pd.id
                    WHERE sco_despachos_sco_productosdespachossco_despachos_ida = '$id_des'
                    AND dp.deleted = 0
                    AND pd.deleted = 0; ";
        $obj_pc_items = $GLOBALS['db']->query($pc_items, true);
        $row_pc_items = $GLOBALS['db']->fetchByAssoc($obj_pc_items);



        if($_REQUEST['module'] == 'SCO_ProductosCompras'){

            $query_des = "SELECT SUM(pd.prdes_cantidad) as cantidaTotalEnDespacho
                        FROM suitecrm.sco_despachos_sco_productosdespachos_c as d_pd
                        INNER JOIN suitecrm.sco_productosdespachos as pd
                        ON d_pd.sco_despachos_sco_productosdespachossco_productosdespachos_idb = pd.id
                        WHERE d_pd.deleted = 0
                        AND pd.deleted = 0
                        AND sco_despachos_sco_productosdespachossco_despachos_ida = '$id_des' ";
            $obj_despacho = $GLOBALS['db']->query($query_des, true);
            $row_despacho = $GLOBALS['db']->fetchByAssoc($obj_despacho);

            $porcentajeItem = ($row_pc_des['cantidad'] / $row_despacho['cantidaTotalEnDespacho'] ) * 100;
            if($porcentajeItem == 0){
                $porcentajeRestante = 0;
                $porcentajeRestanteValue = 100;
            }else{
                $porcentajeRestante = 100 - $porcentajeItem;
                $porcentajeRestanteValue = 100 - $porcentajeItem;
            }
            
            return "<div class='row'style='width: 170px;'>
                        <div class='col-sm-3' style='line-height: 37px;'>
                            <span class='badge badge-pill badge-info'>".$row_pc_des['cantidad']."</span>
                        </div>
                        <div class='col-sm-9' >                        
                            <div class='progress' style='margin-top: 10px;'>                      
                                <div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='".round($porcentajeItem)."' style='width:".round($porcentajeItem)."%'>
                                    ".round($porcentajeItem)."%
                                </div>
                                <div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='".round($porcentajeRestante)."' style='width:".round($porcentajeRestanteValue)."%'>
                                    ".round($porcentajeRestante)."%
                                </div>
                            </div>
                        </div>
                    </div>                                            
                    ";
        }elseif($_REQUEST['module'] == 'SCO_OrdenCompra'){
            //Query para obtener cantidades de productos en despachos de acuerdo al Id de Despacho ($id_des)
            $despacho = "SELECT SUM(pd.prdes_cantidad) as cantidad
            FROM sco_despachos_sco_productosdespachos_c as d_pd
            INNER JOIN sco_productosdespachos as pd
            ON d_pd.sco_despachos_sco_productosdespachossco_productosdespachos_idb = pd.id
            INNER JOIN sco_productos_co as p
            ON pd.prdes_idproductos_co = p.id
            WHERE d_pd.deleted = 0
            AND pd.deleted = 0
            AND d_pd.sco_despachos_sco_productosdespachossco_despachos_ida = '$id_des';";
            $obj_despacho = $GLOBALS['db']->query($despacho, true);
            $row_despacho = $GLOBALS['db']->fetchByAssoc($obj_despacho);

            $query_pro_oc = "SELECT SUM(pro_cantidad) as pro_cantidad FROM sco_productos_co WHERE pro_idco = '".$_REQUEST['record']."'; ";
            $obj_pro_oc = $GLOBALS['db']->query($query_pro_oc, true);
            $row_pro_oc = $GLOBALS['db']->fetchByAssoc($obj_pro_oc);
            
            $porcentajepro_oc = ($row_despacho['cantidad'] / $row_pro_oc['pro_cantidad']) * 100;
            
            if($porcentajepro_oc == 0){
                $porcentajepro_ocRestante = 0;
                $porcentajepro_ocRestanteValue = 100;
            }else{
                $porcentajepro_ocRestante = 100 - $porcentajepro_oc;
                $porcentajepro_ocRestanteValue = 100 - $porcentajepro_oc;
            }

            return "
                <div class='row'style='width: 170px;'>
                    <div class='col-sm-3' style='line-height: 37px;'>
                        <p class='text-info'style='font-size: 12px;'>Productos:  ".$row_pc_items['cantidadItems']." </p>
                        <p style='font-size: 12px;background: #fff;color:#000;' class=''>".$row_despacho['cantidad']." / ".$row_pro_oc['pro_cantidad']."</p>
                    </div>                
                    <div class='col-sm-9' >                        
                        <div class='progress' style='margin-top: 10px;'>                      
                            <div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='".round($porcentajepro_oc)."' style='width:".round($porcentajepro_oc)."%'>".round($porcentajepro_oc)."%
                            </div>
                            <div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='".round($porcentajepro_ocRestante)."' style='width:".round($porcentajepro_ocRestanteValue)."%'>".round($porcentajepro_ocRestante)."%
                            </div>
                        </div>
                    </div>
                </div>
            ";            
        }else{

            //Query para obtener cantidades de productos en despachos de acuerdo al Id de Despacho ($id_des)
            $despacho = "SELECT SUM(pd.prdes_cantidad) as cantidad
            FROM sco_despachos_sco_productosdespachos_c as d_pd
            INNER JOIN sco_productosdespachos as pd
            ON d_pd.sco_despachos_sco_productosdespachossco_productosdespachos_idb = pd.id
            INNER JOIN sco_productos_co as p
            ON pd.prdes_idproductos_co = p.id
            WHERE d_pd.deleted = 0
            AND pd.deleted = 0
            AND d_pd.sco_despachos_sco_productosdespachossco_despachos_ida = '$id_des';";
            $obj_despacho = $GLOBALS['db']->query($despacho, true);
            $row_despacho = $GLOBALS['db']->fetchByAssoc($obj_despacho);
            return "<p class='text-info'style='font-size: 12px;'>items ".$row_pc_items['cantidadItems']." </p><p style='font-size: 12px;background: #fff;color:#000;' class=''>Cantidad ".$row_despacho['cantidad']."</p>";
        }
    }
}
