<?php 
/**
*Este clase devuelve la calificacion de un proveedor
*
*@author Erik Maquera <emaquera@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelProveedorCalificacion extends SugarWidgetField
{
    function displayHeaderCell($layout_def){
        return '<a style="font-weight: normal;color:#fff;">Calificación</a>';
    }
    function displayList($layout_def)
    {
      //Obteniendo el id del Subpanel de Proveedores
        $id = $layout_def['fields']['ID'];

        $beanp = BeanFactory::getBean('SCC_ProveedorCotizacion', $id);
        $valor = $beanp->prc_calificacion;
        $valor1=5-$valor;
        for($i=0;$i<$valor;$i++){
          $html.='<i class="fas fa-star" style="color:#ffc107;"></i>';
        }
        for($i=0;$i<$valor1;$i++){
          $html.='<i class="fas fa-star" style="color:#9C9486;"></i>';
        }
       return $html; 
    }  
}
?>