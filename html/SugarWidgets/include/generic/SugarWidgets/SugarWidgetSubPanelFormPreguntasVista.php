<?php 
/**
*Este widget muestra la cantidad de preguntas de cada formulario en el subpanel Formulario de Preguntas
*
*@author Erik Maquera <emaquera@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/include/generic/SugarWidgets/
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SugarWidgetSubPanelFormPreguntasVista extends SugarWidgetField
{
    function displayHeaderCell($layout_def){
        return '<a style="font-weight: normal;color:#fff; padding-left:60px;">Vista</a>';
    }
    function displayList($layout_def)
    {
      //Obteniendo el id del Subpanel de Form Preguntas
        $id = $layout_def['fields']['ID'];
        $html='';
        $html.='
          <div>
           <a href="#"><span onclick=\'verForm("'.$id.'")\'>Ver formulario de preguntas <i style="font-size:15px; padding-top:10px;" class="fas fa-eye"></i></span></a>
          </div>
          <!-- Modal Vista Plantilla-->
            <div class="modal fade" id="modalVistaPlantilla" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document" style="width:600px;">
                <div class="modal-contentView" style="padding:10px;">
                  <div class="modal-header2" style="margin:20px;">
                    <span class="modal-title h3">Vista de Formulario</span>
                    <span>

                    </span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div id="vistaPlantilla" class="border" style="border-radius:3px; margin-top:10px; width:100%; height:100%;">
                          
                      </div>
                  </div>
                </div>
              </div>
            </div>
        ';
         $html.='<script>
         var idFormV="'.$id.'"
         $(document).ready(function(){
            console.log(idFormV)
          })
          function verForm(idx){
            $("#modalVistaPlantilla").modal("show")
            $.ajax({
              type:"post",
              url:"index.php?to_pdf=true&module=SCC_Formulario&action=formularioCRUD",
              data:{
                valor:10,
                id:idx
              },
              success:function(e){
                const contVReturn=atob(e)
                $("#vistaPlantilla").html(contVReturn)
              },
              error:function(data){
                alert("ocurrio un error")
              }
            })
          }
         </script>';
        
       return $html; 
    }  
}
?>