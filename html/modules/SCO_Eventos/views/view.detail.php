<?php
/**
*Esta clase realiza operaciones matemáticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/modules/views/SCO_Eventos
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class SCO_EventosViewDetail extends ViewDetail {

 	function SCO_EventosViewDetail(){
 		parent::ViewDetail();
 	}

 	function display(){
 		$id_ev = $this->bean->id;
 		$fechaReal = $this->bean->eve_fechare;
 		$aduana = $this->bean->sco_cnf_eventos_list_id_c;

 		$arr_estados =  array(1 => 'Borrador',2 =>'Solicitud de embarque',3 =>'En Transito',4 =>'Concluido');
		$estado = $this->bean->eve_estado;

		echo '<link href="modules/SCO_Consolidacion/css-loader.css?'.time().'" rel="stylesheet" type="text/css" />  
			  <link rel="stylesheet" href="modules/SCO_OrdenCompra/BackOrder/bootstrap-datetimepicker.min.css?'.time().'">';

		echo '<script src="modules/SCO_Eventos/viewdetail.js?'.time().'"></script>
			  <script src="modules/SCO_OrdenCompra/BackOrder/bootstrap-datetimepicker.min.js?'.time().'"></script>';

		echo '<div id="modalEmbarque"></div>
			  <div id="modalFecha"></div>
			  <div class="loader loader-default" data-text="Enviando datos"></div>';
			  
		$st ='<style>
			.cantidad{pointer-events:none;}
			.precio{pointer-events:none;}
			.gris{color: #ccc;}
			.gris:hover{color: #ccc;}
			.single{display: none;}
			#whole_subpanel_sco_despachos_sco_productosdespachos tbody td a {pointer-events: none; cursor: default;}
			#whole_subpanel_sco_eventos_sco_riesgo .SugarActionMenu {display:none;}
			#whole_subpanel_sco_eventos_sco_problema .SugarActionMenu {display:none;}
			#whole_subpanel_history .SugarActionMenu {display:none;}
			#whole_subpanel_activities .SugarActionMenu {display:none;}
			</style>';
			//#edit_button{pointer-events: none; cursor: default;}
echo "<style>#list_subpanel_sco_ordencompra_sco_productos .list{display:none;}
            .menuoc{
              float: right !important;
              margin-top: -10px;
            }
            .menuoc-text{
              margin-right: 3px;
              background: #fff;
              padding: 5px 10px;
            }
            .menuoc-text img{
              height: 17px;
            }
            .menuoc-text:hover{              
              background: #eee;
            }
            .menuoc-click{
              cursor: pointer;
              margin-left: 5px;
            }
            .menu-send{
              background:#427505;
            }
            .menu-send:hover{
              background: #70aa2a;
            }
            .menu-anular{
              background:#f44336
            }
            .menu-anular:hover{
              background: #ffa0a0;
            }
        </style>";

        

 		switch ($estado) {
 			case '':
	 			echo '<div class="menuoc">	          
		          <span class="menuoc-text menu-send"> 
					<span class="glyphicon glyphicon-send" style="font-size: 12px; color: #000 !important; line-height: 33px !important;"></span>
		            <span class="menuoc-click" onClick="solicitar(2);">Concluir Evento</span>
		          </span>
		          </div>
		          ';
	 			parent::display();
				
				echo '<script>
					var id_ev = "'.$id_ev.'";
					var fechaReal = "'.$fechaReal.'";
					var fechaReal2 = fechaReal.split("/");
					var fechaReal3 = fechaReal2[1] + "/"+fechaReal2[0]+ "/"+fechaReal2[2]
					
				</script>';
 				break;

 			case 'Pendiente':

	 			echo '<div class="menuoc">
		          <span class="menuoc-text"> 
		            <span class="glyphicon glyphicon-calendar" style="font-size: 12px; color: #000 !important; line-height: 33px !important;"></span>
		            <span class="menuoc-click" class="btn btn-sm btn-info" onClick="ventanaModalFecha();" >Registrar Fecha Real </span>
		          </span>
		          
		          <span class="menuoc-text menu-send" style="color: #FFF;"> 
					<span class="glyphicon glyphicon-send" style="font-size: 12px; line-height: 33px !important;"></span>
		            <span class="menuoc-click"  onClick="solicitar(2);">Concluir Evento</span>
		          </span>
		          </div>
		          ';
	 				parent::display();
				echo '<script>
					var id_ev = "'.$id_ev.'";
					var aduana = "'.$aduana.'";
					var fechaReal = "'.$fechaReal.'";
					if(fechaReal != ""){
						var fechaReal2 = fechaReal.split("/");
						var fechaReal3 = fechaReal2[1] + "/"+fechaReal2[0]+ "/"+fechaReal2[2]
					}else{
						var fechaReal3 = "";
					}
				</script>';
 				break;
 			case 'Concluido':
	 			echo '<div class="menuoc">		          
		          <span class="menuoc-text menu-send" style="color:#70aa2a;"> 
					<span class="glyphicon glyphicon-ok-sign" style="font-size: 12px; line-height: 33px !important;"></span>
		            <span class="menuoc-click" style="color: #FFF;" onClick="solicitar(2);">Concluido</span>
		          </span>
		          </div>
		          ';
	 			parent::display();
				echo '<script>
				var id_ev = "'.$id_ev.'";			
				</script>';
 				break;
 			default:
 			parent::display();
 				break;
 		}

 	}
 }
?>
