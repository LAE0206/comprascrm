<?php
/**
*Esta clase realiza operaciones matemÃ¡ticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/modules/views/SCO_Despachos
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class SCO_DespachosViewDetail extends ViewDetail {

 	function SCO_DespachosViewDetail(){
 		parent::ViewDetail();
 	}

 	function display(){
    echo '<script src="modules/SCO_Despachos/viewdetail.js?'.time().'"></script>';
    echo '
    <link href="modules/SCO_Consolidacion/css-loader.css" rel="stylesheet" type="text/css" />
    <div class="loader loader-default" data-text="Cargando"></div>';
    $html = '
    <style>.campopersonalizado{margin-top:10px;}</style>
    <div class="row detail-view-row" style="background:#FFF;">
      <div class="col-xs-12 col-sm-6 detail-view-row-item">
        <div class="col-xs-12 col-sm-4 label col-1-label">
          Total precio productos:
        </div>        
        <div class="col-sm-2 campopersonalizado " type="varchar" >
          <span class="sugar_field" ><b class="mostrarSubtotalProducto"></b></span>
        </div>  
      </div>
      
      <div class="col-xs-12 col-sm-6 detail-view-row-item">
        <div class="col-xs-12 col-sm-4 label col-1-label">
          Total cantidad productos:
        </div>      
        <div class="col-sm-2 campopersonalizado " type="varchar">
          <span class="sugar_field"><b class="mostrarTotalItems"></b></span>
        </div>        
      </div>
    </div>    
    '  ;
    $js = '    
    var re;
    var valor = 0
    $(".subtotalProducto").each(function(index) {
    console.log($(this).text());
    re = $(this).text();
    console.log(re);
    valor += parseFloat(re)
    });
    console.log("suma " + valor);
    $(".mostrarSubtotalProducto").append(valor.toFixed(2));
    
    var re;
    var valor = 0
    $(".cantidad").each(function(index) {        
    re = $(this).val();
    console.log(re);
    valor += parseFloat(re)
    });
    console.log("suma " + valor)    
    $(".mostrarTotalItems").append(valor);
    ';

 		//$notificaciones = new Notifica();
 		//ID del DESPACHOS
 		$id_des = $this->bean->id;
 		$arr_estados =  array(1 => 'Borrador',2 =>'Solicitud de embarque',3 =>'En Transito',4 =>'Concluido');

		$estado = $this->bean->des_est;
		//CSS, para ocultar botones y Subpaneles
		echo "<style>#whole_subpanel_sco_productoscompras_sco_despachos {display:none;}</style>";
		$st ='<style>
			.cantidad{pointer-events:none;}
			.precio{pointer-events:none;}
			.gris{color: #ccc;}
			.gris:hover{color: #ccc;}
			.single{display: none;}
			.clickMenu {display: none;}
			#edit_button{pointer-events: none; cursor: default;}

			#whole_subpanel_sco_despachos_sco_documentodespacho .clickMenu {display: none;}
			#whole_subpanel_sco_despachos_sco_productosdespachos .clickMenu {display: none;}
			</style>';

    echo "<style>
        .menuoc{
          float: right !important;
          margin-top: -10px;
        }
        .menuoc-text{
          margin-right: 3px;
          background: #fff;
          padding: 5px 10px;
        }
        .menuoc-text img{
          height: 17px;
        }
        .menuoc-text:hover{              
          background: #eee;
        }
        .menuoc-click{
          cursor: pointer;
          margin-left: 5px;
        }
        .menu-send{
          background:#427505;
          color:#FFF;
        }
        .menu-send:hover{
          background: #70aa2a;
        }
        .menu-anular{
          color:#f44336;
        }
    </style>";
    #INICIO MENU DE BOTONES PERSONALIZADOS
    echo '<div class="menuoc">';
		//agregando HTML en la vista de DESPACHOS de acuerdo a los estados del DESPACHO
 		switch ($estado) {
 			case '0':
          echo '
              <span class="menuoc-text menu-anular" > 
                <span class="suitepicon suitepicon-action-clear" style="font-size: 12px; line-height: 33px !important;"></span>
                <span class="menuoc-click" onClick="solicitar(4)" >Anular despacho con Productos</span>
              </span> 
              <span class="menuoc-text menu-anular"> 
                <span class="glyphicon glyphicon-paperclip" style="font-size: 12px; line-height: 33px !important;"></span>
                <span class="menuoc-click" onClick="intangible(3)" >Despacho intangible</span>
              </span>
              <span class="menuoc-text"> 
                <span class="glyphicon suitepicon suitepicon-module-documents" style="font-size: 12px; line-height: 33px !important;"></span>
                <span class="menuoc-click" onClick="mostrarModal()">Dividir Despacho</span>
              </span>
              <span class="menuoc-text menu-send"> 
                <span class="glyphicon glyphicon-send" style="font-size: 12px;  line-height: 33px !important;"></span>
                <span class="menuoc-click" onClick="solicitar(1);" >Solicitar Embarque</span>
              </span>
              
            </div>      
          ';
            //$notificaciones->FnnotificaDespacho($this->bean);
            parent::display();
            
            echo $html;
            echo '<script>
              '.$js.'
              var id_des = "'.$id_des.'";
              function solicitar(est){
                  $.ajax({
                    type: "get",
                    url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
                    data: {est},
                    success: function(data) {
                        data = data.replace("$","");
                        var estado = $.parseJSON(data);
                        if(estado != 9){
                          location.reload();
                        }
                        else{
                          alert("Debe escribir un comentario en el campo de observaciones");
                        }
                    }
                  });
              }
              function intangible(est){
                if (confirm("Esta seguro de convertir a intangible este despcho?")) {
                  $.ajax({
                    type: "get",
                    url: "index.php?to_pdf=true&module=SCO_Despachos&action=despachosIntangibles&id="+id_des,
                    data: {est},
                    dataType: "json",
                    success: function(data) {
                        if(data[0] != 9){
                          location.reload();
                        }
                        else{
                          alert("Debe escribir un comentario en el campo de observaciones");
                        }
                    }
                  });
                }
              }        
            </script>';
 				break;
 			case '1':
            echo $st;
            //$notificaciones->FnnotificaDespacho($this->bean);
            echo '
              <span class="menuoc-text menu-anular"> 
                <span class="glyphicon glyphicon-send" style="font-size: 12px; line-height: 33px !important;"></span>
                <span class="menuoc-click" onClick="solicitar(0);">Cancelar solicitud</span>
              </span>
            </div>      
          ';
            parent::display();
            echo $html;
            echo '<script>
              var id_des = "'.$id_des.'";
              function solicitar(est){

                $.ajax({
                type: "get",
                url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
                data: {est},
                success: function(data) {
                  data = data.replace("$","");

                  var estado = $.parseJSON(data);
                  }
                });
                location.reload();
              }
              '.$js.'
            </script>';
 				break;
 			case '2':
            echo $st;
            //$notificaciones->FnnotificaDespacho($this->bean);
            echo '
            </div>      
          ';
            parent::display();
            echo $html;
            echo '<script>'.$js.'</script>';
        break;
      case '3':
            //$notificaciones->FnnotificaDespacho($this->bean);
            echo $st;
            echo '
            </div>      
          ';
            parent::display();
            echo $html;        
            echo '<script>
              var id_des = "'.$id_des.'";
              function solicitar(est){
                //debugger;
                $.ajax({
                type: "get",
                url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
                data: {est},
                success: function(data) {
                  data = data.replace("$","");

                  var estado = $.parseJSON(data);
                  }
                });
                location.reload();
              }
              '.$js.'
            </script>';
 				break;
 			case '4':
            //$notificaciones->FnnotificaDespacho($this->bean);
            echo '<script>
              var id_des = "'.$id_des.'";
              function solicitar(est){
                //debugger;
                $.ajax({
                type: "get",
                url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
                data: {est},
                success: function(data) {
                  data = data.replace("$","");

                  var estado = $.parseJSON(data);
                  }
                });
                location.reload();
              }
              '.$js.'
            </script>';
            echo '
            </div>      
          ';
            parent::display();
            echo $st.$html;
 				break;
 			default:
 			parent::display();
 				break;
 		}

    if($estado == 0){
        require_once("modules/SCO_Despachos/dividirdespacho.php");
    }
        require_once("modules/SCO_Despachos/validacionesItems.php");
 	  }

 }

/**
class SCO_DespachosViewDetail extends ViewDetail {

  function SCO_DespachosViewDetail(){
    parent::ViewDetail();
  }

  function display(){
    echo '
    <link href="modules/SCO_Consolidacion/css-loader.css" rel="stylesheet" type="text/css" />
    <div class="loader loader-default" data-text="Cargando"></div>';
    $html = '
    <style>.campopersonalizado{margin-top:10px;}</style>
    <div class="row detail-view-row" style="background:#FFF;">
      <div class="col-xs-12 col-sm-6 detail-view-row-item">
        <div class="col-xs-12 col-sm-4 label col-1-label"style="margin-left: -15px;">
          Total precio productos:
        </div>        
        <div class="col-sm-2 campopersonalizado " type="varchar" >
          <span class="sugar_field" ><b class="mostrarSubtotalProducto"></b></span>
        </div>  
      </div>
      
      <div class="col-xs-12 col-sm-6 detail-view-row-item">
        <div class="col-xs-12 col-sm-4 label col-1-label"style="margin-left: -13px;">
          Total cantidad productos:
        </div>      
        <div class="col-sm-2 campopersonalizado " type="varchar">
          <span class="sugar_field"><b class="mostrarTotalItems"></b></span>
        </div>        
      </div>
    </div>    
    '  ;
    $js = '    
    var re;
    var valor = 0
    $(".subtotalProducto").each(function(index) {
    console.log($(this).text());
    re = $(this).text();
    console.log(re);
    valor += parseFloat(re)
    });
    console.log("suma " + valor);
    $(".mostrarSubtotalProducto").append(valor.toFixed(2));
    
    var re;
    var valor = 0
    $(".cantidad").each(function(index) {        
    re = $(this).val();
    console.log(re);
    valor += parseFloat(re)
    });
    console.log("suma " + valor)    
    $(".mostrarTotalItems").append(valor);
    ';

    //$notificaciones = new Notifica();
    //ID del DESPACHOS
    $id_des = $this->bean->id;
    $arr_estados =  array(1 => 'Borrador',2 =>'Solicitud de embarque',3 =>'En Transito',4 =>'Concluido');

    $estado = $this->bean->des_est;
    //CSS, para ocultar botones y Subpaneles
    echo "<style>#whole_subpanel_sco_productoscompras_sco_despachos {display:none;}</style>";
    $st ='<style>
      .cantidad{pointer-events:none;}
      .precio{pointer-events:none;}
      .gris{color: #ccc;}
      .gris:hover{color: #ccc;}
      .single{display: none;}
      .clickMenu {display: none;}
      #edit_button{pointer-events: none; cursor: default;}

      #whole_subpanel_sco_despachos_sco_documentodespacho .clickMenu {display: none;}
      //#whole_subpanel_sco_despachos_sco_productosdespachos .clickMenu {display: none;}
      </style>';
    //agregando HTML en la vista de DESPACHOS de acuerdo a los estados del DESPACHO
    switch ($estado) {
      case '0':
      //$notificaciones->FnnotificaDespacho($this->bean);
      parent::display();
      echo '
      <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
               <div class="col-xs-12 col-sm-6 detail-view-row-item">
              <div class="col-xs-12 col-sm-4 label col-1-label" style="margin-left: -28px;">
              Anular Despacho:
              </div>          
              <div class="col-sm-4 campopersonalizado " type="varchar" >
                <span class="sugar_field" >
                   <a class="btn btn-sm btn-success" style="padding: 2px 5px;background: #d9534f !important;" onClick="solicitar(4)" value="Ver Reporte">Anular despacho con Productos</a>
                </span>
              </div>        
              <div class="col-sm-4 campopersonalizado " type="varchar" >
                <span class="sugar_field" >
                   <a class="btn btn-sm btn-success" style="padding: 2px 5px;background: #328fdb !important;" onClick="intangible(3)" value="Ver Reporte">Despacho intangible</a>
                </span>
              </div>        
        </div>      
        <div class="col-xs-12 col-sm-6 detail-view-row-item">
              <div class="col-xs-12 col-sm-4 label col-1-label"style="margin-left: -26px;">
              Solicitud de Embarque:
              </div>        
              <div class="col-sm-7 campopersonalizado " type="varchar">
                <span class="sugar_field">
                  <a class="btn btn-sm btn-success" style="padding: 2px 5px;background: #328fdb !important;" onClick="solicitar(1);" value="Ver Reporte">Solicitar Embarque</a>
                </span>
              </div>          
        </div>
      </div>  
      '.$html.'
      ';
      echo '<script>
        '.$js.'
        var id_des = "'.$id_des.'";
        function solicitar(est){
            $.ajax({
              type: "get",
              url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
              data: {est},
              success: function(data) {
                  data = data.replace("$","");
                  var estado = $.parseJSON(data);
                  if(estado != 9){
                    location.reload();
                  }
                  else{
                    alert("Debe escribir un comentario en el campo de observaciones");
                  }
              }
            });
        }
        function intangible(est){
          if (confirm("Esta seguro de convertir a intangible este despcho?")) {
            $.ajax({
              type: "get",
              url: "index.php?to_pdf=true&module=SCO_Despachos&action=despachosIntangibles&id="+id_des,
              data: {est},
              dataType: "json",
              success: function(data) {
                  if(data[0] != 9){
                    location.reload();
                  }
                  else{
                    alert("Debe escribir un comentario en el campo de observaciones");
                  }
              }
            });
          }
        }        
      </script>';
        break;
      case '1':
      echo $st;
      //$notificaciones->FnnotificaDespacho($this->bean);
      parent::display();
      echo '
      <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
        <br>
        <div class="col-xs-12 col-sm-6 detail-view-row-item">
              <div class="col-xs-12 col-sm-4 label col-1-label">
              
              </div>          
              <div class="col-sm-3 " type="varchar" >
                <span class="sugar_field" >
                   
                </span>
              </div>               
        </div>      
        <div class="col-xs-12 col-sm-6 detail-view-row-item">
              <div class="col-xs-12 col-sm-4 label col-1-label" style="margin-left: -26px;">
              Cancelar:
              </div>        
              <div class="col-sm-2 campopersonalizado " type="varchar">
                <span class="sugar_field">
                  <a class="btn btn-sm btn-success" style="padding: 2px 5px;background: #d9534f !important;" onClick="solicitar(0);" value="Cancelar">Cancelar solicitud</a>
                </span>
              </div>          
        </div>
      </div>       
      '.$html;
      echo '<script>
        var id_des = "'.$id_des.'";
        function solicitar(est){

          $.ajax({
          type: "get",
          url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
          data: {est},
          success: function(data) {
            data = data.replace("$","");

            var estado = $.parseJSON(data);
            }
          });
          location.reload();
        }
        '.$js.'
      </script>';

        break;
      case '2':
      echo $st;
      //$notificaciones->FnnotificaDespacho($this->bean);
      
      parent::display();
      echo $html;
      echo '<script>'.$js.'</script>';
        break;
      case '3':
      //$notificaciones->FnnotificaDespacho($this->bean);
      echo $st;
      parent::display();
      echo '
      <!--div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
        <br>
        <div class="col-xs-12 col-sm-6 detail-view-row-item">
              <div class="col-xs-12 col-sm-4 label col-1-label">
              
              </div>          
              <div class="col-sm-3" >
              
              </div>               
        </div>      
        <div class="col-xs-12 col-sm-6 detail-view-row-item">
              <div class="col-xs-12 col-sm-4 label col-1-label">
              Pasar a estado:
              </div>        
              <div class="col-sm-2 campopersonalizado " type="varchar">
                <span class="sugar_field">
                  <a class="btn btn-sm btn-success" style="padding: 2px 5px;background: #d9534f !important;" onClick="solicitar(1);" value="Cancelar">Cancelar solicitud</a>
                </span>
              </div>          
        </div>
      </div-->       
      '.$html;        
      echo '<script>
        var id_des = "'.$id_des.'";
        function solicitar(est){
          //debugger;
          $.ajax({
          type: "get",
          url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
          data: {est},
          success: function(data) {
            data = data.replace("$","");

            var estado = $.parseJSON(data);
            }
          });
          location.reload();
        }
        '.$js.'
      </script>';
        break;

      case '4':
      //$notificaciones->FnnotificaDespacho($this->bean);
      echo '<script>
        var id_des = "'.$id_des.'";
        function solicitar(est){
          //debugger;
          $.ajax({
          type: "get",
          url: "index.php?to_pdf=true&module=SCO_Despachos&action=estado&id="+id_des,
          data: {est},
          success: function(data) {
            data = data.replace("$","");

            var estado = $.parseJSON(data);
            }
          });
          location.reload();
        }
        '.$js.'
      </script>';
      parent::display();
      echo $st.$html;
        break;

      default:
      parent::display();
        break;
    }
    if($estado == 0){
    require_once("modules/SCO_Despachos/dividirdespacho.php");
    }
    require_once("modules/SCO_Despachos/validacionesItems.php");
  }

 }**/
?>
