var usuarioNombre = $("#usuarioNombre").val();
var usuarioDivision = $("#usuarioDivision").val();
var usuarioAmercado = $("#usuarioAmercado").val();
var usuarioRol = $("#usuarioRol").val();
$("#division").val(usuarioDivision);
$("#aMercado").val(usuarioAmercado);

if(usuarioDivision != ''){      
    $('.am').hide();
    $('.'+usuarioDivision+'').show();
}
$("#division").change(function() {  
    $('.am').show();   
    if ($(this).data('options') === undefined) {    
      $(this).data('options', $('#aMercado option').clone());
    }
    var id = $(this).val();  
    var options = $(this).data('options').filter('.'+id+'');      
    $('#aMercado').html(options);
    $('#aMercado').append('<option value="" selected="selected">Todo</option>');    
});

$('#division').on('change', function () {
  var division = $('#division').val();
  console.log(division);
  if (division != null) {
    $.ajax({
      type: 'GET',
      url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial',
      datatype: 'json',
      data: {
        division: division,
        filtro: "aMercado"
      },
      async: false,
      success: function (e) {
        console.log(e);
        var res = JSON.parse(e);

        var html = '<option value="">Todo</option>';
        for (var i = 0; i < res.length; i++) {
          html += '<option value="' + res[i]["idfmilia_c"] + '">' + res[i]["idfamilia_c_name"] + '</option>';
        }
        $('#familia').html(html); 
        $('#grupo').html('<option value="">Todo</option>');        
      },
      error: function (data) {
        console.log('ERROR, No se pudo conectar', data);
      }
    });
  }
});
$('#aMercado').on('change', function () {
  var aMercado = $("#aMercado").val();
  var division = $('#division').val();
  console.log(division+"--"+aMercado);
  if (division != null) {
    $.ajax({
      type: 'GET',
      url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial',
      datatype: 'json',
      data: {
        division: division,
        aMercado: aMercado,
        filtro: "familia"
      },
      async: false,
      success: function (e) {
        console.log(e);
        var res = JSON.parse(e);

        var html = '<option value="">Todo</option>';
        for (var i = 0; i < res.length; i++) {
          html += '<option value="' + res[i]["idfmilia_c"] + '">' + res[i]["idfamilia_c_name"] + '</option>';
        }
        $('#familia').html(html); 
        $('#grupo').html('<option value="">Todo</option>');        
      },
      error: function (data) {
        console.log('ERROR, No se pudo conectar', data);
      }
    });
  }
});
$('#familia').on('change', function () {
  var aMercado = $("#aMercado").val();
  var division = $('#division').val();
  var familia = $('#familia').val();
  console.log(division+"--"+aMercado+"--"+familia);
  if (division != null && aMercado != null ) {
    $.ajax({
      type: 'GET',
      url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial',
      datatype: 'json',
      data: {
        division: division,
        aMercado: aMercado,
        familia: familia,
        filtro: "grupo"
      },
      async: false,
      success: function (e) {
        var res = JSON.parse(e);
        var html = '<option value="">Todo</option>';
        for (var i = 0; i < res.length; i++) {
          html += '<option value="' + res[i]["idgrupo_c"] + '">' + res[i]["idgrupo_c_name"] + '</option>';
        }
        $('#grupo').html(html);        
      },
      error: function (data) {
        console.log('ERROR, No se pudo conectar', data);
      }
    });
  }
});

$('#grupo').on('change', function () {
  var aMercado = $("#aMercado").val();
  var division = $('#division').val();
  var familia = $('#familia').val();
  var grupo = $('#grupo').val();
  console.log(division+"--"+aMercado+"--"+familia+"--"+grupo);
  if (division != null && aMercado != null && familia != null ) {
    $.ajax({
      type: 'GET',
      url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial',
      datatype: 'json',
      data: {
        division: division,
        aMercado: aMercado,
        familia: familia,
        grupo: grupo,
        filtro: "subgrupo"
      },
      async: false,
      success: function (e) {
        var res = JSON.parse(e);
        var html = '<option value="">Todo</option>';
        for (var i = 0; i < res.length; i++) {
          html += '<option value="' + res[i]["idsubgrupo_c"] + '">' + res[i]["idsubgrupo_c_name"] + '</option>';
        }
        $('#subgrupo').html(html);        
      },
      error: function (data) {
        console.log('ERROR, No se pudo conectar', data);
      }
    });
  }
});


$("#buscar").on("click", function(event){
    var aMercado = $("#aMercado").val();
    var division = $('#division').val();
    var familia = $('#familia').val();
    var grupo = $('#grupo').val();
    var crecimiento = $('#crecimiento').val();
    console.log(division,aMercado,familia,grupo,crecimiento);
    obtenerInformacion(division,aMercado,familia,grupo,crecimiento);
});

/*Exporta la tabla en Excel*/
$("#btnExportar").on("click",function(){
    let text = "Esta seguro que desea exportar a excel?, esto tomar unos minutos.";
	if (confirm(text) == true) {		
		//downloadExcel($("#aMercado").val());
        var aMercado = $("#aMercado").val();
        var division = $('#division').val();
        var familia = $('#familia').val();
        var grupo = $('#grupo').val();
        var crecimiento = $('#crecimiento').val();
        window.open('index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial&division='+division+'&aMercado='+aMercado+'&familia='+familia+'&grupo='+grupo+'&crecimiento='+crecimiento+'&filtro=2','','');
	} else {
		console.log("You canceled! Export excel");
	}    
});

function obtenerInformacion(division,aMercado,familia,grupo,crecimiento){
$("#mostrarDatos").html("<div id='cargando' class='loader'></div> ");
  $.ajax({
  type: 'get',
  url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial',
  data: {
        aMercado:aMercado,
        division:division,
        familia:familia,
        grupo:grupo,   
        crecimiento: crecimiento,   
        filtro:1,
  },
  success: function(data) {
        var productos = $.parseJSON(data);
        $("#cargando").remove();
        console.log(productos);
        var data = [];

        var am = [];
        var amobj = [];
        var fm = [];
        var fmobj = [];
        var gr = [];
        var grobj = [];
        var sg = [];
        var sgobj = [];

        var contador = 0;
        for (var i = 0; i < productos.length; i++) {

            if(am.includes( productos[i]["AreaMercado"] )){
                
            }else{
                data[contador] = [
                    productos[i]["AreaMercado"],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                am.push(productos[i]["AreaMercado"]);
                amobj.push(contador + 1);
                contador++
            }

            if(fm.includes(productos[i]["AreaMercado"] + productos[i]["Familia"])){
               
            }else{
               data[contador] = [
                    productos[i][""],
                    productos[i]["Familia"],                    
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                fm.push(productos[i]["AreaMercado"] + productos[i]["Familia"]);
                fmobj.push(contador + 1);
                contador++
            }

            if(gr.includes(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"])){
               
            }else{
                data[contador] = [
                    productos[i][""],
                    productos[i][""],
                    productos[i]["Grupo"],                    
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                gr.push(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"]);
                grobj.push(contador + 1);
                contador++
            }

            if(sg.includes(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"]  + productos[i]["SubGrupo"])){
                
            }else{
                data[contador] = [
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i]["SubGrupo"],                    
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                sg.push(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"]  + productos[i]["SubGrupo"]);
                sgobj.push(contador + 1);
                contador++
            }
            
            data[contador] = [
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    '<a href="index.php?module=SCO_ProductosCompras&action=DetailView&record='+productos[i]["IdProducoCompra"]+'") target="_blank">' + productos[i]["IdProducto"] + '</a>',
                    productos[i]["CodigoProveedor"],
                    productos[i]["Producto"],
                    productos[i]["IndicadorABC"],
                    productos[i]["EstadoMaterial"],
                    productos[i]["Procedencia"],
                    productos[i]["ProductoEstrella"],
                    productos[i]["Observaciones"],
                    //'<a style="cursor: pointer;" onclick=modalObservaciones("' + productos[i]["Observaciones"] + '")>' + productos[i]["Observaciones"] + '<a/>',
                    productos[i]["PrecioVta"],
                    '<a style="cursor: pointer;" onclick=showModal("' + productos[i]["IdProducto"] + '","' + productos[i]["PrecioFobNegociado"] + '")>' + productos[i]["PrecioFob"] + '</a>',
                    productos[i]["PrecioFobNegociado"],
                    productos[i]["SaldoStock"],
                    productos[i]["StockRango180"],
                    productos[i]["SalidaAutorizada"],
                    productos[i]["IngresoPedMesActual"],
                    productos[i]["totalIngresoMesActual"],
                    productos[i]["IngresoPedMesActualMasUno"],
                    productos[i]["IngresoPedMesActualMasdos"],
                    productos[i]["PendPorEnviar"],
                    productos[i]["PedConfirmFabrica"],     
                    productos[i]["VentaCantidad3AnioAtras"],
                    productos[i]["VentaCantidad2AnioAtras"],
                    productos[i]["VentaCantidad1AnioAtras"],                    
                    productos[i]["VentaCantidad0AnioAtras"],
                    productos[i]["Promedio1AtrasAnio"],
                    productos[i]["Promedio0AtrasAnio"],
                    productos[i]["StockDispMasDosMeses"],
                    productos[i]["MesStock"],
                    productos[i]["PedidoSugerido"],
                    Math.round((productos[i]["PrecioFob"] * productos[i]["PedidoSugerido"]))                    
                    ]
            contador++
        }
        console.log(amobj);
        console.log(fmobj);
        console.log(grobj);
        console.log(sgobj);
        var fecha = new Date();
        var options = { year: 'numeric', month: 'long'};
        var optionYear = {year:'numeric'}

        var masUnMes_fecha = new Date(fecha.getFullYear(), (fecha.getMonth()+1), fecha.getDate());
        var masDoMes_fecha = new Date(fecha.getFullYear(), (fecha.getMonth()+2), fecha.getDate());

        var anioActual = ((fecha.toLocaleDateString("es-ES", optionYear)));
        var anioActualMenosUno =((fecha.toLocaleDateString("es-ES", optionYear)) - 1);
        var anioActualMenosDos =((fecha.toLocaleDateString("es-ES", optionYear)) - 2);
        var anioActualMenosTres =((fecha.toLocaleDateString("es-ES", optionYear)) - 3);

        var mesActual = fecha.toLocaleDateString("es-ES", options)
        var sumaUnMes = masUnMes_fecha.toLocaleDateString("es-ES", options)
        var sumaDosMes = masDoMes_fecha.toLocaleDateString("es-ES", options)
        jexcel(document.getElementById('mostrarDatos'), {
            data:data,
            tableOverflow:true,
            columnSorting:true,
            csvHeaders:true,
            search:true,
            tableWidth: '100%',
            tableHeight: '500px',
            //lazyLoading:true,
            //loadingSpin:true,
            //filters: true,
            allowComments:true,
            pagination: 100,
            freezeColumns: 7,
            columns: [
                { 
                    type: 'text', 
                    title:'AreaMercado', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Familia', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Grupo', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'SubGrupo', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'html', 
                    title:'CodigoSap', 
                    width:100,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Codigo Proveedor', 
                    width:100,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Descripcion', 
                    width:250,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Indicador ABC',
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Estado material',
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Procedencia', 
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Producto Estrella', 
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'html', 
                    title:'Observaciones', 
                    width:150,
                    readOnly:true,
                },
                { 
                    type: 'text',
                    title:'Precio Vta USD',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'html',
                    title:'Precio FOB',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'text',
                    title:'Precio FOB Negociado',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Saldo Stock Disp',
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Stock Rango >180 ',
                    width:50,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'SA',
                    width:50,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Ingre. Pedido ' + mesActual,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Total disp. '+ mesActual,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title: 'Ingre. Pedido '+ sumaUnMes,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Ingre. Pedido ' + sumaDosMes,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Pend. Por Enviar (BackOrder)',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Pedidos Confirm Fabrica',
                    width:60,
                    readOnly:true,
                },
                
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosTres,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosDos,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosUno ,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActual,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Mensual Promed ' + anioActualMenosUno ,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Mensual Promed ' + anioActual,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Stock Dispon 90 días',
                    width:80,
                    readOnly:true,
                }, 
                { 
                    type: 'numeric',
                    title:'Meses Stock',
                    width:80,
                    readOnly:true,
                },                
                {
                    type: 'numeric',
                    title:'Pedido Sugerido',
                    width:80,
                    readOnly:true,
                },
                 {
                    type: 'numeric',
                    title:'Inversion requerida (FOB)',
                    width:80,
                    mask:'$ #,###',
                    readOnly:true,
                },                
             ],

             text:{
                noRecordsFound: 'No se encontraron resultados',
                showingPage: 'Pagina {0} de {1}',
                show: 'Mostrar ',
                search: 'Buscar',
                entries: ' Entradas',
                columnName: 'Nombre Columna',
                insertANewColumnBefore: 'Insertar una nueva columna antes',
                insertANewColumnAfter: 'Insert a new column after',
                deleteSelectedColumns: 'Delete selected columns',
                renameThisColumn: 'Rename this column',
                orderAscending: 'Order ascending',
                orderDescending: 'Order descending',
                insertANewRowBefore: 'Insert a new row before',
                insertANewRowAfter: 'Insert a new row after',
                deleteSelectedRows: 'Delete selected rows',
                editComments: 'Edit comments',
                addComments: 'Add comments',
                comments: 'Comments',
                clearComments: 'Clear comments',
                copy: 'Copy...',
                paste: 'Paste...',
                saveAs: 'Save as...',
                about: 'About',
                areYouSureToDeleteTheSelectedRows: 'Are you sure to delete the selected rows?',
                areYouSureToDeleteTheSelectedColumns: 'Are you sure to delete the selected columns?',
                thisActionWillDestroyAnyExistingMergedCellsAreYouSure: 'This action will destroy any existing merged cells. Are you sure?',
                thisActionWillClearYourSearchResultsAreYouSure: 'This action will clear your search results. Are you sure?',
                thereIsAConflictWithAnotherMergedCell: 'There is a conflict with another merged cell',
                invalidMergeProperties: 'Invalid merged properties',
                cellAlreadyMerged: 'Cell already merged',
                noCellsSelected: 'No cells selected',
            },
        })
        //COLOR EN CELDAS
        for(var i = 0; data.length >= i; i++) { 
            $('#mostrarDatos').jexcel('setStyle', 'Q'+i, 'background-color', '#FFBE41');
            $('#mostrarDatos').jexcel('setStyle', 'F'+i, 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'F'+i, 'border-bottom', '0.5px solid #4472C4');

            $('#mostrarDatos').jexcel('setStyle', 'M'+i, 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'M'+i, 'border-bottom', '0.5px solid #4472C4');

            $('#mostrarDatos').jexcel('setStyle', 'E'+i, 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'F'+i, 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'G'+i, 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'H'+i, 'text-align', 'left');

            $('#mostrarDatos').jexcel('setStyle', 'AG'+i, 'background-color', '#ffeb3b');
        }

        for(var i = 0; i < amobj.length ; i++) {
            $('#mostrarDatos').jexcel('setStyle', 'A'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'B'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'C'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'D'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'E'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'F'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'G'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'H'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'I'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'J'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'K'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'L'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'M'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'N'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'O'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'P'+amobj[i], 'background-color', '#203764');

            $('#mostrarDatos').jexcel('setStyle', 'A'+amobj[i], 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'A'+amobj[i], 'color', '#FFFFFF');
            $('#mostrarDatos').jexcel('setStyle', 'F'+amobj[i], 'border-bottom', 'NONE');
            $('#mostrarDatos').jexcel('setStyle', 'H'+amobj[i], 'border-bottom', 'none');
        }
        for(var i = 0; fmobj.length > i; i++) {

            $('#mostrarDatos').jexcel('setStyle', 'B'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'C'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'D'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'E'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'F'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'G'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'H'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'I'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'J'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'K'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'L'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'M'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'N'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'O'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'P'+fmobj[i], 'background-color', '#8EA9DB');

            $('#mostrarDatos').jexcel('setStyle', 'F'+fmobj[i], 'border-bottom', 'none');
            $('#mostrarDatos').jexcel('setStyle', 'H'+fmobj[i], 'border-bottom', 'none');
        }
        for(var i = 0; grobj.length > i; i++) {            
            $('#mostrarDatos').jexcel('setStyle', 'C'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'D'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'E'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'F'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'G'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'H'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'I'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'J'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'K'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'L'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'M'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'N'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'O'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'P'+grobj[i], 'background-color', '#D9E1F2');

            $('#mostrarDatos').jexcel('setStyle', 'F'+grobj[i], 'border-bottom', 'none');
            $('#mostrarDatos').jexcel('setStyle', 'H'+grobj[i], 'border-bottom', 'none');
        }
        for(var i = 0; sgobj.length > i; i++) {            
            $('#mostrarDatos').jexcel('setStyle', 'D'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'E'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'F'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'G'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'H'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'I'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'J'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'K'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'L'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'M'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'N'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'O'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'P'+sgobj[i], 'background-color', '#DDEBF7');

            $('#mostrarDatos').jexcel('setStyle', 'D'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'E'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'F'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'G'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'H'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'I'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'J'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'K'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'L'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'M'+sgobj[i], 'border-bottom', '1px solid #4472C4');
        }

    }
    });
}

/*
function obtenerInformacion(division,aMercado,familia,grupo,crecimiento){
$("#mostrarDatos").html("<div id='cargando' class='loader'></div> ");
  $.ajax({
  type: 'get',
  url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial',
  data: {
        aMercado:aMercado,
        division:division,
        familia:familia,
        grupo:grupo,   
        crecimiento: crecimiento,   
        filtro:1,
  },
  success: function(data) {
        var productos = $.parseJSON(data);
        $("#cargando").remove();
        console.log(productos);
        var data = [];

        var am = [];
        var amobj = [];
        var fm = [];
        var fmobj = [];
        var gr = [];
        var grobj = [];
        var sg = [];
        var sgobj = [];

        var contador = 0;
        for (var i = 0; i < productos.length; i++) {

            if(am.includes( productos[i]["AreaMercado"] )){
                
            }else{
                data[contador] = [
                    productos[i]["AreaMercado"],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                am.push(productos[i]["AreaMercado"]);
                amobj.push(contador + 1);
                contador++
            }

            if(fm.includes(productos[i]["AreaMercado"] + productos[i]["Familia"])){
               
            }else{
               data[contador] = [
                    productos[i][""],
                    productos[i]["Familia"],                    
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                fm.push(productos[i]["AreaMercado"] + productos[i]["Familia"]);
                fmobj.push(contador + 1);
                contador++
            }

            if(gr.includes(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"])){
               
            }else{
                data[contador] = [
                    productos[i][""],
                    productos[i][""],
                    productos[i]["Grupo"],                    
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                gr.push(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"]);
                grobj.push(contador + 1);
                contador++
            }

            if(sg.includes(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"]  + productos[i]["SubGrupo"])){
                
            }else{
                data[contador] = [
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i]["SubGrupo"],                    
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""]
                    ];
                sg.push(productos[i]["AreaMercado"] + productos[i]["Familia"] + productos[i]["Grupo"]  + productos[i]["SubGrupo"]);
                sgobj.push(contador + 1);
                contador++
            }

            
            data[contador] = [
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i][""],
                    productos[i]["IdProducto"],
                    productos[i]["CodigoProveedor"],
                    productos[i]["Producto"],
                    productos[i]["IndicadorABC"],
                    productos[i]["EstadoMaterial"],
                    productos[i]["PrecioVta"],
                    productos[i]["PrecioFob"],
                    productos[i]["SaldoStock"],
                    productos[i]["StockRango180"],
                    productos[i]["SalidaAutorizada"],
                    productos[i]["IngresoPedMesActual"],
                    productos[i]["totalIngresoMesActual"],
                    productos[i]["IngresoPedMesActualMasUno"],
                    productos[i]["IngresoPedMesActualMasdos"],
                    productos[i]["PendPorEnviar"],
                    productos[i]["PedConfirmFabrica"],     
                    productos[i]["VentaCantidad3AnioAtras"],
                    productos[i]["VentaCantidad2AnioAtras"],
                    productos[i]["VentaCantidad1AnioAtras"],                    
                    productos[i]["VentaCantidad0AnioAtras"],
                    productos[i]["Promedio1AtrasAnio"],
                    productos[i]["Promedio0AtrasAnio"],
                    productos[i]["StockDispMasDosMeses"],
                    productos[i]["MesStock"],
                    productos[i]["PedidoSugerido"],
                    Math.round((productos[i]["PrecioFob"] * productos[i]["PedidoSugerido"]))                    
                    ]
            contador++
        }
        console.log(amobj);
        console.log(fmobj);
        console.log(grobj);
        console.log(sgobj);
        var fecha = new Date();
        var options = { year: 'numeric', month: 'long'};
        var optionYear = {year:'numeric'}

        var masUnMes_fecha = new Date(fecha.getFullYear(), (fecha.getMonth()+1), fecha.getDate());
        var masDoMes_fecha = new Date(fecha.getFullYear(), (fecha.getMonth()+2), fecha.getDate());

        var anioActual = ((fecha.toLocaleDateString("es-ES", optionYear)));
        var anioActualMenosUno =((fecha.toLocaleDateString("es-ES", optionYear)) - 1);
        var anioActualMenosDos =((fecha.toLocaleDateString("es-ES", optionYear)) - 2);
        var anioActualMenosTres =((fecha.toLocaleDateString("es-ES", optionYear)) - 3);

        var mesActual = fecha.toLocaleDateString("es-ES", options)
        var sumaUnMes = masUnMes_fecha.toLocaleDateString("es-ES", options)
        var sumaDosMes = masDoMes_fecha.toLocaleDateString("es-ES", options)
        jexcel(document.getElementById('mostrarDatos'), {
            data:data,
            tableOverflow:true,
            columnSorting:true,
            csvHeaders:true,
            search:true,
            tableWidth: '100%',
            tableHeight: '500px',
            //lazyLoading:true,
            //loadingSpin:true,
            //filters: true,
            allowComments:true,
            pagination: 100,
            freezeColumns: 13,
            columns: [
                { 
                    type: 'text', 
                    title:'AreaMercado', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Familia', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Grupo', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'SubGrupo', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'CodigoSap', 
                    width:100,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Codigo Proveedor', 
                    width:100,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Descripcion', 
                    width:250,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Indicador ABC',
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Estado material',
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'text',
                    title:'Precio Vta USD',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'text',
                    title:'Precio FOB',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Saldo Stock Disp',
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Stock Rango >180 ',
                    width:50,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'SA',
                    width:50,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Ingre. Pedido ' + mesActual,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Total disp. '+ mesActual,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title: 'Ingre. Pedido '+ sumaUnMes,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Ingre. Pedido ' + sumaDosMes,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Pend. Por Enviar (BackOrder)',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Pedidos Confirm Fabrica',
                    width:60,
                    readOnly:true,
                },
                
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosTres,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosDos,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosUno ,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActual,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Mensual Promed ' + anioActualMenosUno ,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Mensual Promed ' + anioActual,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Stock Dispon 90 días',
                    width:80,
                    readOnly:true,
                }, 
                { 
                    type: 'numeric',
                    title:'Meses Stock',
                    width:80,
                    readOnly:true,
                },                
                {
                    type: 'numeric',
                    title:'Pedido Sugerido',
                    width:80,
                    readOnly:true,
                },
                 {
                    type: 'numeric',
                    title:'Inversion requerida (FOB)',
                    width:80,
                    mask:'$ #,###',
                    readOnly:true,
                },                
             ],

             text:{
                noRecordsFound: 'No se encontraron resultados',
                showingPage: 'Pagina {0} de {1}',
                show: 'Mostrar ',
                search: 'Buscar',
                entries: ' Entradas',
                columnName: 'Nombre Columna',
                insertANewColumnBefore: 'Insertar una nueva columna antes',
                insertANewColumnAfter: 'Insert a new column after',
                deleteSelectedColumns: 'Delete selected columns',
                renameThisColumn: 'Rename this column',
                orderAscending: 'Order ascending',
                orderDescending: 'Order descending',
                insertANewRowBefore: 'Insert a new row before',
                insertANewRowAfter: 'Insert a new row after',
                deleteSelectedRows: 'Delete selected rows',
                editComments: 'Edit comments',
                addComments: 'Add comments',
                comments: 'Comments',
                clearComments: 'Clear comments',
                copy: 'Copy...',
                paste: 'Paste...',
                saveAs: 'Save as...',
                about: 'About',
                areYouSureToDeleteTheSelectedRows: 'Are you sure to delete the selected rows?',
                areYouSureToDeleteTheSelectedColumns: 'Are you sure to delete the selected columns?',
                thisActionWillDestroyAnyExistingMergedCellsAreYouSure: 'This action will destroy any existing merged cells. Are you sure?',
                thisActionWillClearYourSearchResultsAreYouSure: 'This action will clear your search results. Are you sure?',
                thereIsAConflictWithAnotherMergedCell: 'There is a conflict with another merged cell',
                invalidMergeProperties: 'Invalid merged properties',
                cellAlreadyMerged: 'Cell already merged',
                noCellsSelected: 'No cells selected',
            },
        })
        //COLOR EN CELDAS
        for(var i = 0; data.length >= i; i++) { 
            $('#mostrarDatos').jexcel('setStyle', 'M'+i, 'background-color', '#FFBE41');
            $('#mostrarDatos').jexcel('setStyle', 'F'+i, 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'F'+i, 'border-bottom', '0.5px solid #4472C4');

            $('#mostrarDatos').jexcel('setStyle', 'J'+i, 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'J'+i, 'border-bottom', '0.5px solid #4472C4');

            $('#mostrarDatos').jexcel('setStyle', 'E'+i, 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'F'+i, 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'G'+i, 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'H'+i, 'text-align', 'left');

        }

        for(var i = 0; i < amobj.length ; i++) {
            $('#mostrarDatos').jexcel('setStyle', 'A'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'B'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'C'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'D'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'E'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'F'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'G'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'H'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'I'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'J'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'K'+amobj[i], 'background-color', '#203764');
            $('#mostrarDatos').jexcel('setStyle', 'L'+amobj[i], 'background-color', '#203764');

            $('#mostrarDatos').jexcel('setStyle', 'A'+amobj[i], 'text-align', 'left');
            $('#mostrarDatos').jexcel('setStyle', 'A'+amobj[i], 'color', '#FFFFFF');
            $('#mostrarDatos').jexcel('setStyle', 'F'+amobj[i], 'border-bottom', 'NONE');
            $('#mostrarDatos').jexcel('setStyle', 'H'+amobj[i], 'border-bottom', 'none');
        }
        for(var i = 0; fmobj.length > i; i++) {

            $('#mostrarDatos').jexcel('setStyle', 'B'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'C'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'D'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'E'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'F'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'G'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'H'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'I'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'J'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'K'+fmobj[i], 'background-color', '#8EA9DB');
            $('#mostrarDatos').jexcel('setStyle', 'L'+fmobj[i], 'background-color', '#8EA9DB');

            $('#mostrarDatos').jexcel('setStyle', 'F'+fmobj[i], 'border-bottom', 'none');
            $('#mostrarDatos').jexcel('setStyle', 'H'+fmobj[i], 'border-bottom', 'none');
        }
        for(var i = 0; grobj.length > i; i++) {            
            $('#mostrarDatos').jexcel('setStyle', 'C'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'D'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'E'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'F'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'G'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'H'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'I'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'J'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'K'+grobj[i], 'background-color', '#D9E1F2');
            $('#mostrarDatos').jexcel('setStyle', 'L'+grobj[i], 'background-color', '#D9E1F2');

            $('#mostrarDatos').jexcel('setStyle', 'F'+grobj[i], 'border-bottom', 'none');
            $('#mostrarDatos').jexcel('setStyle', 'H'+grobj[i], 'border-bottom', 'none');
        }
        for(var i = 0; sgobj.length > i; i++) {            
            $('#mostrarDatos').jexcel('setStyle', 'D'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'E'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'F'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'G'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'H'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'I'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'J'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'K'+sgobj[i], 'background-color', '#DDEBF7');
            $('#mostrarDatos').jexcel('setStyle', 'L'+sgobj[i], 'background-color', '#DDEBF7');

            $('#mostrarDatos').jexcel('setStyle', 'D'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'E'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'F'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'G'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'H'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'I'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'J'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'K'+sgobj[i], 'border-bottom', '1px solid #4472C4');
            $('#mostrarDatos').jexcel('setStyle', 'L'+sgobj[i], 'border-bottom', '1px solid #4472C4');
        }

    }
    });
}
*/

function modalObservaciones(observaciones){
  alert("Observaciones:"+observaciones);
}

var options = {
    minDimensions:[10,10],
    tableOverflow:true,
}

var data = ['','','','','','','','','','','','','','','',''];
//dataJexcel(data);
//$('#spreadsheet').jexcel(options); 
jexcel(document.getElementById('mostrarDatos'), {
    data:data,
    tableOverflow:true,
    columnSorting:true,
    csvHeaders:true,
    search:true,
    tableWidth: '100%',
    tableHeight: '700px',
    //lazyLoading:true,
    //loadingSpin:true,
    //filters: true,
    allowComments:true,
    pagination: 50,
    freezeColumns: 6,
    columns: [
        { 
                    type: 'text', 
                    title:'AreaMercado', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Familia', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Grupo', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'SubGrupo', 
                    width:20,
                    readOnly:true,
                },
                { 
                    type: 'html', 
                    title:'CodigoSap', 
                    width:100,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Codigo Proveedor', 
                    width:100,
                    readOnly:true,
                },
                { 
                    type: 'text', 
                    title:'Descripcion', 
                    width:250,
                    readOnly:true,
                },
                { 
                    type: 'text',
                    title:'Precio Vta USD',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'text',
                    title:'Precio FOB',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Saldo Stock Disp',
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Stock Rango >180 ',
                    width:50,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'SA',
                    width:50,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Ingre. Pedido ' + mesActual,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Total disp. '+ mesActual,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title: 'Ingre. Pedido '+ sumaUnMes,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Ingre. Pedido ' + sumaDosMes,
                    width:60,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Pend. Por Enviar (BackOrder)',
                    width:70,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Pedidos Confirm Fabrica',
                    width:60,
                    readOnly:true,
                },
                
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosTres,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosDos,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActualMenosUno ,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Total ' + anioActual,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Mensual Promed ' + anioActualMenosUno ,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Venta Mensual Promed ' + anioActual,
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Stock Dispon 90 días',
                    width:80,
                    readOnly:true,
                }, 
                { 
                    type: 'numeric',
                    title:'Meses Stock',
                    width:80,
                    readOnly:true,
                },                
                {
                    type: 'numeric',
                    title:'Pedido Sugerido',
                    width:80,
                    readOnly:true,
                },
                 {
                    type: 'numeric',
                    title:'Inversion requerida (FOB)',
                    width:80,
                    mask:'$ #,###',
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Indicador ABC',
                    width:80,
                    readOnly:true,
                },
                { 
                    type: 'numeric',
                    title:'Estado material',
                    width:80,
                    readOnly:true,
                },
     ],
     text:{
        noRecordsFound: 'No se encontraron resultados',
        showingPage: 'Pagina {0} de {1}',
        show: 'Mostrar ',
        search: 'Buscar',
        entries: ' Entradas',
        columnName: 'Nombre Columna',
        insertANewColumnBefore: 'Insertar una nueva columna antes',
        insertANewColumnAfter: 'Insert a new column after',
        deleteSelectedColumns: 'Delete selected columns',
        renameThisColumn: 'Rename this column',
        orderAscending: 'Order ascending',
        orderDescending: 'Order descending',
        insertANewRowBefore: 'Insert a new row before',
        insertANewRowAfter: 'Insert a new row after',
        deleteSelectedRows: 'Delete selected rows',
        editComments: 'Edit comments',
        addComments: 'Add comments',
        comments: 'Comments',
        clearComments: 'Clear comments',
        copy: 'Copy...',
        paste: 'Paste...',
        saveAs: 'Save as...',
        about: 'About',
        areYouSureToDeleteTheSelectedRows: 'Are you sure to delete the selected rows?',
        areYouSureToDeleteTheSelectedColumns: 'Are you sure to delete the selected columns?',
        thisActionWillDestroyAnyExistingMergedCellsAreYouSure: 'This action will destroy any existing merged cells. Are you sure?',
        thisActionWillClearYourSearchResultsAreYouSure: 'This action will clear your search results. Are you sure?',
        thereIsAConflictWithAnotherMergedCell: 'There is a conflict with another merged cell',
        invalidMergeProperties: 'Invalid merged properties',
        cellAlreadyMerged: 'Cell already merged',
        noCellsSelected: 'No cells selected',
    },
})


function showModal(IdProducto, PrecioFobNegociado){
    //$("#mostrarDatos").html("<div id='cargando' class='loader'></div> ");
    ventanaModal(IdProducto,PrecioFobNegociado);  
    $('#modalOrdenCompra').modal('show');
}

//Estructura de la vista de la venta modal
function ventanaModal(IdProducto,PrecioFobNegociado){	
	    var htmlm = '';
	    htmlm += '<div class="modal fade" id="modalOrdenCompra" style="display: block;margin-top: 5%;">';
	    htmlm += '    <div class="modal-dialog">';
	    htmlm += '        <div class="modal-content">';
	    htmlm += '            <div class="modal-header" !important;">';
	    htmlm += '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	    htmlm += '                <h4 class="modal-title"> Variación de precios '+IdProducto+'</h4>';
	    htmlm += '            </div>';
	    htmlm += '            <div class="modal-body" >';
	    htmlm += '                <div class="panel panel-danger">';
    	htmlm += '                    <div class="panel-body"style="padding: 20px 30px;">';
    	htmlm += '						<iframe src="http://swarm.hansa.com.bo:3001/public/question/0c161145-a064-48bf-972d-5683ea340cb4?IdProducto='+IdProducto+'" frameborder="0" width="100%" height="600" allowtransparency></iframe>';
		htmlm += '					  </div>';
		htmlm += '				   </div>';
	    htmlm += '            </div>';
	    htmlm += '        </div>';
	    htmlm += '    </div>';
	    htmlm += '</div>';  
    $("#ventanaModal").html(htmlm);
}