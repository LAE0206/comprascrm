<?php

require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');
require '/var/www/phpexcel/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

Class Plantillaexcel{

	function plantilla($division,$aMercado,$familia,$grupo,$crecimiento){
    
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    
    #TIPO DE LETRA Y TAMAÑO DE LETRA
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);

		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    
    #TITULO
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'PLANILLA PEDIDOS ');
		#CABECERA 
    $mes_actual = date("M");
    $anio_actual = date("Y");
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'AM');
		$objPHPExcel->getActiveSheet()->setCellValue('B2', 'FM');
		$objPHPExcel->getActiveSheet()->setCellValue('C2', 'GR');
		$objPHPExcel->getActiveSheet()->setCellValue('D2', 'SGr');
		$objPHPExcel->getActiveSheet()->setCellValue('E2', 'Codigo SAP-AIO');
		$objPHPExcel->getActiveSheet()->setCellValue('F2', 'Codigo Proveedor');
		$objPHPExcel->getActiveSheet()->setCellValue('G2', 'Producto Descripción');
		$objPHPExcel->getActiveSheet()->setCellValue('H2', 'Indicador ABC');	
		$objPHPExcel->getActiveSheet()->setCellValue('I2', 'Estado Material');	
		$objPHPExcel->getActiveSheet()->setCellValue('J2', 'Procedencia');	
		$objPHPExcel->getActiveSheet()->setCellValue('K2', 'Producto Estrella');	
		$objPHPExcel->getActiveSheet()->setCellValue('L2', 'Observaciones');	
		$objPHPExcel->getActiveSheet()->setCellValue('M2', 'Precio Vta');
		$objPHPExcel->getActiveSheet()->setCellValue('N2', 'Precio Fob');
    $objPHPExcel->getActiveSheet()->setCellValue('O2', 'Precio Fob Negociado');
		$objPHPExcel->getActiveSheet()->setCellValue('P2', 'Saldo Stock');
		$objPHPExcel->getActiveSheet()->setCellValue('Q2', 'Stock Rango > 180');
		$objPHPExcel->getActiveSheet()->setCellValue('R2', 'Salida Autorizada');
		$objPHPExcel->getActiveSheet()->setCellValue('S2', 'Ingre. Pedido '.date("M",strtotime($mes_actual."+ 0 month")).'. '.date("Y"));
		$objPHPExcel->getActiveSheet()->setCellValue('T2', 'Total disp. '.date("M",strtotime($mes_actual."+ 0 month")).'. '.date("Y"));
		$objPHPExcel->getActiveSheet()->setCellValue('U2', 'Ingre. Pedido '.date("M",strtotime($mes_actual."+ 1 month")).'. '.date("Y"));     
    switch (date("M",strtotime($mes_actual."+ 2 month"))) {
    case 'Jan':
        $objPHPExcel->getActiveSheet()->setCellValue('V2', 'Ingre. Pedido '.date("M",strtotime($mes_actual."+ 2 month")).'. '.date("Y",strtotime($anio_actual."+ 1 year")));
        break;
    case 'Feb':
        $objPHPExcel->getActiveSheet()->setCellValue('V2', 'Ingre. Pedido '.date("M",strtotime($mes_actual."+ 2 month")).'. '.date("Y",strtotime($anio_actual."+ 1 year")));
        break;
    default:
        $objPHPExcel->getActiveSheet()->setCellValue('V2', 'Ingre. Pedido '.date("M",strtotime($mes_actual."+ 2 month")).'. '.date("Y"));
        break;
    } 
		$objPHPExcel->getActiveSheet()->setCellValue('W2', 'Pend. Por Enviar (BackOrder)');
		$objPHPExcel->getActiveSheet()->setCellValue('X2', 'Pedidos Confirm Fabrica');
		$objPHPExcel->getActiveSheet()->setCellValue('Y2', 'Venta Total '.date("Y",strtotime($anio_actual."- 3 year")));
		$objPHPExcel->getActiveSheet()->setCellValue('Z2', 'Venta Total '.date("Y",strtotime($anio_actual."- 2 year")));
		$objPHPExcel->getActiveSheet()->setCellValue('AA2', 'Venta Total '.date("Y",strtotime($anio_actual."- 1 year")));
		$objPHPExcel->getActiveSheet()->setCellValue('AB2', 'Venta Total '.date("Y",strtotime($anio_actual."+ 0 year")));
		$objPHPExcel->getActiveSheet()->setCellValue('AC2', 'Venta mensual promedio '.date("Y",strtotime($anio_actual."- 1 year")));
		$objPHPExcel->getActiveSheet()->setCellValue('AD2', 'Venta mensual promedio '.date("Y",strtotime($anio_actual."- 0 year")));
		$objPHPExcel->getActiveSheet()->setCellValue('AE2', 'Stock Dispo 90 dias ');
		$objPHPExcel->getActiveSheet()->setCellValue('AF2', 'Meses Stock');
		#$objPHPExcel->getActiveSheet()->setCellValue('AE2', 'Pedido Autorizado');	
		$objPHPExcel->getActiveSheet()->setCellValue('AG2', 'Pedido Autorizado');
		$objPHPExcel->getActiveSheet()->setCellValue('AH2', 'Pedido Sugerido');	
		$objPHPExcel->getActiveSheet()->setCellValue('AI2', 'Inversion requerida');		
    #FILTROS
		$objPHPExcel->getActiveSheet()->setAutoFilter('E2:F2');
  	#DATOS
		$contador = 2;
		$styleArray = array(
		    'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'top' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'left' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'right' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
		    ),		    
        'font'  => array(
          'bold'  => true,
          'color' => array('rgb' => '000000')
        ),
         'alignment'  => array(
          'horizontal'  =>  PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )   
          
		);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('M'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('N'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('O'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('P'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('Q'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('R'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('S'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('T'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('U'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('V'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('W'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('X'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('Y'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('Z'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AA'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AB'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AC'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AD'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AE'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AF'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AH'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('AI'.$contador)->applyFromArray($styleArray);
    #ANCHO DE CELDA
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(12); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(12); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(10); 

     
    $styleArray = array(
		    'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'top' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'left' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'right' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
		    ),
        'alignment'  => array(
          'horizontal'  =>  PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )   
		); 
     
    $styleArray2 = array(		    
        'alignment'  => array(
          'horizontal'  =>  PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        )    
		);
   
   $styleArray3 = array(		    
        'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '4472C4')
		        ),
		    ),		          
		);
   
   #DATOS A LISTAR
    $query = "call suitecrm.sp_ordencompra_rep01('".$division."','".$aMercado."','".$familia."','".$grupo."','".$crecimiento."');";
	    $results = $GLOBALS['db']->query($query, true);
	    $object= array();
		$arreglo = array();	
		
		$am = array();
		$fm = array();
		$gr = array();
		$sg = array();
		$contador = 2;
   
    #PONIENDO ESTILOS A LAS CELDAS
    while($row = $GLOBALS['db']->fetchByAssoc($results))
        {
					$contador++;
					if(in_array($row['AreaMercado'], $am)) {
				}else{
					array_push($am, $row['AreaMercado']);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$contador, $row['AreaMercado']);

					$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFF85C');
					$objPHPExcel->getActiveSheet()->getStyle('L'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFBE41');

					$objPHPExcel->getActiveSheet()->getStyle('A'.$contador.':O'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('203764');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$contador)->getFont()->getColor()->setARGB('FFFFFF');
					$contador++;
				}

				if(in_array($row['AreaMercado'].$row['Familia'], $fm)) {
					
				}else{
					array_push($fm, $row['AreaMercado'].$row['Familia']);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$contador, $row['Familia']);

					$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFF85C');
					$objPHPExcel->getActiveSheet()->getStyle('P'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFBE41');

					$objPHPExcel->getActiveSheet()->getStyle('B'.$contador.':O'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('8EA9DB');
					$contador++;
				}

				if(in_array($row['AreaMercado'].$row['Familia'].$row['Grupo'], $gr)) {
					
				}else{
					array_push($gr, $row['AreaMercado'].$row['Familia'].$row['Grupo']);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$contador, $row['Grupo']);

					$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFF85C');
					$objPHPExcel->getActiveSheet()->getStyle('P'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFBE41');

					$objPHPExcel->getActiveSheet()->getStyle('C'.$contador.':O'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('D9E1F2');
					$contador++;
				}

				if(in_array($row['AreaMercado'].$row['Familia'].$row['Grupo'].$row['SubGrupo'], $sg)) {

				}else{
					array_push($sg, $row['AreaMercado'].$row['Familia'].$row['Grupo'].$row['SubGrupo']);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$contador, $row['SubGrupo']);

					$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFF85C');	
					$objPHPExcel->getActiveSheet()->getStyle('P'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFBE41');

					$objPHPExcel->getActiveSheet()->getStyle('D'.$contador.':O'.$contador)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('DDEBF7');
					$objPHPExcel->getActiveSheet()->getStyle('D'.$contador.':J'.$contador)->applyFromArray($styleArray3);
					$contador++;
				}

				#$sheet->setCellValue('A'.$contador, $row['AreaMercado']);
				#$sheet->setCellValue('B'.$contador, $row['Familia']);
				#$sheet->setCellValue('C'.$contador, $row['Grupo']);
				#$sheet->setCellValue('D'.$contador, $row['SubGrupo']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$contador, $row['IdProducto']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$contador, $row['CodigoProveedor']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$contador, $row['Producto']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$contador, $row['IndicadorABC']);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$contador, $row['EstadoMaterial']);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$contador, $row['Procedencia']);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$contador, $row['ProductoEstrella']);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$contador, $row['Observaciones']);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$contador, $row['PrecioVta']);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$contador, $row['PrecioFob']);
        $objPHPExcel->getActiveSheet()->setCellValue('O'.$contador, $row['PrecioFobNegociado']);
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$contador, $row['SaldoStock']);
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$contador, $row['StockRango180']);
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$contador, $row['SalidaAutorizada']);
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$contador, $row['IngresoPedMesActual']);
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$contador, $row['totalIngresoMesActual']);
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$contador, $row['IngresoPedMesActualMasUno']);
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$contador, $row['IngresoPedMesActualMasdos']);
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$contador, $row['PendPorEnviar']);
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$contador, $row['PedConfirmFabrica']);
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$contador, $row['VentaCantidad3AnioAtras']);
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$contador, $row['VentaCantidad2AnioAtras']);
				$objPHPExcel->getActiveSheet()->setCellValue('AA'.$contador, $row['VentaCantidad1AnioAtras']);
				$objPHPExcel->getActiveSheet()->setCellValue('AB'.$contador, $row['VentaCantidad0AnioAtras']);
				$objPHPExcel->getActiveSheet()->setCellValue('AC'.$contador, $row['Promedio1AtrasAnio']);
				$objPHPExcel->getActiveSheet()->setCellValue('AD'.$contador, $row['Promedio0AtrasAnio']);
				$objPHPExcel->getActiveSheet()->setCellValue('AE'.$contador, $row['StockDispMasDosMeses']);
				$objPHPExcel->getActiveSheet()->setCellValue('AF'.$contador, $row['MesStock']);
				$objPHPExcel->getActiveSheet()->setCellValue('AH'.$contador, $row['PedidoSugerido']);				
				$objPHPExcel->getActiveSheet()->setCellValue('AI'.$contador, (round($row['PrecioFob'] * $row['PedidoSugerido'])));
				
        
  		  $objPHPExcel->getActiveSheet()->getStyle('A'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$contador)->applyFromArray($styleArray2);

				$objPHPExcel->getActiveSheet()->getStyle('L'.$contador)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('M'.$contador)->applyFromArray($styleArray2);
        $objPHPExcel->getActiveSheet()->getStyle('N'.$contador)->applyFromArray($styleArray2);
        
        		#$objPHPExcel->getActiveSheet()->getStyle('J'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$contador)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('L'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('M'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('O'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('P'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('Q'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('R'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('S'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('T'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('U'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('V'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('W'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('X'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('Y'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('Z'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AA'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AB'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AC'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AD'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AE'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AF'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AH'.$contador)->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('AI'.$contador)->applyFromArray($styleArray);

        
				$objPHPExcel->getActiveSheet()->getStyle('F'.$contador)->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setARGB('D9E1F2');
				$objPHPExcel->getActiveSheet()->getStyle('M'.$contador)->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setARGB('D9E1F2');
				$objPHPExcel->getActiveSheet()->getStyle('P'.$contador)->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setARGB('FFBE41');
				$objPHPExcel->getActiveSheet()->getStyle('AG'.$contador)->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setARGB('FFF85C');

				$objPHPExcel->getActiveSheet()->getStyle('F'.$contador)->applyFromArray($styleArray3);
				$objPHPExcel->getActiveSheet()->getStyle('M'.$contador)->applyFromArray($styleArray3);
    }
    
    #FONDO DE COLOR EN CELDA
		$objPHPExcel->getActiveSheet()->getStyle('A2:AI2')->getFill()
    	->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()->setARGB('D9E1F2');
		$objPHPExcel->getActiveSheet()->getStyle('Y2:AB2')->getFill()
    	->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()->setARGB('C4E0B8');


		$objPHPExcel->getActiveSheet()->getStyle('P2')->getFill()
    	->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    	->getStartColor()->setARGB('FFBE41');

    	$objPHPExcel->getActiveSheet()->getStyle('AG2')->getFill()
    	->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    	->getStartColor()->setARGB('FFF85C');

    	#ALTURA DE CELDAReporte de Pedidos de Órdenes de Compra Div01 modificación
		$objPHPExcel->getActiveSheet()->getStyle('A2:AI2')
		->getAlignment()->setWrapText(true);

    
    $filename = 'Pedido'.date("M",strtotime($mes_actual."+ 0 month")).''.date("Y",strtotime($anio_actual."+ 0 year")).'.xlsx';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: cache, must-revalidate'); 
    header('Pragma: public'); 
    
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel, 'Xlsx');
    #$objWriter->save('some_excel_file.xlsx');
    
    #$writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
    $objWriter->save('php://output');
	}

	function plantillaPreciosFob(){
		$objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    
    #TIPO DE LETRA Y TAMAÑO DE LETRA
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

		#$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		// Establecer el formato de las columnas como texto
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getNumberFormat()->setFormatCode('@');
		$objPHPExcel->getActiveSheet()->getStyle('B1')->getNumberFormat()->setFormatCode('@');
		$objPHPExcel->getActiveSheet()->getStyle('C1')->getNumberFormat()->setFormatCode('@');
    
    #TITULO
		#$objPHPExcel->getActiveSheet()->setCellValue('A1', 'PLANILLA PRORRATEO ');
		#CABECERA 
    $mes_actual = date("M");
    $anio_actual = date("Y");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'IdProducto');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'PrecioFobNegociado');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Procedencia');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ProductoEstrella');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Observaciones');

		// Luego, puedes establecer los valores en las celdas como cadenas de texto
		$objPHPExcel->getActiveSheet()->setCellValue('A2', '');
		$objPHPExcel->getActiveSheet()->setCellValue('B2', '');
		$objPHPExcel->getActiveSheet()->setCellValue('C2', '');	
		$objPHPExcel->getActiveSheet()->setCellValue('D2', '');
		$objPHPExcel->getActiveSheet()->setCellValue('E2', '');

  	#DATOS
		$contador = 2;
		$styleArray = array(
		    'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'top' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'left' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
          'right' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN
		        ),
		    ),		    
        'font'  => array(
          'bold'  => true,
          'color' => array('rgb' => '000000')
        ),
         'alignment'  => array(
          'horizontal'  =>  PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )   
		);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$contador)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$contador)->applyFromArray($styleArray);

    #ANCHO DE CELDA
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);	

    $filename = 'PlantillaProductosFob.xlsx';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: cache, must-revalidate'); 
    header('Pragma: public'); 
    
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel, 'Xlsx');
    #$objWriter->save('some_excel_file.xlsx');
    
    #$writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
    $objWriter->save('php://output');
	}
}