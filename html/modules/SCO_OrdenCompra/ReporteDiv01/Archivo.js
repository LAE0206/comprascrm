// Primero supervise los cambios del cuadro de entrada, seleccione un nuevo archivo que activará el evento de cambio
//document.querySelector("#miArchivoExcel").addEventListener("change", function () {
function btnExportarExportar(){
	window.open('index.php?to_pdf=true&module=SCO_OrdenCompra&action=ReporteGerencial&filtro=0');
}
	
document.getElementById("miArchivoExcel").addEventListener("change", function () {
    // Obtener el archivo seleccionado
	$("#mostrarDatosExcel").html("<div id='cargando' class='loader'></div> ");
	$("#cargando").remove();
	$("#cantidadTotal").remove();
	$("#guardarDatos").remove();
	$("#respuestaProductosPrecioFob").remove();
	$("#respuestaCantidad").remove();
	var file = document.querySelector("#miArchivoExcel").files[0];
	var type = file.name.split('.');
	if (type[type.length - 1] !== 'xlsx' && type[type.length - 1] !== 'xls') {
		alert('Archivo no válido, seleccione un archivo válido en formato excel.');
		//return false;
	}else{				
		const reader = new FileReader();
		reader.readAsBinaryString(file);		
		reader.onload = (e) => {
			const datos = e.target.result;
			const zzexcel = window.XLS.read(datos, {
				type: 'binary'
			});
			const result = [];			
			for (let i = 0; i < zzexcel.SheetNames.length; i++) {
				const newData = window.XLS.utils.sheet_to_json(zzexcel.Sheets[zzexcel.SheetNames[i]]);
				result.push(...newData)
			}
			console.log('result', result)
			var datosEnviarWS = btoa(JSON.stringify(result));
		    console.log("BASE64 ENCODE de enviar datos al web service "+ datosEnviarWS);
			console.log("BASE64 DESCODE "+ atob(datosEnviarWS))
			var totalRegistros = result.length;
			$("#miArchivoExcel").after("<br><div id='cantidadTotal'><div style='font-size: 17px; font-weight: 700;' > Cantidad de registros encontrados <b class='badge badge-info'>" + totalRegistros + "</b ></div><br><div>Antes de enviar a actualizar todos los productos, verifique toda su información.</div></div> ");
			$("#cantidadTotal").after("<br><button type='button' class='btn btn-sm btn-info' id='guardarDatos' onclick='guardarDatos(\""+datosEnviarWS+ "\")' ><i class='glyphicon glyphicon-upload'></i> Enviar a guardar</button>");
			//console.log("Cantidad total de registros "+ totalRegistros)
			var data = [];
			result.forEach(element => {
				//console.log("IdProducto: "+ element.__rowNum__);
				data[(element.__rowNum__ - 1)] = [
                    element.IdProducto,
                    element.PrecioFobNegociado,
                    element.Procedencia,
                    element.ProductoEstrella,
                    element.Observaciones,
                    ]			
			});
			//console.log("DATOS: "+ data)
			jexcel(document.getElementById('mostrarDatosExcel'), {
				data:data,
				tableOverflow:true,
				columnSorting:true,
				csvHeaders:true,
				search:true,
				tableWidth: '100%',
				tableHeight: '300px',
				//lazyLoading:true,
				//loadingSpin:true,
				//filters: true,
				allowComments:true,
				pagination: 10,
				//freezeColumns: 14,
				columns: [
					{
						type: 'text', 
						title:'IdProducto', 
						width:80,
						readOnly:true,
					},
					{
						type: 'text', 
						title:'PrecioFobNegociado', 
						width:110,
						readOnly:true,
					},
					{
						type: 'text', 
						title:'Procedencia', 
						width:100,
						readOnly:true,
					},
					{
						type: 'text', 
						title:'Producto Estrella', 
						width:120,
						readOnly:true,
					},
					{
						type: 'text', 
						title:'Observaciones', 
						width:180,
						readOnly:true,
					}
				 ],
	
				 text:{
					noRecordsFound: 'No se encontraron resultados',
					showingPage: 'Pagina {0} de {1}',
					show: 'Mostrar ',
					search: 'Buscar',
					entries: ' Entradas',
					columnName: 'Nombre Columna',
					insertANewColumnBefore: 'Insertar una nueva columna antes',
					insertANewColumnAfter: 'Insert a new column after',
					deleteSelectedColumns: 'Delete selected columns',
					renameThisColumn: 'Rename this column',
					orderAscending: 'Order ascending',
					orderDescending: 'Order descending',
					insertANewRowBefore: 'Insert a new row before',
					insertANewRowAfter: 'Insert a new row after',
					deleteSelectedRows: 'Delete selected rows',
					editComments: 'Edit comments',
					addComments: 'Add comments',
					comments: 'Comments',
					clearComments: 'Clear comments',
					copy: 'Copy...',
					paste: 'Paste...',
					saveAs: 'Save as...',
					about: 'About',
					areYouSureToDeleteTheSelectedRows: 'Are you sure to delete the selected rows?',
					areYouSureToDeleteTheSelectedColumns: 'Are you sure to delete the selected columns?',
					thisActionWillDestroyAnyExistingMergedCellsAreYouSure: 'This action will destroy any existing merged cells. Are you sure?',
					thisActionWillClearYourSearchResultsAreYouSure: 'This action will clear your search results. Are you sure?',
					thereIsAConflictWithAnotherMergedCell: 'There is a conflict with another merged cell',
					invalidMergeProperties: 'Invalid merged properties',
					cellAlreadyMerged: 'Cell already merged',
					noCellsSelected: 'No cells selected',
				},
			})
		}
	}
	$("#cargando").remove();
});

function mostrarYocultar() {
	var x = document.getElementById("miContenedor");
	if (x.style.display === "none") {
	  x.style.display = "block";
	  document.getElementById("botonSubirExcel").innerHTML = 'Ocultar vista excel precios fob negociados';
	} else {
	  x.style.display = "none";
	  document.getElementById("botonSubirExcel").innerHTML = 'Subir archivo excel para precios fob negociados';
	}
}

function guardarDatos(base64){	
	let text = "Esta seguro de enviar la información?.";
	if (confirm(text) == true) {		
		envioProductosPrecioFob(base64)
	} else {
		console.log("You canceled!");
	}
}

function envioProductosPrecioFob(base64){
	console.log(base64)
	$("#cargando").remove();
	$("#respuestaProductosPrecioFob").remove();
	$("#guardarDatos").after("<div id='cargando' class='loader'></div> ");
	$("#respuestaDatosExcel").html("<div id='cargando' class='loader'></div> ");
	$("#guardarDatos").css("pointer-events", "none");
	$("#respuestaCantidad").remove();
	var inputfile =document.getElementById("miArchivoExcel");
        inputfile.value = ''
	$.ajax({
	type: 'post',
	url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ActualizaProductosRepPedido',
	data: {base64},
	dataType: 'json',
		success: function(data) {
			console.log(data);
			json = JSON.parse(data);
			console.log("transaccion: "+ json.transaccion) 
			if(json == 404){
				$("#guardarDatos").after('<div id="respuestaProductosPrecioFob" class="alert alert-danger" role="alert">Servicio no disponible, intente nuevamente en unos minutos o consulte a su administrador de sistemas</div>');
				console.log("ERROR!!! Verifique que el servicio que recepciona la información este disponible")
			}else{
				respuestaServicio(json)
				if(json.transaccion == "true"){
					$("#guardarDatos").after('<div id="respuestaProductosPrecioFob" class="alert alert-success" role="alert">Respuesta: '+ json.descripcion +'.</div>');
				}else{
					$("#guardarDatos").after('<div id="respuestaProductosPrecioFob" class="alert alert-danger" role="alert">Respuesta: '+ json.descripcion +'</div>');
				} 
			}			 
			$("#cargando").remove(); 
			$("#guardarDatos").css("pointer-events", "visible");  
		},
		error: function (request, error) {
			console.log('ERROR!!!!; ' + error)
			$("#guardarDatos").after('<div id="respuestaProductosPrecioFob" class="alert alert-danger" role="alert"> Ocurrio un error de conexion al enviar su información, pruebe nuevamente o consulte a su administrador de sistemas</div>');
			$("#cargando").remove();
			$("#guardarDatos").css("pointer-events", "visible");
		}
	});
}

function respuestaServicio(datos){	
	$("#cargando").remove();	
	var arrRespuesta = JSON.parse(datos.productosNoActualizados);
	console.log("PRODUCTOS NO ACTUALIZADOS arrRespuesta " + arrRespuesta )
	var cntNoActualizados = datos.cantidadNoActualizados;
	if(cntNoActualizados == 0){
		$("#respuestaDatosExcel").before('<div id="respuestaCantidad" class="alert-success" role="alert" style="padding: 9px;margin-bottom: 10px;margin-top: 5px;"> '+ cntNoActualizados +' Productos con error.</div>');
	}else{
		$("#respuestaDatosExcel").before('<div id="respuestaCantidad" class="alert-danger" role="alert" style="padding: 9px;margin-bottom: 10px;margin-top: 5px;">'+ cntNoActualizados +' Productos con error o no se encontraron.</div>');
	}
	
	var data = [];
	for (let index = 0; index < arrRespuesta.length; index++) {
		//console.log("IdProducto: "+ element.__rowNum__);
		data[index] = [
			arrRespuesta[index]
			]
	}				
	//console.log("DATOS: "+ data)
	jexcel(document.getElementById('respuestaDatosExcel'), {
		data:data,
		tableOverflow:true,
		columnSorting:true,
		csvHeaders:true,
		//search:true,
		tableWidth: '100%',
		tableHeight: '300px',
		//lazyLoading:true,
		//loadingSpin:true,
		//filters: true,
		allowComments:true,
		pagination: 10,
		//freezeColumns: 14,
		columns: [
			{
				type: 'text', 
				title:'IdProducto', 
				width:170,
				readOnly:true,
			}
			],

			text:{
			noRecordsFound: 'No se encontraron resultados',
			showingPage: 'Pagina {0} de {1}',
			show: 'Mostrar ',
			search: 'Buscar',
			entries: ' Entradas',
			columnName: 'Nombre Columna',
			insertANewColumnBefore: 'Insertar una nueva columna antes',
			insertANewColumnAfter: 'Insert a new column after',
			deleteSelectedColumns: 'Delete selected columns',
			renameThisColumn: 'Rename this column',
			orderAscending: 'Order ascending',
			orderDescending: 'Order descending',
			insertANewRowBefore: 'Insert a new row before',
			insertANewRowAfter: 'Insert a new row after',
			deleteSelectedRows: 'Delete selected rows',
			editComments: 'Edit comments',
			addComments: 'Add comments',
			comments: 'Comments',
			clearComments: 'Clear comments',
			copy: 'Copy...',
			paste: 'Paste...',
			saveAs: 'Save as...',
			about: 'About',
			areYouSureToDeleteTheSelectedRows: 'Are you sure to delete the selected rows?',
			areYouSureToDeleteTheSelectedColumns: 'Are you sure to delete the selected columns?',
			thisActionWillDestroyAnyExistingMergedCellsAreYouSure: 'This action will destroy any existing merged cells. Are you sure?',
			thisActionWillClearYourSearchResultsAreYouSure: 'This action will clear your search results. Are you sure?',
			thereIsAConflictWithAnotherMergedCell: 'There is a conflict with another merged cell',
			invalidMergeProperties: 'Invalid merged properties',
			cellAlreadyMerged: 'Cell already merged',
			noCellsSelected: 'No cells selected',
		},
	})
}
/*function ventaModal(){
    ventanaModal();
    $('#abrirModalSubirExcel').modal('show');
	document.querySelector("#miArchivoExcel").addEventListener("change", function () {
		// Obtener el archivo seleccionado
		var file = document.querySelector("#miArchivoExcel").files[0];
		var type = file.name.split('.');
		if (type[type.length - 1] !== 'xlsx' && type[type.length - 1] !== 'xls') {
			alert('Archivo no válido, seleccione un archivo válido en formato excel.');
			//return false;
		}else{
			const reader = new FileReader();
			reader.readAsBinaryString(file);
			reader.onload = (e) => {
				const data = e.target.result;
				const zzexcel = window.XLS.read(data, {
					type: 'binary'
				});
				const result = [];
				for (let i = 0; i < zzexcel.SheetNames.length; i++) {
					const newData = window.XLS.utils.sheet_to_json(zzexcel.Sheets[zzexcel.SheetNames[i]]);
					result.push(...newData)
				}
				console.log('result', result)
			}
		}
		
	});
}

function ventanaModal(){	
	    var htmlm = '';
	    htmlm += '<div class="modal fade" id="abrirModalSubirExcel" style="display: block;margin-top: 5%;">';
	    htmlm += '    <div class="modal-dialog">';
	    htmlm += '        <div class="modal-content">';
	    htmlm += '            <div class="modal-header" >';
	    htmlm += '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	    htmlm += '                <h4 class="modal-title">Subir su archivo Excel</h4>';
	    htmlm += '            </div>';
	    htmlm += '            <div class="modal-body" >';
	    htmlm += '                <div class="panel panel-danger">';
    	htmlm += '                    <div class="panel-heading"></div>';
    	htmlm += '                    <div class="panel-body"style="padding: 20px 30px;">';
    	htmlm += '						<input type="file" id="miArchivoExcel"><br>';
		htmlm += '					  </div>';
		htmlm += '				   </div>';
	    htmlm += '            </div>';
	    htmlm += '        </div>';
	    htmlm += '    </div>';
	    htmlm += '</div>';  
    $("#ventanaModal").html(htmlm);
	
}
*/