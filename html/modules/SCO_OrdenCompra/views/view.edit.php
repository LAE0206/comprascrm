<?php
/**
*Esta clase realiza operaciones matemÃ¡ticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license ruta: /var/www/html/modules/views/SCO_Embarque
*/
if (! defined ( 'sugarEntry' ) || ! sugarEntry) die ( 'Not A Valid Entry Point' );

require_once ('include/MVC/View/views/view.edit.php');
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SCO_OrdenCompraViewEdit extends ViewEdit {

  function SCO_OrdenCompraViewEdit() {
    parent::ViewEdit ();
    //$this->useForSubpanel = true;
  }

  function display(){
    if($this->bean->id){
        #echo $this->bean->orc_estado;
        if($this->bean->orc_estado == 3 || $this->bean->orc_estado == 1){
          echo "<style>
            #orc_observaciones, #orc_fechaord, #orc_fechaord_trigger, #orc_tipo,#orc_observaciones, #orc_tipoo, #orc_solicitado, 
            #btn_orc_solicitado, #btn_clr_orc_solicitado{
              pointer-events: none; background:#f5f5f5;
            }
            #SAVE, #save_and_continue{
              display:none;
            }
            #detailpanel_0 input{
              pointer-events: none; background:#f5f5f5;
            }
            #detailpanel_1 input{
              pointer-events: none; background:#f5f5f5;
            }
            #detailpanel_2 input{
              pointer-events: none; background:#f5f5f5;
            }
          </style>";
        }
        if($this->bean->orc_estado == 6 || $this->bean->orc_estado == 5){
          echo "<style>
            #name, #orc_fechaord, #orc_fechaord_trigger, #orc_tipo,#orc_observaciones, #orc_tipoo, #orc_solicitado, #btn_orc_solicitado, #btn_clr_orc_solicitado,
            #sco_proveedor_sco_ordencompra_name, #btn_sco_proveedor_sco_ordencompra_name, #btn_clr_sco_proveedor_sco_ordencompra_name, #sco_ordencompra_contacts_name,
            #sco_ordencompra_contacts_name, #btn_sco_ordencompra_contacts_name, #btn_clr_sco_ordencompra_contacts_name {
              pointer-events: none; background:#f5f5f5;
            }
            #SAVE, #save_and_continue{
              display:none;
            }
            #detailpanel_1 input{
              pointer-events: none; background:#f5f5f5;
            }
            #detailpanel_2 input{
              pointer-events: none; background:#f5f5f5;
            }
          </style>";
        }
        echo "<style>#orc_verco, #btn_orc_occ, #btn_clr_orc_occ, #orc_occ{display:none}</style>";
    }else{
        #echo "Creacion nueva";
    }
    global $current_user;
      $division = $current_user->iddivision_c;
        //QUery, ordenando los nombres del modulo de CNF_EVENTOS
        $query ="SELECT cnf_comentarios FROM suitecrm.sco_cnf_comentarios where cnf_division = '$division' ";
        $obj = $GLOBALS['db']->query($query, true);
        #echo "<script>alert('".$this->bean->emb_orig.$this->bean->emb_transp."')</script>";
      $Comentarios = '';
        while($row = $GLOBALS['db']->fetchByAssoc($obj))
        {
        $Comentarios = $row["cnf_comentarios"];
        }
      $com = base64_encode($Comentarios);

    echo "<script>
      var comentario = '".$com."';
      $(document).ready(function () {
        if($('#orc_observaciones').val() == '')
        {      
            $('#orc_observaciones').val(b64_to_utf8(comentario));
        }
        function b64_to_utf8( str ) {      
          return decodeURIComponent(escape(window.atob( str )));
        }
      });
      </script>";

    $js = "
            <script>
            $(document).ready(function () {
                $('#orc_verco').on('click',function()
                {
                  if($('#orc_verco')[0].checked) {
                    alert('Seguro que desea clonar esta Orden de Compra?');
                    $('.panel-default').hide('swing');
                    //Desbloqueamos el campo de busqueda para clonar ordenes de compra
                    $('#btn_orc_occ').prop('disabled',false);
                    $('#orc_occ').prop('disabled', false);
                    $('#name').prop('disabled', false);
                    $('#orc_solicitado').prop('disabled', false);
                    //Bloqueamos los demas campos

                    $('#orc_fechaord').prop('disabled', true);
                    $('#orc_observaciones').prop('disabled', true);
                    $('#sco_proveedor_sco_ordencompra_name').prop('disabled', true);
                    $('#sco_ordencompra_contacts_name').prop('disabled', true);
                    $('#orc_tcinco').prop('disabled', true);
                    $('#orc_tcmoneda').prop('disabled', true);
                    $('#orc_tclugent').prop('disabled', true);
                    $('#orc_tcmulta').prop('disabled', true);
                    $('#orc_tiempo').prop('disabled', true);
                    $('#orc_tccertor').prop('disabled', true);
                    $('#orc_tcforpag').prop('disabled', true);
                    $('#orc_tcgarantia').prop('disabled', true);
                  }
                  else{
                    $('.panel-default').show('swing');
                    //Bloqueamos el campo de busqueda para clonar ordenes de compra
                    $('#orc_occ').prop('disabled', true);
                    //Desbloqueamos los demas campos
                    $('#name').prop('disabled', false);
                    $('#orc_tipo').prop('disabled', false);
                    $('#orc_fechaord').prop('disabled', false);
                    $('#orc_tipoo').prop('disabled', false);
                    $('#orc_observaciones').prop('disabled', false);
                    $('#orc_solicitado').prop('disabled', false);
                    $('#sco_proveedor_sco_ordencompra_name').prop('disabled', false);
                    $('#sco_ordencompra_contacts_name').prop('disabled', false);
                    $('#orc_tcinco').prop('disabled', false);
                    $('#orc_tcmoneda').prop('disabled', false);
                    $('#orc_tclugent').prop('disabled', false);
                    $('#orc_tcmulta').prop('disabled', false);
                    $('#orc_tiempo').prop('disabled', false);
                    $('#orc_tccertor').prop('disabled', false);
                    $('#orc_tcforpag').prop('disabled', false);
                    $('#orc_tcgarantia').prop('disabled', false);
                  }
                })
                function ocudaenv()
                {
                  if($('#orc_decop')[0].checked)
                  {
                    //alert('esto es check');
                    $('#orc_denomemp_label').prop('disabled', false);
                    $('#orc_denomemp').prop('disabled', false);
                    $('#orc_defax_label').prop('disabled', false);
                    $('#orc_defax').prop('disabled', false);
                    $('#orc_depobox_label').prop('disabled', false);
                    $('#orc_depobox').prop('disabled', false);
                    $('#orc_depais_label').prop('disabled', false);
                    $('#orc_depais').prop('disabled', false);
                    $('#orc_detelefono_label').prop('disabled', false);
                    $('#orc_detelefono').prop('disabled', false);
                    $('#orc_dedireccion_label').prop('disabled', false);
                    $('#orc_dedireccion').prop('disabled', false);
                  } else {
                    //alert('esto es no check');
                    $('#orc_denomemp_label').prop('disabled', true);
                    $('#orc_denomemp').prop('disabled', true);
                    $('#orc_defax_label').prop('disabled', true);
                    $('#orc_defax').prop('disabled', true);
                    $('#orc_depobox_label').prop('disabled', true);
                    $('#orc_depobox').prop('disabled', true);
                    $('#orc_depais_label').prop('disabled', true);
                    $('#orc_depais').prop('disabled', true);
                    $('#orc_detelefono_label').prop('disabled', true);
                    $('#orc_detelefono').prop('disabled', true);
                    $('#orc_dedireccion_label').prop('disabled', true);
                    $('#orc_dedireccion').prop('disabled', true);
                  }
                }
               
                $('#orc_decop').change(ocudaenv);
    
                $('#orc_occ').prop('disabled', true);
                $('#btn_orc_occ').prop('disabled', true);
    
              });
            </script>";
         echo "<style> :disabled{ backgorund:rgb(235, 235, 228); color:#aaa;} input[type='text']:disabled { background: rgb(235, 235, 228); color:#aaa;} 
         select:disabled{ background: rgb(235, 235, 228); color:#aaa;} 
         #LBL_PANEL_SECURITYGROUPS {
              display: none;
          }</style>";
        echo $js;
    parent::display();
  }

}
