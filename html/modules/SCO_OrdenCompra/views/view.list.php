<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'include/MVC/View/views/view.list.php';
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');

class SCO_OrdenCompraViewList extends ViewList
{
        function display(){
        echo '<style>
.tooltip-lae .tooltiptext-lae {
    visibility: hidden;
    width: 100px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    left: -35px;
    margin-top: -60px;
    opacity: 5;
    transition: opacity 0.3s;
}

.tooltip-lae .tooltiptext-lae::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip-lae:hover .tooltiptext-lae {
  visibility: visible;  
  opacity: 1;
}
</style>';
            global $current_user, $db, $current_language, $mod_strings;
            #echo "DIVISION ".$current_user->iddivision_c;
            #echo $current_user->id;         
            $roles = ACLRole::getUserRoleNames($current_user->id);
            #print_r($roles);
            if (in_array("Acceso a crear, editar y modificar Orden de compra", $roles ) || in_array("solo ver", $roles ) || in_array("LOGISTICOS", $roles ) || in_array("Logistico Administrativo", $roles ) || $current_user->is_admin ){
              if($current_user->iddivision_c == ""){
                if($current_user->is_admin != 1){
                  echo '<div class="alert alert-danger" role="alert">
                <strong>Advertencia!!!</strong>, 
                No tiene asignado una division, comuniquece con su administrador de sistemas para que se regularice esta información.
                </div>';
                }                
              }
              parent::display();
            }else{
                echo '<div class="alert alert-danger" role="alert">No tiene los permisos asignado para el Módulo de Orden de Compra, <a class="alert-link">Solicite un rol a su administrador de sistemas.</a></div>';
            }                                        
        }
}