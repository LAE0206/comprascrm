<?php
/**
*Esta clase realiza operaciones matem?ticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license /var/www/html/modules/SCO_OrdenCompra
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class SCO_OrdenCompraViewDetail extends ViewDetail {

  function SCO_OrdenCompraViewDetail(){
    parent::ViewDetail();
    $this->useForSubpanel = true;
  }

  public function preDisplay(){
    $validaDocumentos = [];
    echo '<link href="/modules/SCO_OrdenCompra/Documentos/view-subpanle.css" rel="stylesheet" type="text/css" rel="preload"/>';
      parent::preDisplay();
    }

  function display(){ 

    header('Content-Type: text/html; charset=UTF-8');
    echo '<meta charset="utf-8">';
    global $current_user;    
    if($current_user->iddivision_c == ""){
      if($current_user->is_admin != 1){
        echo '<div class="alert alert-danger" role="alert">
      <strong>Advertencia!!!</strong>, 
      No tiene asignado una division, comuniquece con su administrador de sistemas para que se regularice esta informaci?n.
      </div>';
      }                
    }
    if($this->bean->sco_proveedor_sco_ordencompra_name == ""){
        echo '<div class="alert alert-danger" role="alert">
      <strong>Advertencia!!!</strong>, 
      No tiene registrado un Proveedor en su Orden de Compra..
      </div>';             
    }
    echo '<link href="/modules/SCO_OrdenCompra/Documentos/view-subpanle.css" rel="stylesheet" type="text/css" rel="preload"/>';
    echo '<link href="modules/SCO_Consolidacion/css-loader.css?'.time().'" rel="stylesheet" type="text/css" />';
    echo '<script src="modules/SCO_OrdenCompra/viewdetail.js?'.time().'"></script>';
    echo '<script src="modules/SCO_OrdenCompra/Documentos/jquery.aCollapTable.js?'.time().'"></script>';
    echo '<link href="/modules/SCO_documentos/fileinput.css?'.time().'" type="text/css"/>';
    
  $sty = "<style>
    #whole_subpanel_sco_ordencompra_sco_documentos{display:none}
     .btnCompra {
        /* padding: 10px !important; */
        padding: 0px 10px 0px 10px !important;
        /* font-size: 10px !important; */
    }
    }
    #alertapp{
      position: fixed;
      float: left;
      margin-top: 10px;
      margin-left:35%;
      z-index:1;
    }
    #idpro tbody tr{
      border-bottom: 1px solid #ccc;
      background-color: #f2f2f2;
    }
    .search-form{
      display: none;
    }
    .label{
      font-size: 11.5px;
    }
    .campopersonalizado{
      margin-top:10px;
    }
    #list_subpanel_sco_ordencompra_sco_productos .oddListRowS1{background:red;}
    #whole_subpanel_sco_ordencompra_sco_productoscompras {display:none;}
    #whole_subpanel_sco_productoscotizados_sco_ordencompra {display:none;}
    .aprimg{
          height: 30px;
          border-radius: 50%;
        }
    .showDragHandle{
      box-shadow: 0px 3px 10px #ccc;
      color: #000;
      font-weight: bold;
    }
    .moduleTitle{
      display: flex;
      margin-bottom: 15px;
    }
    
    .cuadrado-int{
      margin: auto;
      font-size: 22px;
      letter-spacing: 1px;
      color: #fff;
    }
    .moduleTitle h2 {
      display: block;
      font-size: 17.5px;
      font-weight: 600;
      color: #0e2741;
      margin: 0 0 12px 10px;
      float: left;
      letter-spacing: 1px;
      text-transform: uppercase;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
      overflow: hidden;
    }
    
    </style>";
    $htmlpp ='<div id="alertapp"></div> 
              <div id="ventanaModal"></div>
              <div id="ventanaModalPM"></div>
              <div id="ventanaModalAprobacion"></div>
              ';
    echo $sty.$htmlpp;
    if($this->bean->orc_tipoo == '1'){
        echo "<style>
             #whole_subpanel_sco_despachos_sco_ordencompra {display:none;}
           </style>";
      }
    #Consultas SQL para obtecion de informacion de productos
    $cantidad_prd = "SELECT SUM(pro_cantidad) as total_cantidad FROM sco_productos_co WHERE pro_idco = '".$this->bean->id."'; ";
    $obj_pro = $GLOBALS['db']->query($cantidad_prd, true);
    $row = $GLOBALS['db']->fetchByAssoc($obj_pro);

    #Suma total 
    $cantidad_pd = "SELECT SUM(pd.prdes_cantidad) as cantidad_pr
    FROM sco_productosdespachos as pd
    INNER JOIN sco_despachos_sco_productosdespachos_c as dpd
    ON pd.id = dpd.sco_despachos_sco_productosdespachossco_productosdespachos_idb
    INNER JOIN sco_despachos_sco_ordencompra_c as d_oc
    ON d_oc.sco_despachos_sco_ordencomprasco_despachos_idb = dpd.sco_despachos_sco_productosdespachossco_despachos_ida
    INNER JOIN sco_despachos as d
    ON d.id = d_oc.sco_despachos_sco_ordencomprasco_despachos_idb
    WHERE d_oc.sco_despachos_sco_ordencomprasco_ordencompra_ida = '".$this->bean->id."'
    AND pd.deleted = 0
    AND dpd.deleted = 0
    AND d.deleted = 0;";
    $obj_pd = $GLOBALS['db']->query($cantidad_pd, true);
    $row_pd = $GLOBALS['db']->fetchByAssoc($obj_pd);
    
    if($row_pd['cantidad_pr']!=0){
      $c = $row_pd['cantidad_pr'];
    }else{
      $c = 0;
    }
    $disponible = $row['total_cantidad'] - $c;

    $cantidades_p = "SELECT  pro_cantidad,pro_nombre,pro_descripcion,pro_saldos,pro_canttrans,pro_preciounid,pro_cantresivida,pro_nomproyco
    FROM sco_productos_co
    WHERE pro_idco = '".$this->bean->id."' AND deleted = 0 order by pro_saldos";
    $obj_cantidades_p = $GLOBALS['db']->query($cantidades_p, true);
    $arr_cantidades_p = array();
    $arr_recibido_p = array();
    $arr_saldos_p = array();
    $arr_nombre = array();
    $arr_descripcion = array();
    $arr_cantidadProducto = array();
    $arr_nomProy = array();
    while($row_cantidades_p = $GLOBALS['db']->fetchByAssoc($obj_cantidades_p)){
      array_push($arr_cantidades_p, $row_cantidades_p['pro_canttrans']);
      array_push($arr_recibido_p, $row_cantidades_p['pro_cantresivida']);
      array_push($arr_saldos_p, $row_cantidades_p['pro_saldos']);
      array_push($arr_nombre, $row_cantidades_p['pro_nombre']);
      array_push($arr_descripcion, $row_cantidades_p['pro_descripcion']);
      array_push($arr_cantidadProducto, $row_cantidades_p['pro_cantidad']);
      array_push($arr_nomProy, $row_cantidades_p['pro_nomproyco']);
    }
    $cantidad = json_encode($arr_cantidades_p);
    $recibido = json_encode($arr_recibido_p);
    $saldos = json_encode($arr_saldos_p);
    $cantidadProducto = json_encode($arr_cantidadProducto);
    $descripicion = json_encode($arr_descripcion);
    $nombre = json_encode($arr_nombre);
    $nomProyecto = json_encode($arr_nomProy);
    #echo $cantidad;

    echo "<script>$(\"#subpanel_title_sco_despachos_sco_ordencompra tr \").append(\"<div style='background: #FFF; color:#000; padding:2px; margin-top:3px; width: 360px; border-radius:0px; margin-right: 600px; border: solid 2px #CCC;'>Productos en despacho = <span class='label label-success'>".$c."</span> / <span class='label label-primary'>".$row['total_cantidad']."</span>, Restantes = <span class='label label-danger'>".$disponible."</span> </div>\");</script>";
   
    $estado = $this->bean->orc_estado;
    if($estado == '2'){
    
      echo "      
      <script>
      $(document).ready(function () {
        $(\"#subpanel_title_sco_ordencompra_sco_aprobadores\").after(\"<div class='validar'><button style='z-index: 1;position: absolute;right: 0;margin-top: -30px;margin-right: 90px;height: 25px;line-height: 25px;background: #378cbe;color: #fff;border: solid 0.5px #fff;box-shadow: 1px 2px 2px #ccc;' onclick='modalOrdernarAprobador()'><span class='suitepicon suitepicon-action-add'></span> Ordenar Aprobacion de usuarios </button></div>\");
      });
      </script>";
    }
    
    $estado = $this->bean->orc_estado;
    $arr_estado = array(1 => 'En curso',2=>'Borrador ', 3 =>'Solicitar Aprobacion ', 4 => 'Aprobado ' ,5 => 'Anulado ', 6 =>'Cerrado ');
    $moneda = $this->bean->orc_tcmoneda;
    $st ='<style>
              .gris{color: #ccc;}
              .gris:hover{color: #ccc;}
              .single{display: none;}
              #sugar_action_button, .sugar_action_button{display: none;}
              #whole_subpanel_sco_ordencompra_sco_productos tbody td a {pointer-events: none; cursor: default;}
              #whole_subpanel_sco_ordencompra_sco_contactos tbody td a {pointer-events: none; cursor: default;}
              #whole_subpanel_sco_ordencompra_sco_aprobadores tbody td a {pointer-events: none; cursor: default;}
              #whole_subpanel_sco_ordencompra_sco_plandepagos tbody td a {pointer-events: none; cursor: default;}
              .footable-first-visible{display:none}
              .footable-last-visible{display:none}
          </style>';
  #Obteniendo la relacion del la Orden de compra con Consolidacion de Cotizaciones
  $beanOC = BeanFactory::getBean('SCO_OrdenCompra', $this->bean->id);
  $beanOC->load_relationship('sco_consolidacion_sco_ordencompra');
  $relatedBeans = $beanOC->sco_consolidacion_sco_ordencompra->getBeans();
  reset($relatedBeans);
  $parentBean   = current($relatedBeans);
  #id Consolidacion Cotizaciones
  $idConsolidacion = $parentBean->id;
  $beanCons = BeanFactory::getBean('SCO_Consolidacion', $idConsolidacion);
  $consolidacion   = '<a class="suitepicon suitepicon-action-view"href="index.php?module=SCO_Consolidacion&action=DetailView&record='.$idConsolidacion.'"> '.$beanCons->name.'</a>';            

    #script para cambio de estados
  echo "
    <script>
    var nombreOC = '".$this->bean->name."';
    var nombreCortoOC = '".$this->bean->orc_nomcorto."';
    var idOc = '".$this->bean->id."';   
    var divisionSolicitante = '".$this->bean->orc_division."';
    var divisionUsuario = '".$current_user->iddivision_c."';
  </script>";
  echo "
  <script>
    $('#list_subpanel_sco_ordencompra_sco_productos #sco_ordencompra_sco_productos_nuevo_button').on('click',function(){
    $('#idpro').fadeOut();
    });
    //$('#list_subpanel_sco_ordencompra_sco_productos .list tbody .oddListRowS1').hide();
    //$('#list_subpanel_sco_ordencompra_sco_productos').append(htmlpro);
    </script>";

        // echo "<script src=\"modules/SCO_Productos/jquery.bdt.min.js?".time()."\"></script>";
        // echo "<script>$('#idpro').bdt();</script>";
        echo "<style>#list_subpanel_sco_ordencompra_sco_productos .list{display:none;}
            .menuoc{
              float: right !important;
              margin-top: -10px;
            }
            .menuoc-text{
              margin-right: 3px;
              background: #fff;
              padding: 5px 10px;
            }
            .menuoc-text img{
              height: 17px;
            }
            .menuoc-text:hover{              
              background: #eee;
            }
            .menuoc-click{
              cursor: pointer;
              margin-left: 5px;
            }
            .menu-send{
              background:#427505;
            }
            .menu-send:hover{
              background: #70aa2a;
            }
            .menu-anular{
              background:#f44336
            }
            .menu-anular:hover{
              background: #ffa0a0;
            }
        </style>";

        echo '<div class="menuoc">
          <span class="menuoc-text"> 
            <span class="glyphicon glyphicon-paperclip" style="font-size: 12px; color: #000 !important; line-height: 33px !important;"></span>
            <span class="menuoc-click" onClick=showreport("'.$this->bean->id.'");>Ver reporte</span>
          </span>
          
          <span class="menuoc-text"> 
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAsdQTFRFAAAA/f//5+rs4uXn19vevsbN/f//s7vCytLZ/f//ucDGzNTbucHHucDHy9Pat7/FydDY7PT75Oz04ury4enx+///3+jw8////f///2dP/2JL/19I/1hB+1E7/2JL/2BK/2FK/2VN/1xG/2VN/19I/1U+5ZSN7f7//P//////7vHz////5enr9vr84uXn5efp5+rs5ejp0dbZ0dbYrrW80NXYrLO6sbi/rLO5r7e9srm/5Ofp0dXZq7K5r7a8sLe95Ofo3uHkq7G3rrW74+bo2t7hxsvQu8LKvMPLvcTLv8bN5unq2N3hytHYy9LZzNPa0djf1tvfyM/XydHYztbd4+bnztXczNTb5u3v4ujq4ufp4ubo2t/j5Pn94PX64PT54fH14evu6Ovt81I98lA78k458k445c7M4urs+1pF9lhD9VZC81M/8VE88VA78VE98VI980Yw6LCr4ezv8VZC8VVB8Ew28Ek08Es18E458E868Ew38Eo18Es28Eo08VRA8VQ/8ko06LOu4e3w8E46/efk/u/t/ebj9X9v70Qu/NLN/d/c/NzX9piM8Egy8VpF/N7Z/d7b/eTh9pGE8VI+4e3x8U86////82RS9IN07z4n83Nh/vn59pqO8mZT9Hhp9YFz8V9M8U047jUd7z8p70Ar70Mt9X1v/efl8WBN+K+n+beu8lxI8VM/96ec7z4o7z8q70Is9ox//d/d8WFO96ab+K6l8l5L8VM+8VA870Er8EYy8EYx952R+auh9YFy8mlX/vz78EQu8E048FA796KX+Kyi+bOq9HVk95uO+FhE8lQ/8Us18Uw28U458lRA8lI980gy6LKs91hD9VVA9E869Ew19Es09Eo09Ekz9Eoz9Eky9UAp5rez4uvu07Ky0q+v0q+u0q2t0K+v2ebq3uzx2+js2+fr3Obq4+jr5+zu4+fp4+bp/rA77gAAAC50Uk5TAAWp++40Tu40YO407u407u4SICAgYCBgIArS7+/1ICAgFv01UFCRYGAg/BH0dsT0OswAAAABYktHRCnKt4UkAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAByklEQVQoz2NgAAFGJmY9BNBnYWVAAWwGSLJ6hkbsHCjSnKjSxiZc3HikTc3MebjxSFtYWiHLo0pb29ja2Vvx8uGQdnB0snB2tufnwC7tYujq5u7h6SUgiFVaz8Xa28fXzz9ACLs0UIF+YFBwiDAuab1Q68CgMNzSIPlwEai0aEQkGoiKdrGOEYNKi8fGoYL4hMQolyQJoJSklLSMbHIKKkhNS8+INgBJy2VmZefk5uXlAnFebj6YLEgpLCqGSMtnlZTml5VXVFZV19RW5udV11Xn1Tc0NjXDpRtaWtvaOzq7unt6+/onTJw0eQpQeipcesq06TNmTp81fXbHnLnzps9fsBBVumDR9MVl05dMX7ps+YqV01etXrMWXXr69HXrp2/YuGnzlulbt22vr0eW3rFz+q7ddXum7903ff+Bg4cO7yhFka4/crS6YEf1sdXHT9SUnKxcW1qCkFY4VVI6ZWfB6dO5i86cPQf0fcHp8+cvXLwE9Zii0uUrV69dv379xs2bN2/dAAIg+/adu/cg0gzKKqpq9x88RAGPHj+J1INIA4H602eo4PmLl9EIaY1Xr9GAS7SeXpIm1kwEBW+0tKHSqFkQCnR0GRgAhcYZ1qjOk2QAAABidEVYdGNvbW1lbnQAYm9yZGVyIGJzOjAgYmM6IzAwMDAwMCBwczowIHBjOiNlZWVlZWUgZXM6MCBlYzojMDAwMDAwIGNrOjUwMGQwMmE0ZjFmMWQ3NDk3MzQwY2M1ODY4OTZiZjExhJ/QAAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMS0wNy0xM1QyMzo1NToyNiswMDowMBXLC+oAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjEtMDctMTNUMjM6NTU6MjYrMDA6MDBklrNWAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg=="/>
            <span class="menuoc-click" onClick=imprimir("'.$this->bean->id.'",nombreOC,nombreCortoOC);>Descargar reporte</span>
          </span>
          
          ';
        #script para cambio Reporte
        if($current_user->iddivision_c == '03'){
          echo '          
            <span class="menuoc-text"> 
              <span class="glyphicon glyphicon-paperclip" style="font-size: 12px; color: #000 !important; line-height: 33px !important;"></span>
              <span class="menuoc-click" onClick=showReporteGerencialDiv03("'.$this->bean->id.'");>Ver reporte gerencial</span>
            </span>
            <span class="menuoc-text"> 
              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAXCAMAAAAr4Q9YAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAj1QTFRFAAAAAMd2ALtvALluALhtALZrAN2NAN2OAN6OAOeUALtvANKGAOeUALluAN6OALhvAN2NAJZGAI5CAIxBAIo/AIU8AH04AI5CAM6BAIxBALdsALluALtwAKJXAIo/AI5BAHY7AItAAIk/AIU+AGY2AC8WAGc3AGg4AGg3AGk4AGI0AGw6AG87AGg4AGc3AGg3AGw5////////////////AKxmAKlkAKhkAKVhAMqBAMqCAMmBAKllAKZjAKViAKJfAMZ/AMV+AKxoAKpnAKlmAKdkAKNgAMd/AHo2AHk1AHk2AJNRAMiBAMeAAMuCAII8AH86AHw2AHkyAHkzAHk0AHcxAHcyAH05AFopAH5FALx2ALp1ALl0AL12AIA7AHs3J5NaxeLTEIZIAHEocbeSkcerAFclAGEoAKdjAKRhAKJgAKZiAHUu7PbyqtS9////RaBwAFkmAGQqAKpmAHw4AHcwIY5VwODQxOHSAHs2AFonAGYsAKhlAKtnAHw5AHgzAGwhz+faKJJaAHMrAHw3AIE8AFQmAFQoAJVRAJJPAJFOAJRQAHYwMJZgudzKzOXZAFElAEQkAH45AHs4AHo3e7yaBn8/VKl8AIE7AEUlAH46AH06AH87RaFwwN/Pf76cn8+3AFImAEYmAII7AHw1AHozAHs1AH04AIM8AFMmAEgmAGw3AGo1AGk1AGw2AIY+AEQdAEonAFwyAFoxAFowACoUACkUADIYAFswAF0yAFwxAF4yAGE0AF8zAF8ySrt9rgAAADV0Uk5TAAQcHBwcHBwcE4f6H5cvly8W5/r6+v0vLy8vLy8vLy8v+vr9L5cvly+H/SIEHBwcFAYPDg2qGcNaAAAAAWJLR0Qx2dsdcgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAWZJREFUKM9jYKASYGRiBgIWVjZ2NjYOTgxpLlMzIDC3sLSytrbk5kGX5rWxBQI7ewcHR0dHSz50aX4nZ2dnF1c3d7C0AJKMoJCwiKiYh6enl6e3q7WPr6+vH7Jucf+AwKDgkNAwj/CIyKjomJiYWAkkacm4+ITEpOSU1OCAtPSMzKysrGwpVOmc3Dyv/ILguMKiYls7OzszaRTpktKy/PKK5Mq4qmonl5qamloZFOm6+obG/Kbmlta29o7Orq6ubllU3T29+X39yZWtEyZOmjxlypRwORTpypD8qdPypwfPmDAzbtbs2bPjJJGl53jMnVefPH9B8IyFixbPiIuL85dH8fekJUuXLQteunzFylWr16xdu3adAmqoKSopL54xw3/9ho2bNm/ZsmWTCnqYq27dtm3b1u07du4Cgt1q6NLqe/aCAFh2124NdGlNsPS+3WCgpY0uraOrBwf6BmQnOggwNDI2MQEA9LmD50eBYFQAAABidEVYdGNvbW1lbnQAYm9yZGVyIGJzOjAgYmM6IzAwMDAwMCBwczowIHBjOiNlZWVlZWUgZXM6MCBlYzojMDAwMDAwIGNrOjUwMGQwMmE0ZjFmMWQ3NDk3MzQwY2M1ODY4OTZiZjExhJ/QAAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMS0wNy0xM1QyMzo1NTo0MSswMDowMBYDPOMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjEtMDctMTNUMjM6NTU6NDErMDA6MDBnXoRfAAAAAElFTkSuQmCC"/>
              <span class="menuoc-click" onClick=descargaReporteGerencialDiv03("'.$this->bean->id.'",nombreOC,nombreCortoOC);>Descargar reporte gerencial</span>
            </span>
            ';
          $reporteGerencial = '
            <div class="col-xs-12 col-sm-6 detail-view-row-item">
                <div class="col-xs-12 col-sm-4 label col-1-label" style="margin-left: -20px;">
                  <b>Consolidacion:</b>
                </div>        
                <div class="col-sm-4 col-sm-7 campopersonalizado" type="varchar"style="margin-left: 40px;">
                '.$consolidacion.'
                </div>                 
            </div>    
          ';
        }else{
          $reporteGerencial = '';
        }
        if($this->bean->orc_reporte >= 1){
          echo '          
          <span class="menuoc-text" style="background: #ffffff5e; color: green;"> 
            <span class="glyphicon glyphicon-floppy-check" style="font-size: 15px; margin-right: 5px; color: green !important; line-height: 33px !important;"></span>
            # Caso Aprobacion PM
            <b>'.$this->bean->orc_reporte.'</b>
          </span>
          ';
        }      
      switch ($estado) {
        case '1':
          global $current_user;
          $id_usuario = $current_user->id;
            if($id_usuario == $this->bean->user_id_c || $id_usuario == '1'){
              echo '
                <span class="menuoc-text menu-anular"> 
                  <span class="glyphicon glyphicon-floppy-close" style="font-size: 15px; color: #fff !important; line-height: 33px !important;"></span>
                  <span class="menuoc-click" style="color: #fff !important;" onClick="estado(5,idOc);">Anular OC</span>
                </span>
              ';
            }
            break;
        case '2':
            echo '
              <span class="menuoc-text menu-send" > 
                    <span class="glyphicon glyphicon-send" style="color: #fff !important; font-size: 12px; line-height: 33px !important;"></span>
                    <span class="menuoc-click" id="menu-send" style="color: #fff !important;" onClick=estado(3,idOc);>Enviar Aprobación</span>
                  </span>
            ';
            break;
        case '3':
          global $current_user;
              $id_usuario = $current_user->id;
                if($id_usuario == $this->bean->user_id_c || $id_usuario == '1'){
                  echo '
                    <span class="menuoc-text menu-anular"> 
                      <span class="glyphicon glyphicon-floppy-close" style="font-size: 15px; color: #fff !important; line-height: 33px !important;"></span>
                      <span class="menuoc-click" style="color: #fff !important;" onClick="estado(5,idOc);">Anular OC</span>
                    </span>
                  ';
                }
            break;
       case '99':
          global $current_user;
              $id_usuario = $current_user->id;
                #if($id_usuario == $this->bean->user_id_c || $id_usuario == '1'){
                  echo '
                  <span class="menuoc-text menu-send" > 
                    <span class="glyphicon glyphicon-send" style="color: #fff !important; font-size: 12px; line-height: 33px !important;"></span>
                    <span class="menuoc-click" id="menu-send" style="color: #fff !important;" onClick=estado(3,idOc);>Reenviar Aprobación</span>
                  </span>
                ';
                #}
            break;
        case '4':
            echo '
              <span class="menuoc-text" > 
                <span class="suitepicon suitepicon-action-import" style="font-size: 15px; color: #000 !important; line-height: 33px !important;"></span>
                <span class="menuoc-click" >'.$arr_estado[4].'</span>
              </span>
            ';
            break;
        case '5':
            echo '
              <span class="menuoc-text" >             
                <span class="suitepicon suitepicon-action-img" style="font-size: 15px; color: #000 !important; line-height: 33px !important;"></span>
                <span class="menuoc-click" > </span>                
              </span>
            ';
            break;
        case '6':
            echo '
              <span class="menuoc-text" style="background: #fff0;">             
                <span class="suitepicon suitepicon-action-close" style="font-size: 15px; color: #000 !important; line-height: 33px !important;"></span>
                <span class="menuoc-click" style="color: green;">'.$arr_estado[6].'</span>                
              </span>              
            ';
            $id_usuario = $current_user->id;
            if($id_usuario == $this->bean->user_id_c || $id_usuario == '1'){
                  echo '
                    <span class="menuoc-text menu-anular"> 
                      <span class="glyphicon glyphicon-floppy-close" style="font-size: 15px; color: #fff !important; line-height: 33px !important;"></span>
                      <span class="menuoc-click" style="color: #fff !important;" onClick="estado(5,idOc);">Anular OC</span>
                    </span>
                  ';
                }
            break;
        default:

          break;
        }
        echo '</div>';

    switch ($estado) {
      case '1':
          echo "<style>#list_subpanel_sco_ordencompra_sco_productos table .clickMenu{display:none;}
                   #list_subpanel_sco_ordencompra_sco_plandepagos table .clickMenu{display:none;}
                   #list_subpanel_sco_ordencompra_sco_contactos table .clickMenu{display:none;}
                   #list_subpanel_sco_ordencompra_sco_aprobadores table .clickMenu{display:none;}
                   #whole_subpanel_sco_ordencompra_sco_plandepagos  tbody td a {pointer-events: none; cursor: default;}
                   #detail_header_action_menu {pointer-events: none; cursor: default;}
                   #whole_subpanel_sco_ordencompra_sco_documentos table .clickMenu{display:none;}
                 </style>";
          /*echo "
            <style>
              #whole_subpanel_sco_ordencompra_sco_documentos tbody td a {pointer-events: none; cursor: default;}
            </style>";*/
            if($divisionUser != '06'){
              echo "<style>
                   #whole_subpanel_sco_ordencompra_sco_documentos table .clickMenu{display:inline;}
                 </style>";
            }
            echo "
            <script>//$('#btn-estados').hide();
            $('#desc_por').prop('disabled', true);
            $('#desc_val').prop('disabled', true);</script>";
      parent::display();
      echo '
        <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
          '.$reporteGerencial.'
        </div>
        ';
      #echo $js.$st;
      break;
    case '2':
        echo "<style>#whole_subpanel_sco_despachos_sco_ordencompra{display:none;}</style>";
      parent::display();
      echo '
        <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
          '.$reporteGerencial.'
        </div>
        ';
      break;
    case '3':
           echo "<style>#list_subpanel_sco_ordencompra_sco_productos table .clickMenu{display:none;}
                         #list_subpanel_sco_ordencompra_sco_plandepagos table .clickMenu{display:none;}
                         #list_subpanel_sco_ordencompra_sco_contactos table .clickMenu{display:none;}
                         #list_subpanel_sco_ordencompra_sco_aprobadores table .clickMenu{display:none;}
                         #whole_subpanel_sco_ordencompra_sco_plandepagos  tbody td a {pointer-events: none; cursor: default;}
                         #detail_header_action_menu {pointer-events: none; cursor: default;}
                         #whole_subpanel_sco_despachos_sco_ordencompra{display:none;}
                 </style>";
        parent::display();

      echo "<style>#list_subpanel_sco_ordencompra_sco_documentos .sugar_action_button{display:block;}</style>";
      if($divisionUser != '06'){
        echo "<style>
             #whole_subpanel_sco_ordencompra_sco_documentos table .clickMenu{display:inline;}
           </style>";
      }

      echo '
        <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
          '.$reporteGerencial.'
        </div>
        ';
        break;
  case '4':
      parent::display();
     echo '
        <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
          '.$reporteGerencial.'
        </div>
        ';
       echo $js.$st."<script>$('#list_subpanel_sco_ordencompra_sco_documentos .sugar_action_button').show();</script>";
      break;
  case '5':
    /*echo "
      <style>
        #whole_subpanel_sco_ordencompra_sco_documentos tbody td a {pointer-events: none; cursor: default;}
      </style>";*/
      echo "
      <script>//$('#btn-estados').hide();
      $('#desc_por').prop('disabled', true);
      $('#desc_val').prop('disabled', true);</script>";
    parent::display();
    echo '
        <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
          '.$reporteGerencial.'
        </div>
        ';
    echo $js.$st;
    break;
    case '6':
          echo "<style>#list_subpanel_sco_ordencompra_sco_productos table .clickMenu{display:none;}
         #list_subpanel_sco_ordencompra_sco_plandepagos table .clickMenu{display:none;}
         #list_subpanel_sco_ordencompra_sco_contactos table .clickMenu{display:none;}
         #list_subpanel_sco_ordencompra_sco_aprobadores table .clickMenu{display:none;}
         #whole_subpanel_sco_ordencompra_sco_plandepagos  tbody td a {pointer-events: none; cursor: default;}
            #list_subpanel_sco_despachos_sco_ordencompra table .clickMenu{display:none;}
         #detail_header_action_menu {pointer-events: none; cursor: default;}
         #whole_subpanel_sco_ordencompra_sco_documentos table .clickMenu{display:none;}
         </style>";
      /*echo "
        <style>
          #whole_subpanel_sco_ordencompra_sco_documentos tbody td a {pointer-events: none; cursor: default;}
        </style>";*/
        echo "
        <script>//$('#btn-estados').hide();
        $('#desc_por').prop('disabled', true);
        $('#desc_val').prop('disabled', true);</script>";
      parent::display();
      echo '
      <div class="row detail-view-row" style="background:#FFF;margin-top:-20px;">
        '.$reporteGerencial.'
      </div>
      ';
      #echo $js.$st;
      if($divisionUser != '06'){
        echo "<style>
             #whole_subpanel_sco_ordencompra_sco_documentos table .clickMenu{display:inline;}
           </style>";
      }
      break;
    default:
      parent::display();
      break;
    }
        #echo "<div id='subPanelDocumentos'></div>";
    
    echo "<script src='modules/SCO_OrdenCompra/Documentos/view-subpanle.js?".time()."'></script>";
    include 'modules/SCO_OrdenCompra/Documentos/view-subpanle.html';
    echo "<script src='modules/SCO_OrdenCompra/Prorrateos/view-prorrateo.js?".time()."'></script>";
    include 'modules/SCO_OrdenCompra/Prorrateos/view-prorrateo.html';    
  }
}
?>
