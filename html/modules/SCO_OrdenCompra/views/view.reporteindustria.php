<?php

require_once('include/MVC/View/views/view.html.php');

#customAOS_ProductsViewPlan_inventario
class SCO_OrdenCompraViewReporteindustria extends ViewHtml
{
    public function display(){
    	include '../SCO_OrdenCompra/ReporteDiv01/excel_reader2.php';
    
		#$data = new Spreadsheet_Excel_Reader("example.xls");
		global $current_user, $db, $current_language, $mod_strings;
		#Datos de Usuario
		$usuarioNombre = $current_user->name;       
		$usuarioDivision = $current_user->iddivision_c;
		$usuarioAmercado = $current_user->idamercado_c;

		#Datos de usuario extrayendo su ROL
		$roles = ACLRole::getUserRoleNames($current_user->id);
		$usuarioRol = $current_user->user_name;
		#Consulta trae datos de una vista sco_viewdar nombres y codigos de division.
       	$query2 = "	SELECT DISTINCT(iddivision_c) as iddivision_c, iddivision_c_name
                	FROM suitecrm.sco_viewdar
                	ORDER BY iddivision_c asc;";
       	$results2 = $GLOBALS['db']->query($query2, true);
        while($row = $GLOBALS['db']->fetchByAssoc($results2))
        {
            $division .= '<option value="'.$row['iddivision_c'].'">'.$row['iddivision_c_name'].'</option>';
        }
        #Consulta trae datos de Area de mercado
       	$query = "SELECT DISTINCT(idamercado_c) as idamercado_c, idamercado_c_name, iddivision_c
                 FROM suitecrm.sco_viewdar
                 ORDER BY idamercado_c asc;";
       	$results = $GLOBALS['db']->query($query, true);
        while($row = $GLOBALS['db']->fetchByAssoc($results))
        {
            $amercado .= '<option class="'.$row['iddivision_c'].' am" value="'.$row['idamercado_c'].'">'.$row['idamercado_c_name'].'</option>';
        }
        
    $css = '<style>
			.modal-dialog {
				width: 60%;
				text-align: center;
			}
		  </style>';
		$cabecera='       		
			<link rel="stylesheet" href="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/jsuites.css" type="text/css" />
			<link rel="stylesheet" href="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/jexcel.css" type="text/css" />
			<link rel="stylesheet" href="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/style.css" type="text/css" />
			<link rel="stylesheet" href="modules/SCO_OrdenCompra/ReporteDiv01/ReporteGerencial.css" type="text/css" />
       	';
		$js = '
       		<script src="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/jexcel.js"></script>
			<script src="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/jsuites.js"></script>
			<script src="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/jsuites.js"></script>
			<script src="modules/SCO_OrdenCompra/ReporteDiv01/JexcelV4/table2excel.min.js"></script>
			<script src="modules/SCO_OrdenCompra/ReporteDiv01/ReporteGerencial.js?' . time () . '"></script>
      <script src="modules/SCO_OrdenCompra/ReporteDiv01/xlsx.full.min.js"></script>	
			<script src="modules/SCO_OrdenCompra/ReporteDiv01/Archivo.js?' . time () . '"></script>				
       	';
       	$titulo .= '
				<div class="">
				<span><a href="?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DSCO_OrdenCompra%26action%3Dreporte">Reporte</a></span>
				<span> | Reporte Pedidos</span>
				<br><br>
				<span class="titulo">Reporte Pedidos</span>
				<br>
				<h5><strong>Nota. </strong>La actualización de datos de este reporte se realiza todos los días, en horarios de 5:00am y 13:30pm, esta actualización dura aproximadamente de 20min a 30min.</h5>
				</div>
				<br>
				<!--div class="loader loader-default" data-text="Cargando"></div-->';
		$filtro = '					
			<input id="usuarioNombre" type="hidden" value="'.$usuarioNombre.'" disabled>
			<input id="usuarioDivision" type="hidden" value="'.$usuarioDivision.'" disabled>
			<input id="usuarioAmercado" type="hidden" value="'.$usuarioAmercado.'" disabled>
			<input id="usuarioRol" type="hidden" value="'.$usuarioRol.'" disabled>
				
			<div class="container-fluid">
				<div class="row filtro">	
        												
					';
		if ($current_user->is_admin) {											
		$filtro .= '	<div class="col-sm-2">
						<div class="input-group">
							<div class="input-group">
                              <span class="input-group-addon">Division</span>               
                              <select class="form-control" id="division" name="division" >
                                <option value="" selected="selected">Todo</option>
                                '.$division.'                           
                              </select>  
                            </div>
						</div>
					</div>
					';
				}else{
		$filtro .= '	<div class="col-sm-2">
						<div class="input-group">
							<div class="input-group">
                              <span class="input-group-addon">Division</span>               
                              <select class="form-control" id="division" name="division" disabled>
                                <option value="" selected="selected">Todo</option>
                                '.$division.'                           
                              </select>  
                            </div>
						</div>
					</div>
					';
				}
        
		$filtro .= '<div class="col-sm-3">
						<div class="input-group">
							<div class="input-group">
                              	<span class="input-group-addon">Area de mercado </span>
                              	<select class="form-control" id="aMercado" name="aMercado">
                                	<option value=""selected="selected">Todo</option>
                                	'.$amercado.'                     
                              	</select>  
                            </div>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="input-group">
							<div class="input-group">
                              	<span class="input-group-addon">Familia </span>
                              	<select class="form-control" id="familia" name="familia">
                                	<option value=""selected="selected">Todo</option>
                                	'.$familia.'                     
                              	</select>  
                            </div>
						</div>
					</div>
					<div class="col-sm-2">
						 <div class="input-group">
							<div class="input-group">
                              	<span class="input-group-addon">Grupo </span>
                              	<select class="form-control" id="grupo" name="grupo">
                                	<option value=""selected="selected">Todo</option>
                                	'.$grupo.'                     
                              	</select>  
                            </div>
						</div>
					</div>						
					<div class="col-sm-1">	
						<div class="row">
							<div class="col-sm-8" style="font-size: 11px; line-height: 27px; background: #eeeeee; border: 0.5px solid #ccc; border-radius: 4px 0px 0px 4px; text-align: center;">
							Crecimiento
							</div>
							<div class="col-sm-4">
							<input type="text" name="crecimiento" id="crecimiento" class="form-control" value="10"  placeholder="Crecimiento">
							</div>
						</div>					
								
					</div>				
					<div class="col-sm-1">
						<button type="button" class="btn btn-sm btn-info" id="buscar">
							<i class="glyphicon glyphicon-search"></i> Buscar
						</button>
					</div>	
					<div class="col-sm-1">
						<!--input type="button" class="btn btn-sm btn-info" value="Excel" id="btnExportar"/-->
						<button type="button" class="btn btn-sm btn-info" id="btnExportar">
							<i class="glyphicon glyphicon-download"></i> Excel
						</button>
					</div>
				</div>
			</div>
			';
		$html = '
       		
			<div id="mostrarDatos">
				<div class="alert" role="alert">Seleccione los campos del buscador para visualizar la información.</div>
			</div>
			<br>
			<div class="moduleTitle"><br>
				<span class="titulo"> Actualizar precios fob negociados</span>
			</div>
			<div class="alert" role="alert">Haga click en el boton para desplegar la carga de archivos.</div>
			<button class="btn btn-sm btn-info" onclick="mostrarYocultar()" id="botonSubirExcel">Subir archivo excel para precios fob negociados</button>
			<div id="miContenedor" style="display: none;">
				<div class="row" style="text-align:left">
	                <button type="button" class="btn btn-sm btn-info" id="btnExportarExportar" onclick="btnExportarExportar()">
	                  <i class="glyphicon glyphicon-download"></i> Descargar plantilla
	                </button>
	            </div>
				<div class="row">
					<div class="col-sm-2">
						<input type="file" id="miArchivoExcel"><br>	
					</div>
					<div class="col-sm-5">
						<div class="mostrarDatosExcel">
							<div id="mostrarDatosExcel"></div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="mostrarDatosExcel">
							<div id="respuestaDatosExcel"></div>
						</div>
					</div>
				</div>										
			</div>	

			<!--button type="button" class="btn btn-sm btn-info" onclick="ventaModal()" ><i class="glyphicon glyphicon-upload"></i> Subir excel</button>
			<div id="ventanaModal"></div-->

			<!--textarea id="log" style="width:100%;height:100px;"></textarea>
			<input type="button" onclick="document.getElementById(\'log\').value =JSON.stringify(document.getElementById(\'mostrarDatos\').jexcel.getJson())" value="Get JSON" -->
       ';
       $modal ='<div id="ventanaModal"></div>';
       	echo $css.$cabecera.$titulo.$filtro.$html.$js.$modal;
    }
}