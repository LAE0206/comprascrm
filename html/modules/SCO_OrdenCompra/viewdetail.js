$(document).ready(function(){
  $(".module-title-text").before("<div class='nuevoTitulo'></div>");
  $(".module-title-text").after("<span style='position: absolute;margin: 30px 0px 0px 10px;color: #666;font-size: 15px;'>Orden de compra</span>");
})

var d = new Date();
var n = d.getTime();

function showreport(id){
	var url1 = "/modules/reportes/ordencompra.php?id="+id+"&ejec="+n;
	window.open(url1,"","width=1220,height=650");
}
function imprimir(id,nombre,orc_nomcorto){
	var url2 = "/modules/reportes/descargaoc.php?id="+id+"&name="+nombre+"&nameprov="+orc_nomcorto+"&ejec="+n;
	window.open(url2,"","");
}
function showReporteGerencialDiv03(id){
	var url1 = "/modules/reportes/reporteGerencialDiv03.php?id="+id+"&ejec="+n;
	window.open(url1,"","width=1220,height=650");
}
function descargaReporteGerencialDiv03(id,nombre,orc_nomcorto){
	var url2 = "/modules/reportes/descargaReporteGerencialDiv03.php?id="+id+"&name="+nombre+"&nameprov="+orc_nomcorto+"&ejec="+n;
	window.open(url2,"","");
}

function estado(est,id){
   if(divisionSolicitante == '06' || divisionSolicitante == '03' || divisionSolicitante == '02' || divisionSolicitante == '07' || divisionSolicitante == '99' || divisionSolicitante == '98' || divisionUsuario == '99' || divisionUsuario == '04' || divisionUsuario == '08' || divisionSolicitante == '08'){
    /*Aprobacion ProcessMaker*/
    var respuestaValidacion = validaVacacion(id)
    if(respuestaValidacion == false){
      	if(est == 3){
        		var titulo = "Aprobación de Orden de Compra";				
        		var mensaje = "Envío de Orden de Compra a ProcessMaker";
        		var cuerpo = "<br><center ><p class='text-info'><strong>Desea enviar su orden de compra a sus aprobadores?</strong></p>";
        		cuerpo += "<br><p > Se realizara el envío de un correo electrónico  a su primer aprobador y creara un caso en ProcessMaker para su respectiva aprobación </p> </center>";
        		ventanaModalPM(titulo,cuerpo,mensaje,est,id);
        		$('#modalOrdenCompraPM').modal('show');
      	}else{
      		  envioDeAprobadores(est,id);
      	}
      }
    }else{
      /*Descomentar en caso de aprobacion manual*/
      envioDeAprobadores(est,id);
    }
}

function validaVacacion(id){  
  var resp = false;
		$.ajax({
		type: 'get',
    url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=validavacacion',
		data: {id},
		dataType: 'json',
		success: function(data) {
			console.log(data);
      json = JSON.parse(data);
      console.log(json.length);
      var htmlvv = '<table class="table-bordered list view table-responsive table-striped">';
      htmlvv += '<thead><tr><th>Usuario</th><th>Nombre</th><th>Mensaje</th><th>Estado</th><th>Usuario Backup</th><th>Nombre Backup</th></tr></thead>';
      htmlvv += '<tbody>';
      if(json.length > 0){        
          for(var i = 0; i < json.length; i++) {
            console.log(json[i]['descripcion']);
            htmlvv += '<tr><td><strong>'+json[i]['usuario']+'</strong></td>';
            htmlvv += '<td>'+json[i]['NombreCompleto']+'</td>';
            htmlvv += '<td>'+json[i]['descripcion'].replaceAll(null, "<b style=\"color:red;\">No asignado</b>")+'</td>';
            htmlvv += '<td><strong>'+json[i]['Estado']+'</strong></td>';
            htmlvv += '<td>'+json[i]['backup'].replaceAll(null, "<b style=\"color:red;\">No asignado</b>")+'</td>';   
            htmlvv += '<td>'+json[i]['nombrebackup'].replaceAll(null, "<b style=\"color:red;\">No asignado</b>")+'</td></tr>';      
          }
        htmlvv += '</tbody></table>';          
        var titulo = "No se pudo enviar su Orden de Compra";				
        var mensaje = "No se pudo enviar su Orden de Compra por que los siguientes aprobadores se encuentran de vacaciones.";
        var cuerpo = "<p>Listado de Aprobadores en vacacion.</p><br>"+htmlvv+"<br><br><p>Para continuar con su Orden de Compra, registre <strong>otros aprobadores o los backups </strong>de los aprobadores que se muestran en el listado.</p>";
        ventanaModal(data,titulo,cuerpo,mensaje);
        $('#modalOrdenCompra').modal('show');
        resp = true;
        console.log('Respuesta :'+ resp);
      }else{
        resp = false;
        console.log('Respuesta :'+ resp);
      }            
		},
    error: function (request, error) {
      console.log('ERROR!!!!; ' + error)
      resp = false;
    }
	});
		return resp;
}

function verificarObs(id,estado){
	//var estado = false;
	var Resp = true;
	$.ajax({
		url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=verificarEstado&id='+id,
		type: 'GET',
		data: {estado},
		dataType: 'json',
		success:function(data) {
			Resp = data['r'];
		}
	});
	return Resp;
}

function envioDeAprobadores(est,id){
  document.getElementById("menu-send").removeAttribute("onclick");
  document.getElementById("menu-send").style.pointerEvents = "none";
  document.getElementById("menu-send").style.cursor = "not-allowed";
  document.getElementById("menu-send").style.color = "#CCC";
debugger;	
	$('#envioConfirma').html('<btn class="btn btn-sm alert-success" >Enviando su Informacion...</btn>');
	$('#btnConfirma').css('display','none');
	console.log("estado :" +est + ", Id:" + id);
	var num = est;
	var respuesta = verificarObs(id,num);	
	if ( respuesta == true) {
		$.ajax({
		type: 'get',
		url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=ordencompra&id='+id,
		data: {num},
		beforeSend: function(){
			//alert('Procesando los datos');
			$('#btn-estados').css('pointer-events','none');
			$('#btn-estados').css('background','#CCC');
			$('.loader').addClass('is-active');
		},
		success: function(data) {
			$('#modalOrdenCompraPM').modal('hide');
			debugger;		
			$('.loader').removeClass('is-active');
			data = data.replace('$','').replace(/(\r\n|\n|\r)/gm,'').replace('>','');
			var desctot = $.parseJSON(data);
			desctot = desctot.replace('\"','').replace('\\','').split('~');   

			if(desctot[0] == 100){
				if(desctot[1] == 0){
	        		if(desctot[2] == desctot[3]){
  		          var jsonMule = desctot[4];
                //var jsonMule = JSON.parse(desctot[4]);
      					//jsonMule = JSON.parse(jsonMule);
         		    //console.log("RESPUESTA SERVICIO jsonMule: "+ jsonMule);
      					//console.log("TRANSACCION "+ jsonMule.transaccion);
      					//console.log("DESCRIPCION "+ jsonMule.descripcion);
	        			if(desctot[4].replace('\"','').replace('\\','') == 'true'){
	        				console.log('conexion exitosa, num = ' + desctot);
	        				alert("Su informacion de Orden de Compra se envio exitosamente, se refrescara su pantalla.");							
       		  		  location.reload(true);
	        			}else{
                  //alert("Su informacion no se pudo enviar, se refrescara su pantalla.");							
      		  		  //location.reload(true);
	        				$('#btn-estados').css('pointer-events','visible');
  		     				var titulo = "Envío de Aprobacion fallido!!!";				
  							  var mensaje = "Servicio de aprobación de Ordenes de Compra no disponible.";
  							  var cuerpo = "<br>Descripcion: <br><center > <p style='color:red;'><strong>No se pudo realizar el envío de su Orden de compra.</strong></p>";
  							  cuerpo += "<br><p >Consulte con su Administrador</p> <span id='reduccionTiempo'></span></center>";
  							  ventanaModal(data,titulo,cuerpo,mensaje);
                  $('#modalOrdenCompra').modal('show');
                  
                  var countdownElement = document.getElementById("reduccionTiempo");
                  var timeLeft = 10;
                  var countdownInterval = setInterval(function() {                                       
                    countdownElement.textContent = "Redireccionando en " + timeLeft + " segundos...";
                    timeLeft--;
                    if (timeLeft === 0) {    
                        clearInterval(countdownInterval);
                        location.reload(true);
                      }
                  }, 1000);
	        			}
	  				}else{
	     				//$('#alertapp').append('<div class="alert alert-danger"><button type="button" style=" background: transparent !important; color: #000000!important; padding-left: 10px; " class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Verifique los precios totales de <strong>Productos</strong> '+ desctot[3] + ' y <strong>Plan de Pagos</strong> '+ desctot[2] + '</div>');
	     				$('#btn-estados').css('pointer-events','visible');
	     				var titulo = "Productos - Plan de Pagos";				
						var mensaje = "Alerta!!! Los montos totales de sus Productos y Plan de pago no son iguales";
						var cuerpo = "<br><center > <p class='text-info'><strong>Para contiuar sus montos deben ser iguales.</strong></p>";
						cuerpo += "<br><p >  Productos precio Total = " + desctot['3'] + ", Plan de pagos suma de los Montos = " + desctot['2'] + " </p> </center>";
						ventanaModal(data,titulo,cuerpo,mensaje);
		      			$('#modalOrdenCompra').modal('show');
	  				}
				}else{
					console.log('conexion exitosa, num = ' + desctot);
					console.log('Error Proyecto');
					if(desctot[1] == 1){
	  					//$('#alertapp').append('<div class="alert alert-danger"><button type="button" style=" background: transparent !important; color: #000000!important; padding-left: 10px; " class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Tiene</strong> ' + desctot[1] + ' Proyecto mal registrado en Productos</div>');
	  					$('#btn-estados').css('pointer-events','visible');
	  					var titulo = "Productos";				
						var mensaje = "<strong>Alerta!!! Tiene " + desctot[1] + " Proyecto no registrado</strong>";
						var cuerpo = "<br><center > <p class='text-info'><strong>Todos sus items deben estar relacionados \"registrados\" con un Proyeco / CO existente en el sistema.</strong></p>";
						cuerpo += "<br><p > Para cambiar de estado <b>\"Borrador\"</b> a \"Aprobacion\" de su Orden de compra, registre los proyectos.</p> </center>";
						ventanaModal(data,titulo,cuerpo,mensaje);
			  			$('#modalOrdenCompra').modal('show');
					}else{	
	  					//$('#alertapp').append('<div class="alert alert-danger"><button type="button" style=" background: transparent !important; color: #000000!important; padding-left: 10px; " class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Tiene</strong> ' + desctot[1] + ' Proyectos mal registrados en Productos</div>');
	   					$('#btn-estados').css('pointer-events','visible');
	   					var titulo = "Productos";				
						var mensaje = "<strong>Alerta!!! Tiene " + desctot[1] + " Proyecto no registrado</strong>";
						var cuerpo = "<br><center > <p class='text-info'><strong>Todos sus items deben estar relacionados \"registrados\" con un Proyeco / CO existente en el sistema.</strong></p>";
						cuerpo += "<br><p > Para cambiar de estado <b>\"Borrador\"</b> a \"Aprobacion\" de su Orden de compra, registre los proyectos.</p> </center>";
						ventanaModal(data,titulo,cuerpo,mensaje);
			  			$('#modalOrdenCompra').modal('show');
					}
				}
			}else{
				//$('#alertapp').append('<div class="alert alert-danger"><button type="button" style=" background: transparent !important; color: #000000!important; padding-left: 10px; " class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
				$('#btn-estados').css('pointer-events','visible');
				var titulo = "Plan de Pagos";				
				var mensaje = " La suma de los porcetajes de su <b>plan de pagos</b>, no coincide con el 100% = " + desctot['3'] + " del momnto total de <b>Productos</b>";
				var cuerpo = "<br><center > <p class='text-info'><strong>Complete el 100 % del Plan de Pagos</strong></p>";
				cuerpo += "<br><p >  Suma de % de plan de pagos = " + desctot['0'] + " %, equivalente a un monto de " + desctot['2'] + " </p> </center>";
				ventanaModal(data,titulo,cuerpo,mensaje);
      			$('#modalOrdenCompra').modal('show');
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$('#envioConfirma').css('display','none');
			$('#datosModal').append('<div class="alert alert-danger"><button type="button" style=" background: transparent !important; color: #000000!important; padding-left: 10px; " class="close" data-dismiss="alert" aria-hidden="true">&times;</button>OCURRIO UN ERROR EN LA SOLICITUD, COMUNIQUESE CON SU ADMINISTRADOR DE SISTEMAS.</div>');
			alert("OCURRIO UN ERROR EN LA SOLICITUD, COMUNIQUESE CON SU ADMINISTRADOR DE SISTEMAS.");
			console.log("OCURRIO UN ERROR EN LA SOLICITUD, COMUNIQUESE CON SU ADMINISTRADOR DE SISTEMAS" + textStatus, errorThrown);
		  }
		});
	}
	else {
		alert(respuesta);
	}
}

//Estructura de la vista de la venta modal
function ventanaModal(datos,titulo,cuerpo,mensaje){	
    console.log(datos);
	    var htmlm = '';
	    htmlm += '<div class="modal fade" id="modalOrdenCompra" style="display: block;margin-top: 5%;">';
	    htmlm += '    <div class="modal-dialog">';
	    htmlm += '        <div class="modal-content">';
	    htmlm += '            <div class="modal-header" style="background:#a94442 !important;">';
	    htmlm += '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	    htmlm += '                <h4 class="modal-title">'+titulo+'</h4>';
	    htmlm += '            </div>';
	    htmlm += '            <div class="modal-body" >';
	    htmlm += '                <div class="panel panel-danger">';
    	htmlm += '                    <div class="panel-heading">' + mensaje+ '</div>';
    	htmlm += '                    <div class="panel-body"style="padding: 20px 30px;">';
    	htmlm += '						<div id="datosModal">' + cuerpo + '</div>';
		htmlm += '					  </div>';
		htmlm += '				   </div>';
	    htmlm += '            </div>';
	    htmlm += '        </div>';
	    htmlm += '    </div>';
	    htmlm += '</div>';  
    $("#ventanaModal").html(htmlm);
}

//Estructura de la vista de la venta modal Solicitud de Aprobacion
function ventanaModalPM(titulo,cuerpo,mensaje,est,id){
    
	    var htmlm = '';
	    htmlm += '<div class="modal fade" id="modalOrdenCompraPM" style="display: block;margin-top: 5%;">';
	    htmlm += '    <div class="modal-dialog">';
	    htmlm += '        <div class="modal-content">';
	    htmlm += '            <div class="modal-header" style="background:#0e2741 !important;">';
	    htmlm += '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	    htmlm += '                <h4 class="modal-title">'+titulo+'</h4>';
	    htmlm += '            </div>';
	    htmlm += '            <div class="modal-body" >';
	    htmlm += '                <div class="panel panel-info">';
    	htmlm += '                    <div class="panel-heading">' + mensaje+ '</div>';
    	htmlm += '                    <div class="panel-body"style="padding: 10px;">';
    	htmlm += '						<div id="datosModal">' + cuerpo + '</div>';
		htmlm += '					  </div>';
		htmlm += '				   </div><hr>';

		htmlm += '            <div class="row">';
		htmlm += '               <div class="col-sm-6">';
		htmlm += '                   <button type="button" class="btn btn-sm btn-danger" style="width: 100%;background: #dc3545;color:#fff;border:solid 1px#dc3545;" data-dismiss="modal">Cancelar</button>';
		htmlm += '               </div>';
		htmlm += '               <div class="col-sm-6">';
		htmlm += '                   <span id="envioConfirma" ><span><button type="button" id="btnConfirma"class="btn btn-sm btn-verde" style="width: 100%;background: #31708f;color:#fff;" onclick=envioDeAprobadores("'+est+'","'+id+'");>Confirmar y Enviar</button>';
		htmlm += '               </div>';
		htmlm += '            </div>';

	    htmlm += '            </div>';
	    htmlm += '        </div>';
	    htmlm += '    </div>';
	    htmlm += '</div>';    
    $("#ventanaModalPM").html(htmlm);
}

//Agregando el boton de ordenar en el subpanel Aprobadores

function modalOrdernarAprobador(){
    	$.ajax({
    		url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=backorderdata',
    		type: 'POST',
    		data: {
        idco:idOc,
        filtro:3},
    		dataType: 'json',
    		success:function(data) {
         console.log(data.length)
         var htmltable = '<table class="table table-bordered list" id="table-1" cellspacing="0" cellpadding="2">';
         htmltable += '<thead><tr><th>#</th><th>Usuario</th><th>Tipo de Aprobador</th><th>Correlativo</th></tr></thead>';
         htmltable += '<tbody>';
         for(var i = 0; i < data.length; i++) {
            htmltable += '<tr id="'+data[i]['id']+'" nombre="'+data[i]['name']+'"><td> <img class="aprimg" src="data:image/png;base64,/9j/4AAQSkZJRgABAQACWAJYAAD/2wCEAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDIBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/CABEIAMgAyAMBIgACEQEDEQH/xAAwAAEBAAMBAQEAAAAAAAAAAAAABQIDBAEGBwEBAQEBAAAAAAAAAAAAAAAAAAECA//aAAwDAQACEAMQAAAA/XxrmAAAAAAAAAAAAAAAAAdnVNSVnAkuzjsBAAAAAAAAMq2PVnYKA5+gQca0m4CwAAAAABu090tITYAACLam2cIuAAAAAAHfwdUtYTYAACdRkpyjWAAAAAAHvhbmyPXzr0KAPDCJv57gLAAAAAAAG7SWx0fP5y3UTArTdJAsAAAAAAAMu6WfurbGpeygODygiXptK+fXOJOBljcgAAAAOn2rNY5k0AAAABhLr+JAdXLrIIAA2a6UvZkTYAAAAAAGMW5w2TRcAALMa5NbBNAAAAAAANO7WQxrAIAuQ7k3sEoAAAAAADXs1kMawCALhN7BKAAAAAAA1hDGsAn/xAA3EAACAQEDCAkCBQUAAAAAAAABAgMEABExBRIgITBAQVETIjI0YXFyobFTkRQjYoGCEBVSYNH/2gAIAQEAAT8A/wBUAJNwBJ5C0WT3cXyHMHLE2SggXFS3mbfhIPpLZ8nwNgCp8DaXJ8iAmM545YGxBBuIuPLco42kcIovJtT0qQLfi/FtKopUnXk/BrSI0blHFxG40VP0MWcw67e2wrafpYs5R111+Y3Clj6WoVTgNZ2VVH0VQwGB1jb5NX8yRuQA2WUl/MjbmCNvkw65B5bLKR1xjwO3oHzKi44MLtlXvn1N3+Iu26sVYMMQbxaGQSxK44+2wmlEMTOeGHjYksxY4k3ncKSpMD3G8ocfDxsrB1DKQQcCNJmCqWY3AYm1XUmd7hqQYblBUyQHVrXiptFWRS6s7Nbk39cLS1kMV4zs5uS2nqZJzrNy8FG6rLInZdh5G34qf6rfezSSP2nY+Z3TE3WSlnfCMgczqsuTpD2nUe9hk3nL9lt/bV+q32scmDhL91s2TpB2XU+1npZ0xjJHMa7YG47dEeRs1FLHwtDk7jK38RaOGOIdRAP22EkEco66A+N1pcncYm/i3/bOjRtmupB8dpTUjTdZtSc+do4kiXNQADaSRJKua4vFqmkaDrL1k58tlR0vTHPfsD3sAALhtiARccLVlL0Jz07B9thDEZpVQfueQsiBFCqLgBcNwdA6lWF4Oo2miMMrIeGB5jTydEAjSHEm4eW5ZRjvRZBiNR8tOlXNpox+m/cqtc6lkH6b9ODu8fpHxuU/d5PSdODu8fpHxuU/d5PSfjTg7vH6R8blP3eT0n404O7x+kfG5T93k9J+ND//xAAZEQADAQEBAAAAAAAAAAAAAAABETAAIED/2gAIAQIBAT8A8Tz9gqKiSyyk8888+3QxHBqamn//xAAbEQEAAwEAAwAAAAAAAAAAAAABESAwABIxQP/aAAgBAwEBPwD4o6Mwo4FXAq+sRouQ95cuUdB0HR0XC6TUxaGpqaf/2Q=="/></td>';
            htmltable += '<td><b>'+data[i]['name']+' </b></td>';
            htmltable += '<td>'+data[i]['apr_tipo']+'</td>';
            htmltable += '<td id="'+data[i]['apr_correlativo']+'">'+data[i]['apr_correlativo']+'</td></tr>'; 
          }
          htmltable += '</tbody>';
          htmltable += '</table>';
        var titulo = "Aprobadores";				
        var mensaje = "Ordenar el listado de aprobación par su correspondiente Aprobación de su Orden de Compra";        		
        ventanaModalSubAprobadores(titulo, mensaje, idOc, htmltable);
        $('#ventanaModalSubAprobadores').modal('show');
    		}
    	});
      

}
      var usuarios = [];
      var objetoUser = new Object();
//Estructura de la vista de la venta modal Solicitud de Aprobacion
function ventanaModalSubAprobadores(titulo,mensaje, idOc, cuerpo){
	    var htmla = '';
	    htmla += '<div class="modal fade" id="ventanaModalSubAprobadores" style="display: block;margin-top: 5%;">';
	    htmla += '    <div class="modal-dialog" style="width: 50% !important;">';
	    htmla += '        <div class="modal-content">';
	    htmla += '            <div class="modal-header" >';
	    htmla += '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	    htmla += '                <h4 class="modal-title">'+titulo+'</h4>';
	    htmla += '            </div>';
	    htmla += '            <div class="modal-body" >';
	    htmla += '                <div>' + mensaje+ '<span id="debugArea"></span></div><div class="panel">';
    	htmla += '                <div class="panel-heading"></div>';
    	htmla += '                    <div class="panel-body"style="padding: 10px;">';
    	htmla += '						        <div id="datosModal">';
      htmla +=                       cuerpo;
  		htmla += '					          </div>';
  		htmla += '				        </div><hr >';
  		htmla += '              <div class="row">';
  		htmla += '               <div class="col-sm-6">';
  		htmla += '                   <button type="button" class="btn btn-sm btn-danger" style="width: 100%;background: #dc3545;color:#fff;border:solid 1px#dc3545;" data-dismiss="modal">Cancelar</button>';
  		htmla += '               </div>';
  		htmla += '               <div class="col-sm-6">';
      htmla += '                   <span id="envioConfirma" ><span><button type="button" id="btnConfirma"class="btn btn-sm btn-verde" style="width: 100%;background: #31708f;color:#fff;" onclick=btnEnvioDatosAprobador(usuarios)>Confirmar y Enviar</button>';
  		htmla += '               </div>';
  		htmla += '              </div>';
	    htmla += '            </div>';
	    htmla += '        </div>';
	    htmla += '    </div>';
	    htmla += '</div>';    
    $("#ventanaModalAprobacion").html(htmla);
	$("#table-1 tr").hover(function() {
		$(this).addClass('showDragHandle');
	}, function() {
		$(this).removeClass('showDragHandle');
	});
    $("#table-1").tableDnD({
       onDragClass: "myDragClass",
        onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
            var debugStr = "Row dropped was "+row.id+". New order: ";
            for (var i=0; i<rows.length; i++) {
                debugStr += rows[i].id+" ";
            }
            $('#debugArea').html(debugStr);
			alert(debugStr);
        },

        onDragStart: function(table, row) {
            $('#debugArea').html("Selecciono a "+row.nombre);
			//$('#'+row.id).css('background', 'blue');
        },
        
        onDrop: function(table, row) {
            /*Aqui para lanzar un evento al momento de cambiar filas .replace('Original Text','New Text')*/
            usuarios = [];            
            //console.log(table.tBodies[0].rows);
            var rows = table.tBodies[0].rows;
            var debugStr = "Row dropped was "+row.id+". New order: ";
            console.log(rows.length);
            for (var i=0; i<rows.length; i++) {
				objetoUser = new Object();
                debugStr += rows[i].id+" ";
                objetoUser.id = rows[i].id;
                usuarios.push(objetoUser);
            }
            console.log(usuarios);
        }        
    });
    $("#table-1 tr:even").addClass("alt fondo");
}

function btnEnvioDatosAprobador(usuarios){
  console.log("Envio datos" + JSON.stringify(usuarios));
  $.ajax({
		url: 'index.php?to_pdf=true&module=SCO_OrdenCompra&action=backorderdata',
		type: 'POST',
		data: {
        usuarios: usuarios,
        filtro:4
        },
		dataType: 'json',
		success:function(data) {
         console.log("RESPUESTA PHP: "+data);
		 location.reload(true);
		}
	});
}