<?php
/**
*Este metodo realiza la extracion de datos de la base de datos para mostrarlos en en FrontEnd
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2020
*@license ruta: /var/www/html/modules/SCO_Productos
*/
if(!defined('sugarEntry'))define('sugarEntry', true);
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');
//pobla usuario actual logeado
global $current_user;
    
$fecha_desde = $_POST['fecha_desde'];
$fecha_hasta = $_POST['fecha_hasta'];
$division = $_POST['division'];
$aMercado = $_POST['aMercado'];
$idco = $_POST['idco'];
$filtro = $_POST['filtro'];
if ($aMercado == '00') {
	$aMercado = '';
}
if ($division == '00') {
	$division = '';
}
$idDiv = $current_user->iddivision_c;

switch ($filtro) {
	case 1:
		try {
		    $query = "call suitecrm.sp_consolidacion_backorder('$fecha_desde','$fecha_hasta','$division','$aMercado');";
		    $results = $GLOBALS['db']->query($query, true);
		    $object= array();
		    while($row = $GLOBALS['db']->fetchByAssoc($results))
		        {
		            $object[] = $row;
		        }
		    echo json_encode($object);
		} catch (Exception $e) {
			echo "Error, no se pudo realizar la peticion";
		}
		break;
	case 2:
		try {
		    $query = "SELECT 
						pro_nombre,
            pro_codaio,
						pro_descripcion,
						pro_unidad,
						pro_cantidad,
						pro_preciounid,
						pro_descval,
						pro_descpor,
						pro_saldos
						FROM suitecrm.sco_productos_co
						WHERE pro_idco = '".$idco."'
						AND pro_saldos > 0;";
		    $results = $GLOBALS['db']->query($query, true);
		    $objectPro= array();
		    while($row = $GLOBALS['db']->fetchByAssoc($results))
		        {
		            $objectPro[] = $row;
		        }
		    echo json_encode($objectPro);
		} catch (Exception $e) {
			echo "Error, no se pudo realizar la peticion";
		}
		break;
   case 3:
		try {
		    $query = "SELECT ap.id, ap.name, ap.apr_titulo, ap.user_id_c, ap.apr_correlativo, ap.apr_tipo
                  FROM suitecrm.sco_ordencompra_sco_aprobadores_c as oc_ap
                  INNER JOIN suitecrm.sco_aprobadores as ap
                  on oc_ap.sco_ordencompra_sco_aprobadoressco_aprobadores_idb = ap.id
                  WHERE sco_ordencompra_sco_aprobadoressco_ordencompra_ida = '".$idco."'
                  AND ap.deleted = 0
                  AND oc_ap.deleted = 0
                  ORDER BY apr_correlativo ASC";
		    $results = $GLOBALS['db']->query($query, true);
		    $object= array();
		    while($row = $GLOBALS['db']->fetchByAssoc($results))
		        {
		            $object[] = $row;
		        }
		    echo json_encode($object);
		} catch (Exception $e) {
			echo "Error, no se pudo realizar la peticion";
		}
		break;
 	case 4:
			try {
			$array = array();
			$usuarios = $_POST['usuarios'];
			$cantidadUsuario = json_decode($usuarios, true);
			$contador = 11;
      $contarNumero = 1;
			foreach ($usuarios as $value) {
				$correlativo = $contador;
				array_push($array, $contador);
				$query = "UPDATE suitecrm.sco_aprobadores
				SET apr_correlativo = ".$correlativo."
				WHERE id = '".$value['id']."';";
				$results = $GLOBALS['db']->query($query, true);
        if($contarNumero == 6){
          $contador = $contador + 4;
          $contarNumero = 0;
        }
        $contador++;
        $contarNumero++;
			}
		    echo json_encode($array);
		} catch (Exception $e) {
			echo "Error, no se pudo realizar la peticion";
		}
		break;
	default:
		echo "Error, no se pudo realizar la peticion";
		break;
}

