<?php
/**
*esta clase extrae todo los datos para la integracion con el proceso de Solicitud de adquisiciones MHES
*@author Yilmar Choque <jose.choque.m@hansa.com.bo>
*@copyright 2020
*@license /var/www/html/custom/modules/SCO_Aprobadores
*/
if(!defined('sugarEntry'))define('sugarEntry', true);
require_once('data/BeanFactory.php');
require_once('include/entryPoint.php');


class Aprobadores{

    function getAprobador($id){
        try {            
            $payload ='{"id":"'.$id.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://docker.hansa.com.bo:22002/mhes"); # PRD
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
            #curl_exec($ch);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            $intReturnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            curl_close ($ch);
            if ($intReturnCode != 200) {
                $var = '404';
            }else{
                $var = $body;
            }
        } catch (Exception $e) {
            $var = '404';   
        }
         return $var;       
    }
    
    function getValidavacacion($id){
        try {            
        $payload ='{"idOc":"'.$id.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://docker.hansa.com.bo:7511/vacaciones"); # PRD            
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
            curl_exec($ch);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            $intReturnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            curl_close ($ch);
            if ($intReturnCode != 200) {
                $var = '404';
            }else{
                $var = $body;                
            }
        } catch (Exception $e) {
            $var = '404';   
        }
         return $var;       
    }
    
    function envioProductosPrecioFob($base64){
        try { 
        $payload ='{"base64":"'.$base64.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://docker.hansa.com.bo:7513/updatepreciofob");          
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
            curl_exec($ch);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            $intReturnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            curl_close ($ch);
            if ($intReturnCode != 200) {
                $var = '404';
            }else{
                $var = $body;                
            }
        } catch (Exception $e) {
            $var = '404';   
        }
         return $var;       
    }
}
?>