<?php
/**
*Esta clase realiza operaciones matem?ticas.
*
*@author Limberg Alcon <lalcon@hansa.com.bo>
*@copyright 2018
*@license /var/www/html/modules/SCO_OrdenCompra
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class SCO_ProductosComprasViewDetail extends ViewDetail {

  function SCO_ProductosComprasViewDetail(){
    parent::ViewDetail();
    $this->useForSubpanel = true;
  }

  public function preDisplay(){
      parent::preDisplay();
    }

  function display(){
    global $current_user; 

    parent::display();
    echo '<div class="row" style="background:#FFF; padding:10px">
            <div class="col-sm-12">
              <iframe src="http://swarm.hansa.com.bo:3001/public/question/0c161145-a064-48bf-972d-5683ea340cb4?IdProducto='.$this->bean->proge_codaio.'" frameborder="0" width="100%" height="400" allowtransparency></iframe>
            </div>
          </div>';
  }
}
?>
